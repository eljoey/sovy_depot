import 'dart:convert';

import 'package:depot/container/depot_container.dart';
import 'package:depot/styles/appTheme.dart';
import 'package:depot/utils/ApiBaseHelper.dart';
import 'package:depot/utils/AppPrefs.dart';
import 'package:depot/utils/ShareMethod.dart';
import 'package:depot/utils/UrlConfig.dart';
import 'package:depot/utils/force_logout.dart';
import 'package:depot/utils/progress_dialog.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'models/response.dart';

class HomeMenu extends StatefulWidget {
  final String name;

  HomeMenu({Key key, this.name}) : super(key: key);

  @override
  _HomeMenuState createState() => _HomeMenuState();
}

const drop = "Sovy.Depot.Plugins.Mobile.frmGateIn";
const pick = "Sovy.Depot.Plugins.Mobile.frmTruckIn";
const gateout = "Sovy.Depot.Plugins.Mobile.frmTruckGateOut";
const eor = "Sovy.Depot.Plugins.frmMaintenanceRepair";
const container = "Sovy.Depot.Plugins.frmContainerDetails";

class _HomeMenuState extends State<HomeMenu> {
  ApiBaseHelper _httpHelper = ApiBaseHelper();
  DateTime? currentBackPressTime;

  bool accessDrop = false;
  bool accessContainer = false;
  bool accessGateOut = false;
  bool accessEOR = false;
  bool accessPick = false;
  int bit;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(const Duration(milliseconds: 500), () {

      _submitPrimeSearch();

    });
  }

  void _submitPrimeSearch() async {
    String baseUrl = await AppPrefs.getAppServer();

    // Save our form now.
    Map data = {'Key': 'Data', 'Value': "null"};

    final response = await _httpHelper.post(
        baseUrl, UrlConfig.GET_ACCESS_RIGHTS, json.encode(data));
    Response res = Response.fromJson(response);
    print("PKM" + res.statusCode.toString());
    if (res.statusCode == 200) {
      List tempResult = json.decode(res.results[0].value);
      for (int i = 0; i < tempResult.length; i++) {

          setState(() {
            if (tempResult[i]['FormName'] == drop) {
              accessDrop = true;
            }
            if (tempResult[i]['FormName'] == pick) {
              accessPick = true;
            }
            if (tempResult[i]['FormName'] == eor) {
              accessEOR = true;
            }
            if (tempResult[i]['FormName'] == gateout) {
              accessGateOut = true;
            }
            if (tempResult[i]['FormName'] == container) {
              accessContainer = true;
            }

            if (tempResult[i]['FormName'] == container) {
              bit = tempResult[i]['Permission'];
              print(bit);
            }
          });

      }
    } else {
      Fluttertoast.showToast(
          msg: res.errorMessage,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );      setState(() {
        FocusScope.of(context).unfocus();
      });

      if (res.errorMessage.contains("AccessToken"))
        new force_logout(context).doForceLogout();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppTheme.nearlyWhite,
        endDrawer: Drawer(
          child: ListView(children: <Widget>[
            ListTile(
              leading: Icon(Icons.report),
              title: Text('About'),
              onTap: () {
                Navigator.pushNamed(context, "/about");
              },
            ),
          ],),
        ),
        appBar: new AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          leading: Padding(
            padding:
                const EdgeInsets.only(top: 8.0, left: 16, right: 0, bottom: 8),
            child: AspectRatio(
                aspectRatio: 1,
                child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        color: Colors.white),
                    child: Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: GestureDetector(
                          onTap: () {
                          },
                          child:
                              Image.asset("assets/images/owl_transporter.png")),
                    ))),
          ),
          title: widget.name == null
              ? new Text(
                  "Home",
                  style: AppTheme.title_white,
                )
              : Row(
                  children: <Widget>[
                    new Text('Home - ', style: AppTheme.title_white),
                    Expanded(
                        child: MarqueeWidget(
                      child: new Text(widget.name, style: AppTheme.title_white),
                    ))
                  ],
                ),
          backgroundColor: AppTheme.colorPrimary,
        ),
        body: Column(children: <Widget>[
          Stack(
            children: [
              Container(
                  width: MediaQuery.of(context).size.width,
                  color: Colors.white,
                  child: Image.asset(
                    "assets/images/banner.png",
                    fit: BoxFit.fill,
                  )),
              if(accessContainer)
              Positioned(
                left: 20,top: 20,
                child: Container(
                  width: 80, height: 80,
                  child: InkWell(
                    onTap: (){
                      Navigator.pushNamed(context, "/container",arguments: ContainerDetail(bit: bit,));
                    },
                    child: Card(
                      elevation: 3,
                      child: Padding(
                        padding: const EdgeInsets.all(2.0),
                        child: Image.asset(
                          "assets/images/enquire.png",
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Expanded(
              child: Row(
            children: <Widget>[
              SizedBox(
                width: 20,
              ),
              Expanded(
                  child: GestureDetector(
                      onTap: () {
                        if (accessDrop) {
                          Navigator.pushNamed(context, "/gateIn");
                        } else {
                          Fluttertoast.showToast(
                              msg: "This account don't have DROP access right",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 1,
                              backgroundColor: Colors.red,
                              textColor: Colors.white,
                              fontSize: 16.0
                          );

                        }
                      },
                      child: Card(
                        color: accessDrop ? Colors.white : Colors.grey,
                        elevation: 3.0,
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 10,
                            ),
                            Expanded(
                              child: Image.asset("assets/images/drop.png"),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Text(
                              "DROP",
                              style: AppTheme.titleOutlet,
                            ),
                            SizedBox(
                              height: 30,
                            )
                          ],
                        ),
                      ))),
              SizedBox(
                width: 20,
              ),
              Expanded(
                  child: GestureDetector(
                      onTap: () {
                        if (accessGateOut) {
                          Navigator.pushNamed(context, "/depot_out_listing");
                        } else {
                          Fluttertoast.showToast(
                              msg: "This account don't have GATE OUT access right",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 1,
                              backgroundColor: Colors.red,
                              textColor: Colors.white,
                              fontSize: 16.0
                          );

                        }
                      },
                      child: Card(
                        color: accessGateOut ? Colors.white : Colors.grey,
                        elevation: 3.0,
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 10,
                            ),
                            Expanded(
                              child: Image.asset("assets/images/gateout.png"),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Text("GATE OUT", style: AppTheme.titleOutlet),
                            SizedBox(
                              height: 30,
                            )
                          ],
                        ),
                      ))),
              SizedBox(
                width: 20,
              ),
            ],
          )),
          SizedBox(
            height: 20,
          ),
          Expanded(
              child: Row(
            children: <Widget>[
              SizedBox(
                width: 20,
              ),
              Expanded(
                  child: InkWell(
                      onTap: () {
                        if (accessEOR) {
                          Navigator.pushNamed(context, "/eor");
                        } else {
                          Fluttertoast.showToast(
                              msg: "This account don't have EOR access right",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 1,
                              backgroundColor: Colors.red,
                              textColor: Colors.white,
                              fontSize: 16.0
                          );

                        }
                      },
                      child: Card(
                        color: accessEOR ? Colors.white : Colors.grey,
                        elevation: 3.0,
                        child: Column(
                          children: <Widget>[
                            Expanded(
                              child: Image.asset("assets/images/eor.png"),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Text("EOR", style: AppTheme.titleOutlet),
                            SizedBox(
                              height: 30,
                            )
                          ],
                        ),
                      ))),
              SizedBox(
                width: 20,
              ),
              Expanded(
                  child: InkWell(
                      onTap: () {
                        if (accessPick) {
                          Navigator.pushNamed(context, "/truckIn");
                        } else {
                          Fluttertoast.showToast(
                              msg: "This account don't have PICK access right",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 1,
                              backgroundColor: Colors.red,
                              textColor: Colors.white,
                              fontSize: 16.0
                          );

                        }
                      },
                      child: Card(
                        color: accessPick ? Colors.white : Colors.grey,
                        elevation: 3.0,
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 10,
                            ),
                            Expanded(
                              child: Image.asset("assets/images/pick.png"),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Text("PICK", style: AppTheme.titleOutlet),
                            SizedBox(
                              height: 30,
                            )
                          ],
                        ),
                      ))),
              SizedBox(
                width: 20,
              ),
            ],
          )),
          SizedBox(
            height: 20,
          ),
        ]));
  }
}
