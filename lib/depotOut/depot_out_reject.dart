import 'dart:convert';

import 'package:depot/models/container.dart';
import 'package:depot/models/response.dart';
import 'package:depot/styles/appTheme.dart';
import 'package:depot/utils/ApiBaseHelper.dart';
import 'package:depot/utils/AppPrefs.dart';
import 'package:depot/utils/UrlConfig.dart';
import 'package:depot/utils/force_logout.dart';
import 'package:depot/utils/progress_dialog.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class DepotOutReject extends StatefulWidget {
  final ContainerModel containerModel;

  DepotOutReject({Key key, this.containerModel}) : super(key: key);

  @override
  _DepotOutRejectState createState() => _DepotOutRejectState();
}

class _DepotOutRejectState extends State<DepotOutReject> {
  String _selectedTab = "AV";
  ProgressDialog pr;
  ApiBaseHelper _httpHelper = ApiBaseHelper();
  String result, baseUrl;
  String et_reason, et_status;
  final GlobalKey<FormState> _formKey_reject = new GlobalKey<FormState>();

  @override
  void initState() {
    // TODO: implement initState
    getPrefData();

    super.initState();
  }

  getPrefData() async {
    baseUrl = await AppPrefs.getAppServer();
  }

  String _validateReason(String value) {
    if (value.length < 1) {
      return 'Please enter your reason.';
    }

    return null;
  }

  @override
  Widget build(BuildContext context) {

    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);

    return Scaffold(
      appBar: new AppBar(
        backgroundColor: AppTheme.colorPrimary,
        title: Text(
          "Reject - " + widget.containerModel.containerNo,
          style: AppTheme.title_white,
        ),
        leading: BackButton(
          color: Colors.white,
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            GestureDetector(
              onTap: (){
                FocusScope.of(context).unfocus();
              },
              child: Card(
                elevation: 2.0,
                child: Container(
                  color: Colors.transparent,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Wrap(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top:8.0,left: 8,right: 8,bottom: 8),
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(bottom: 8.0),
                                child: Row(
                                  children: <Widget>[
                                    Text("Reason:", style: AppTheme.titleDesc,),
                                  ],
                                ),
                              ),
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    child:Form(
                                      key: this._formKey_reject,
                                      child: Column(
                                        children: <Widget>[
                                          new TextFormField(
                                              keyboardType: TextInputType.multiline,
                                              maxLines: 5,
                                              validator: this._validateReason,
                                              decoration: new InputDecoration(
                                                  focusedBorder: OutlineInputBorder(
                                                    borderSide:
                                                    BorderSide(color: AppTheme.colorPrimary, width: 1.0),
                                                  ),
                                                  enabledBorder: OutlineInputBorder(
                                                    borderSide:
                                                    BorderSide(color: AppTheme.colorMediumGrey, width: 1.0),
                                                  ),
                                                  hintText: "Type your reason here.",
                                                  border: new OutlineInputBorder()),
                                            onSaved: (value) {
                                            et_reason = value;
                                          },
                                            ),
                                          Padding(
                                            padding: const EdgeInsets.only( top: 16.0),
                                            child: Row(
                                              children: <Widget>[
                                                Text("Is the Container Damage?", style: AppTheme.titleDesc,),
                                              ],
                                            ),
                                          ),
                                          new Row(
                                            mainAxisSize: MainAxisSize.max,
                                            children: <Widget>[
//                                          new Icon(, color: Colors.black45,),
                                              Expanded(
                                                child: Row(
                                                  children: <Widget>[
                                                    new Radio(
                                                      value: "AV",
                                                      onChanged: (value) {
                                                        this._selectedTab = value;
                                                        setState(() {
                                                          et_status = value;
                                                        });
                                                      },
                                                      groupValue: this._selectedTab,
                                                    ),
                                                    new Text(
                                                      'NO',
                                                        style: AppTheme.titleDescCondition
                                                    ),
                                                  ],
                                                ),
                                              ),

                                              Expanded(
                                                child: Row(
                                                  children: <Widget>[
                                                    new Radio(
                                                      value: "DM",
                                                      onChanged: (value) {
                                                        et_status = value;
                                                        setState(() {
                                                          _selectedTab = value;
                                                        });
                                                      },
                                                      groupValue: _selectedTab,
                                                    ),
                                                    new Text(
                                                      'YES',
                                                      style: AppTheme.titleDescCondition
                                                    ),
                                                  ],
                                                ),
                                              ),

                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Expanded(child: SizedBox(),),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Container(
                  color: Colors.transparent,
                  height: 50.0,
                  width: 170.0,
                  margin: new EdgeInsets.only(bottom: 10.0, top: 10.0),
                  child: Card(
                    elevation: 5.0,
                    // this field changes the shadow of the card 1.0 is default
                    child: new RaisedButton(
                      padding: const EdgeInsets.all(12.0),
                      child: new Text(
                        'Reject',
                        style: AppTheme.btnText,
                      ),
                      onPressed: () {
                        this._submitCondition();
                                           },
                      color: Colors.red,
//                              shape: RoundedRectangleBorder(
//                                  borderRadius: new BorderRadius.circular(10.0),
//                                  side: BorderSide(color: Colors.transparent)),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  void _dlg_confirm() async {
    showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Are you sure?',style: AppTheme.title,),
        content: new Text('Do you want to reject this container?'),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(this._submitCondition()),
            child: new Text('Reject', style: TextStyle(color: Colors.red),),
          ),
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(),
            child: new Text('Discard', style: TextStyle(color: Colors.black),),
          ),
        ],
      ),
    );
  }

  void _submitCondition() async {
    FocusScope.of(context).unfocus();
    if (_formKey_reject.currentState.validate()) {
      _formKey_reject.currentState.save();
      pr.show();
      Map dataKey = {
        "TruckInDocNo": widget.containerModel.truckInDocNo,
        "ContainerNo": widget.containerModel.containerNo,
        "IsAcceptOrReject": '0',
        "ContainerStatus": _selectedTab,
        "Remark": et_reason
      };

      List list = [];
      list.add(json.encode(dataKey));

      Map data = {'Key': 'Data',
        'Value': "$list"};

      final response =
      await _httpHelper.post(
          baseUrl, UrlConfig.APP_CONDITION_CHECK, json.encode(data));
      pr.dismiss();
      Response res = Response.fromJson(response);
      print("PKM" + res.statusCode.toString());
//      pr.hide();
      if (res.statusCode == 200) {
        Navigator.pop(context, "success");
      } else {
        if(res.errorMessage.contains("AccessToken"))
          new force_logout(context).doForceLogout();

        Fluttertoast.showToast(
            msg: res.errorMessage,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
      }
    }
  }

}
