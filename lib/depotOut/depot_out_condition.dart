import 'package:depot/depotOut/depot_out_position.dart';
import 'package:depot/models/container.dart';
import 'package:depot/styles/appTheme.dart';
import "package:flutter/material.dart";
import "package:flutter/cupertino.dart";
import 'package:intl/intl.dart';

import 'depot_out_reject.dart';

class DepotOutCondition extends StatefulWidget {
  final ContainerModel containerModel;

  DepotOutCondition({Key key, this.containerModel}) : super(key: key);

  @override
  _DepotOutConditionState createState() => _DepotOutConditionState();
}

class _DepotOutConditionState extends State<DepotOutCondition> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppTheme.nearlyWhite,
      appBar: new AppBar(
        backgroundColor: AppTheme.colorPrimary,
        title: Text(
          "Confirmation - " + widget.containerModel.containerNo,
          style: AppTheme.title_white,
        ),
        leading: BackButton(
          color: Colors.white,
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: SingleChildScrollView(
          child: Wrap(
            children: <Widget>[
              Card(
                elevation: 2.0,
                child: Container(
                  color: Colors.transparent,
                  child: Padding(
                    padding: const EdgeInsets.only(top:8.0,left: 8,right: 8,bottom: 8),
                    child: Wrap(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                  height: 40,
                                  child: Image.asset("assets/images/container.png")),
                              SizedBox(width: 8,),
                              Expanded(child: Text(widget.containerModel.containerNo, style: AppTheme.titleCondition,)),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top:8.0,left: 8,right: 8),
                          child: Row(
                            children: <Widget>[
                              Expanded(child: Text(widget.containerModel.typeSize, style: AppTheme.titleDescCondition,)),
                              Expanded(child: Text(widget.containerModel.operatorCode,style: AppTheme.titleDescCondition ,textAlign: TextAlign.end)),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top:16.0,left: 8,right: 8),
                          child: Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Text("Shipper:", style: AppTheme.titleDesc,),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Expanded(child: Text(widget.containerModel.shipper.length >0 ? widget.containerModel.shipper :"-", style: AppTheme.titleDescCondition,)),
                                ],
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top:16.0,left: 8,right: 8),
                          child: Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Text("Commodity:", style: AppTheme.titleDesc,),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Expanded(child: Text(widget.containerModel.commodity.length >0 ? widget.containerModel.commodity :"-", style: AppTheme.titleDescCondition,)),
                                ],
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top:16.0,left: 8,right: 8),
                          child: Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Text("Remarks:", style: AppTheme.titleDesc,),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Expanded(child: Text(widget.containerModel.remarks.length >0 ? widget.containerModel.remarks :"-", style: AppTheme.titleDescCondition,)),
                                ],
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top:16.0,left: 8,right: 8, bottom: 8),
                          child: Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Text("Truck:", style: AppTheme.titleDesc,),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Expanded(child: Text(widget.containerModel.primeMoverId.length >0 ? widget.containerModel.primeMoverId :"-", style: AppTheme.titleDescCondition,)),
                                  Expanded(child: Text(widget.containerModel.primeMoverRegNo.length >0 ? widget.containerModel.primeMoverRegNo :"-", style: AppTheme.titleDescCondition,textAlign: TextAlign.end,)),
                                ],
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Container(
                color:Colors.transparent,
                height: 70,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        color: Colors.transparent,
                        height: 50.0,
                        width: 170.0,
                        margin: new EdgeInsets.only(bottom: 10.0, top: 10.0),
                        child: Card(
                          elevation: 5.0,
                          // this field changes the shadow of the card 1.0 is default
                          child: new RaisedButton(
                            padding: const EdgeInsets.all(12.0),
                            child: new Text(
                              'Accept',
                              style: AppTheme.btnText,
                            ),
                            onPressed: () {
                              startActivityWithResult(widget.containerModel.containerNo,widget.containerModel);
                            },
                            color: Colors.green,
//                              shape: RoundedRectangleBorder(
//                                  borderRadius: new BorderRadius.circular(10.0),
//                                  side: BorderSide(color: Colors.transparent)),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 8,),
                    Expanded(
                      child: Container(
                        color: Colors.transparent,
                        height: 50.0,
                        width: 170.0,
                        margin: new EdgeInsets.only(bottom: 10.0, top: 10.0),
                        child: Card(
                          elevation: 5.0,
                          // this field changes the shadow of the card 1.0 is default
                          child: new RaisedButton(
                            padding: const EdgeInsets.all(12.0),
                            child: new Text(
                              'Reject',
                              style: AppTheme.btnText,
                            ),
                            onPressed: () {
                              startActivityWithResultReject(widget.containerModel);
                            },
                            color: Colors.red,
//                              shape: RoundedRectangleBorder(
//                                  borderRadius: new BorderRadius.circular(10.0),
//                                  side: BorderSide(color: Colors.transparent)),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  startActivityWithResult(String containerNo,ContainerModel containerModel) async {
    var result = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (_) => DepotOutPosition(
              containerNo: containerNo,
              containerModel : containerModel
            )));

    print("condition $result");
    if (result == "success") {
      Navigator.pop(context, "success");
    }else{
      FocusScope.of(context).unfocus();
    }
  }

  startActivityWithResultReject(ContainerModel containerModel) async {
    var result = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (_) => DepotOutReject(
              containerModel: containerModel,
            )));

    print("condition $result");
    if (result == "success") {
      Navigator.pop(context, "success");
    }else{
      FocusScope.of(context).unfocus();
    }
  }
}
