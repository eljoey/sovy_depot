import 'dart:convert';
import 'dart:io';

import 'package:barcode_scan2/barcode_scan2.dart';
import 'package:depot/depotOut/depot_out_condition.dart';
import 'package:depot/models/response.dart';
import 'package:depot/styles/appTheme.dart';
import 'package:depot/utils/ApiBaseHelper.dart';
import 'package:depot/utils/AppPrefs.dart';
import 'package:depot/utils/ShareMethod.dart';
import 'package:depot/utils/UrlConfig.dart';
import 'package:depot/utils/force_logout.dart';
import 'package:depot/utils/progress_dialog.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_mlkit_text_recognition/google_mlkit_text_recognition.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:depot/models/container.dart';
import 'package:flutter/services.dart';

import 'dart:math' as math;


class DepotOutListing extends StatefulWidget {
  @override
  _DepotOutListingState createState() => _DepotOutListingState();
}

class _SearchData {
  String txtSearch = '';
}


class _DepotOutListingState extends State<DepotOutListing> {
  ApiBaseHelper _httpHelper = ApiBaseHelper();
  final GlobalKey<FormState> _formKey_searchList = new GlobalKey<FormState>();
  ProgressDialog pr;
  _SearchData _data = new _SearchData();
  String result,baseUrl;
  List<ContainerModel> listContainer = [];
  TextEditingController txtSearchController = new TextEditingController();
  TextRecognizer textDetector = TextRecognizer();
  final ImagePicker _picker = ImagePicker();

  @override
  void initState() {

    getData();
    // TODO: implement initState
    super.initState();
  }

  getData() async{
    baseUrl = await AppPrefs.getAppServer();
    print(baseUrl);
  }

  Future<Null> _readContainer() async {
    XFile image = await _picker.pickImage(source: ImageSource.camera);

    final inputImage = InputImage.fromFile(File(image.path));

    RecognizedText regText =
    await textDetector.processImage(inputImage);

    print("TEXT " + regText.blocks[0].lines[0].text);

    String text =
    regText.blocks[0].lines[0].text.replaceAll(" ", "");
    final regex = RegExp(r'^[A-Z]{4}\d{6}$');

    if (regex.hasMatch(text)) {
      txtSearchController.text = _getCheckDigit(text);
    } else {
      txtSearchController.text = text;
    }
  }

  String _getCheckDigit(String containerNo) {
    int value;
    int result = 0;

    for (int i = 0; i < 10; i++) {
      if (i < 4) {
        value = (11 * (containerNo[i].codeUnits[0] - 56) ~/ 10) + 1;
        print("value string = " + value.toString());
      } else {
        value = (containerNo[i].codeUnits[0] - 48);
        print("value interger = " + value.toString());
      }

      int newValue = value * math.pow(2, i);

      result += newValue;
      print("resutl = " + result.toString() + " " + i.toString());
    }

    int checkDigit = (result - (result ~/ 11) * 11).toInt();

    return containerNo + checkDigit.toString();
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData media = MediaQuery.of(context);
    pr = new ProgressDialog(context,type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);
    final Size screenSize = media.size;
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(color: Colors.white, ),
        title: Text("GATE OUT LIST", style: AppTheme.title_white,),
      ),
      body: Container(child: 
        Column(
          children: <Widget>[
            new Form(
                key: this._formKey_searchList,
                child: new TextFormField(
                  controller: txtSearchController,
                  keyboardType: TextInputType.text,
                  textInputAction: TextInputAction.search,
                  // Use email input type for emails.
                  textCapitalization: TextCapitalization.characters,
                  //
                  // inputFormatters: [
                  //   UpperCaseTextFormatter(),
                  // ],
                  decoration: new InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: AppTheme.colorPrimary, width: 1.0),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: AppTheme.colorMediumGrey, width: 1.0),
                      ),
                      suffixIcon: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          GestureDetector(
                              onTap: () {
                                _readContainer();
                              },
                              child: Image.asset(
                                "assets/images/ic_ocr.png",
                                height: 35,
                              )),                          IconButton(icon:Icon(FontAwesomeIcons.search), onPressed: this._submitForgotPass,),
                        ],
                      ),
                      hintText: 'Enter Container No.',
                      border: new OutlineInputBorder()),
                  autofocus: false,
                  validator: (String value) {
                    return null;
                  },
                  onSaved: (value) {
                    this._data.txtSearch = value;
                  },
                  onFieldSubmitted: (value){
                    _submitForgotPass();
                  },
                )),
            Expanded(
              child: ListView.builder(
                  itemCount: listContainer.length,
                  itemBuilder: (BuildContext context, int index) {
                    return ContainerListView(
                      callback: () {
                        startActivityWithResult(listContainer[index].containerNo, listContainer[index]);
                      },
                      post: listContainer[index],
                    );
                  }),
            )
          ],
        ),),
    );
  }

  startActivityWithResult(String containerNo, ContainerModel containerModel) async {
    var result = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (_) => DepotOutCondition(
              containerModel: containerModel,
            )));

    print("here $result");
    if (result == "success") {
      _submitForgotPass();
    }else{
      FocusScope.of(context).unfocus();
    }
  }

  Future<String> _scanQR() async {
    try {
      var qrResult = await BarcodeScanner.scan();
      print("Scanned");
      result = qrResult.rawContent;
      print(result);

      txtSearchController.text = result ;

    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.cameraAccessDenied) {
        setState(() {
          result = "Camera permission was denied";
          Fluttertoast.showToast(
              msg: result,
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0
          );

        });
      } else {
        setState(() {
          result == "Unknown Error $e";
          //Toast.show(result, context, duration: 2);
        });
      }
    } on FormatException {
      setState(() {
        result = "Cancelled";
        Fluttertoast.showToast(
            msg: result,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );

      });
    } catch (e) {
      setState(() {
        result == "Unknown Error $e";
        //Toast.show(result, context, duration: 2);
      });
    }
  }

  void _submitForgotPass() async {
    if (this._formKey_searchList.currentState.validate()) {
//      pr.show();
      _formKey_searchList.currentState.save(); // Save our form now.
      Map dataKey = {"Key":"SearchType","Value":"GateOut"
      };

      Map dataValue = {"Key":"ContainerNumber","Value":_data.txtSearch
      };

      List list = [];
      list.add(json.encode(dataKey));
      list.add(json.encode(dataValue));

      Map data = {'Key': 'Data',
        'Value':"$list"};

      final response =
      await _httpHelper.post(baseUrl,UrlConfig.APP_CONTAINER_LIST, json.encode(data));
      Response res = Response.fromJson(response);
      print("PKM" + res.statusCode.toString());
//      pr.hide();
      if (res.statusCode == 200) {
        if(res.results.length >0) {
          List<ContainerModel> tempResults;
          tempResults = [];
          List<dynamic> tempList = json.decode(res.results[0].value);

          for(int i= 0;i<tempList.length; i++){
            ContainerModel tempRes = ContainerModel.fromJson(tempList[i]);
            tempResults.add(tempRes);
          }

          setState(() {
            listContainer = tempResults;
            FocusScope.of(context).unfocus();

          });
        }
      } else {
        Fluttertoast.showToast(
            msg: res.errorMessage,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );

        setState(() {
          listContainer = [];
          FocusScope.of(context).unfocus();

        });

        if(res.errorMessage.contains("AccessToken"))
          new force_logout(context).doForceLogout();

      }

    }
  }

}

class ContainerListView extends StatelessWidget {
  final VoidCallback callback;
  final ContainerModel post;

  const ContainerListView({Key key, this.post, this.callback})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Padding(
          padding: const EdgeInsets.only(
              left: 24, right: 24, top: 8, bottom: 8),
          child: InkWell(
            splashColor: Colors.transparent,
            onTap: () {
              clearPrefsAll();
              callback();
            },
            child: Card(
              elevation: 3.0,
              child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(12.0)),
                  child: Container(
                    height: 100,
                    decoration: BoxDecoration(color: Colors.white,borderRadius: BorderRadius.circular(4)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(height:40,child: Image.asset("assets/images/container.png")),
                                  SizedBox(width: 12,),
                                  Expanded(child: Column(
                                    children: <Widget>[
                                      Expanded(child: Row(
                                        children: <Widget>[
                                          Expanded(child: Text(post.containerNo, style: AppTheme.title,)),
                                        ],
                                      ),
                                      ),
                                      Expanded(child: Row(
                                        children: <Widget>[
                                          Expanded(child: Text(post.typeSize, style: AppTheme.titleDesc,)),
                                          Expanded(child: Text(post.operatorCode,style: AppTheme.titleDesc ,textAlign: TextAlign.end)),
                                        ],
                                      ),
                                      ),
                                      Expanded(child: Row(
                                        children: <Widget>[
                                          Expanded(child: Text(DateFormat("dd-MM-yyyy").format(DateTime.parse(post.pickDropTime)), style: AppTheme.titleDesc,maxLines: 1,)),
                                          Text(DateFormat("HH : mm").format(DateTime.parse(post.pickDropTime)),style: AppTheme.titleDesc ,textAlign: TextAlign.end,maxLines: 1,),
                                        ],
                                      ),
                                      ),
                                      Expanded(child: Row(
                                        children: <Widget>[
                                          Expanded(child: Text(post.primeMoverId, style: AppTheme.titleDesc,maxLines: 1,)),
                                          Text(post.primeMoverRegNo,style: AppTheme.titleDesc ,textAlign: TextAlign.end,maxLines: 1,),
                                        ],
                                      ),
                                      ),
                                    ],
                                  ),),
                                  SizedBox(width: 30,),
                                  !post.isFileAttached ? Container(height:100,child: Center(child: Container(height:60,child: Image.asset("assets/images/camera.png")))):
                                  Container(height:100,child: Center(child: Container(height:60,child: Image.asset("assets/images/camera_taken.png")))),
                                ],
                              ),
                            )),
                      ],
                    ),

                  )
              ),
            ),
          ),
        )
    );
  }

  timeFormater(String lala){
    String dateFormate = DateFormat("dd-MM-yyyy").format(DateTime.parse(post.pickDropTime));
  }

  clearPrefsAll() {
    AppPrefs.setPosFront("");
    AppPrefs.setPosDoor("");
    AppPrefs.setPosLeft("");
    AppPrefs.setPosRight("");
    AppPrefs.setPosTop("");
    AppPrefs.setPosBottom("");
    AppPrefs.setPosInside("");
    AppPrefs.setPosSeal("");

    AppPrefs.setRemarkFront("");
    AppPrefs.setRemarkDoor("");
    AppPrefs.setRemarkLeft("");
    AppPrefs.setRemarkRight("");
    AppPrefs.setRemarkTop("");
    AppPrefs.setRemarkBottom("");
    AppPrefs.setRemarkInside("");
    AppPrefs.setRemarkSeal("");

    AppPrefs.setSavedSeal("");
  }

}
