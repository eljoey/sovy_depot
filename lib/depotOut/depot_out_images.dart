import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:barcode_scan2/barcode_scan2.dart';
import 'package:depot/styles/appTheme.dart';
import 'package:depot/utils/AppPrefs.dart';
import 'package:depot/utils/ShareMethod.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_mlkit_text_recognition/google_mlkit_text_recognition.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'dart:ui';
import 'dart:io' as Io;
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:path_provider/path_provider.dart';

class DepotOutImages extends StatefulWidget {
  final String title;
  final String savedPrefs;
  final String savedRemark;
  final String containerNo;
  final String savedSeal;
  final bool sealRequired;

  DepotOutImages({Key key, this.title, this.savedPrefs, this.savedRemark,this.containerNo,this.savedSeal,this.sealRequired =false})
      : super(key: key);

  @override
  _DepotOutImagesState createState() => _DepotOutImagesState();
}

class _DepotOutImagesState extends State<DepotOutImages> {
  List<Asset> images = List<Asset>();
  List<ByteData> byts = List<ByteData>();
  List<ByteData> byts2 = List<ByteData>();
  List<ByteData> tempCamera = List<ByteData>();
  List<String> saved = List<String>();
  final GlobalKey<FormState> _formKey_photoList = new GlobalKey<FormState>();
  TextEditingController remark = new TextEditingController();
  TextEditingController tecSeal = new TextEditingController();
  TextRecognizer textDetector = TextRecognizer();
  String _error = 'No Error Dectected';
  String prefSaved = "";
  String toSaved = "";
  String et_remark= "";
  String seal_no= "";
  String result;
  final ImagePicker _picker = ImagePicker();

  @override
  void initState() {
    if (widget.savedPrefs.length > 0) {
      getData();
    }

    if(widget.savedSeal.length > 0){
      tecSeal.text = widget.savedSeal;
    }

    if(widget.savedRemark.length > 0){
      remark.text = widget.savedRemark;
    }

    super.initState();
  }

  showAlertDialog(BuildContext context) {
    Widget optionCamera = SimpleDialogOption(
      child: const Padding(
          padding: EdgeInsets.all(2.0),
          child: Text(
            'Take Picture',
            style: AppTheme.title,
          )),
      onPressed: () {
        print('camera');
        Navigator.of(context).pop();
        getImageFile(ImageSource.camera);
      },
    );
    Widget optionPick = SimpleDialogOption(
      child: const Padding(
          padding: EdgeInsets.all(2.0),
          child: Text(
            'Choose from Gallery',
            style: AppTheme.title,
          )),
      onPressed: () {
        print('gallery');
        //getImage();
       getImageFile(ImageSource.gallery);
        Navigator.of(context).pop();
        loadAssets();
      },
    );
    Widget optionCancel = SimpleDialogOption(
      child: const Padding(
          padding: EdgeInsets.all(2.0),
          child: Text(
            'Cancel',
            style: AppTheme.title,
          )),
      onPressed: () {
        print('cancel');
        Navigator.of(context).pop();
      },
    );

    SimpleDialog dialog = SimpleDialog(
      //title: const Text('Choose'),
      children: <Widget>[
        optionCamera,
        Divider(),
        optionPick,
        Divider(),
        optionCancel,
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return dialog;
      },
    );
  }

  getImageFile(ImageSource source) async {
    //Clicking or Picking from Gallery

    XFile imageX = await _picker.pickImage(source: source);
    File image = File(imageX.path);

    //Cropping the image
//    File croppedFile = await ImageCropper.cropImage(
//      sourcePath: image.path,
//      maxWidth: 512,
//      maxHeight: 512,
//    );

    //Compress the image
    //print("cropped ${croppedFile.path}");
    String compress_path = "${image.parent.path}/compressed_${new DateTime.now().millisecondsSinceEpoch}.jpg";
    //print("compress $compress_path");
    var result = await FlutterImageCompress.compressAndGetFile(
      image.path,
      compress_path,
      minHeight: 720,
      minWidth: 720,
      quality: 90,
    );

    setState(() {
      var imageTaken= result.readAsBytesSync();
      tempCamera.add(imageTaken.buffer.asByteData());
      //print(userData._userTmpImage.lengthSync());
      byts2.add(imageTaken.buffer.asByteData());
    });
  }

  getData() async {
    List<String> listSplit = widget.savedPrefs.split(",");
    print(listSplit);

    for (int i = 0; i < listSplit.length; i++) {
      var list = base64Decode(listSplit[i]);
      byts.add(list.buffer.asByteData());
    }

    setState(() {

    });
  }

  Widget buildGridView() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(4.0),
          child: Text("Add Image", style: AppTheme.title),
        ),
        Expanded(
          child: GridView.count(
            crossAxisCount: 3,
            children: List.generate(byts2.length + 2, (index) {
              if (index == byts2.length) {
                return Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: GestureDetector(
                      onTap: () {
                        getImageFile(ImageSource.camera);
                      },
                      child: Container(
                        height: 300,
                        width: 300,
                        decoration: BoxDecoration(
                            border: Border.all(color: AppTheme.colorLightGrey)),
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Center(
                            child: Icon(Icons.camera_alt,size: 60,color: AppTheme.colorMediumGrey,),
                          ),
                        ),
                      )),
                );
              } else if(index == byts2.length +1 ){
                return Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: GestureDetector(
                      onTap: () {
                        loadAssets();
                      },
                      child: Container(
                        height: 300,
                        width: 300,
                        decoration: BoxDecoration(
                            border: Border.all(color: AppTheme.colorLightGrey)),

                          child: Center(
                            child: Icon(Icons.folder,size: 60,color: AppTheme.colorMediumGrey),
                        ),
                      )),
                );
              } else{
                ByteData bts2 = byts2[index];
                return Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Stack(
                    children: <Widget>[
                      Container(
                          decoration: BoxDecoration(
                              border: Border.all(color: AppTheme.colorMediumGrey),color: AppTheme.colorLightGrey),
                          width: double.infinity,
                          height: double.infinity,
                          child: Image.memory(bts2.buffer.asUint8List(),
                          fit: BoxFit.cover
                          )),
//                      Positioned(
//                        right: 2,
//                        top: 2,
//                        child: GestureDetector(
//                            onTap: () {
//                              deleteSaved(index);
//                            },
//                            child: Text((base64Encode(bts2.buffer.asUint8List())).length.toString())),
//                      ),
                    ],

                  ),
                );
              }
            }),
          ),
        ),
      ],
    );
  }

  Widget buildGridViewSaved() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(4.0),
          child: Text(
            "Saved Image (${widget.title})",
            style: AppTheme.title,
          ),
        ),
        Expanded(
          child: GridView.count(
            crossAxisCount: 3,
            children: List.generate(byts.length, (index) {
              ByteData bts = byts[index];
              return Padding(
                padding: const EdgeInsets.all(4.0),
                child: Stack(
                  children: <Widget>[
                    Container(
                        decoration: BoxDecoration(
                            border: Border.all(color: AppTheme.colorMediumGrey),color: AppTheme.nearlyWhite),                        width: double.infinity,
                        height: double.infinity,
                        child: Image.memory(
                          bts.buffer.asUint8List(),
                          fit: BoxFit.cover,
                        )),
                    Positioned(
                      right: 2,
                      top: 2,
                      child: GestureDetector(
                          onTap: () {
                            deleteSaved(index);
                          },
                          child: Icon(
                            Icons.delete,
                            color: Colors.red,
                            size: 27,
                          )),
                    ),
//                    Positioned(
//                      right: 2,
//                      top: 2,
//                      child: GestureDetector(
//                          onTap: () {
//                            deleteSaved(index);
//                          },
//                          child: Text((base64Encode(bts.buffer.asUint8List())).length.toString())),
//                    ),
                  ],
                ),
              );
            }),
          ),
        ),
      ],
    );
  }

  Future<void> loadAssets() async {
    List<Asset> resultList = List<Asset>();
    String error = 'No Error Dectected';
    List<ByteData> tempByte = List<ByteData>();

    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 300,
        enableCamera: true,
        selectedAssets: images,
        cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: MaterialOptions(
          actionBarColor: "#8BC73F",
          actionBarTitle: "SOVY DEPOT App",
          allViewTitle: "All Photos",
          useDetailsView: false,
          selectCircleStrokeColor: "#ffffff",
        ),
      );
    } on Exception catch (e) {
      error = e.toString();
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    tempByte = new List<ByteData>();
    for (int i = 0; i < resultList.length; i++) {
      ByteData byss = await resultList[i].getByteData(quality:100 );
      tempByte.add(byss);
    }

    setState(() {
      images = resultList;
      byts2 = tempByte;
      byts2.addAll(tempCamera);
//      saveImage(tempByte.toString());
      _error = error;
    });
  }

  saveImage(List<ByteData> tempList, List<ByteData> tempList2) async{
    if(_formKey_photoList.currentState.validate()) {
      _formKey_photoList.currentState.save();
      String newString = "";
      String newString2 = "";
      String tempTest;

      for (int i = 0; i < tempList.length; i++) {
        var buffer = tempList[i].buffer;
        List<int> list = buffer.asUint8List(
            tempList[i].offsetInBytes, tempList[i].lengthInBytes);
        var decodedImage = await decodeImageFromList(list);
        print(" lala" + decodedImage.width.toString());
        print(" lala" + decodedImage.height.toString());

        var result = await FlutterImageCompress.compressWithList(
          list,
          minHeight: 720,
          minWidth: 720,
          quality: 90,
          rotate: 0,
        );
        print(list.length);
        print(result.length);
//      _createFileFromString(Uint8List.fromList(result));
        String tempTest = base64Encode(result);

//      if (decodedImage.width <= decodedImage.height) {
//          img.Image image = img.Image.fromBytes(360,
//              (decodedImage.width ~/ (decodedImage.height / 360)),
//              list);
//          img.Image imageResized = img.copyResize(image,width: 360);
//          List<int> imageBytes = imageResized.getBytes();
//          tempTest = base64Encode(imageBytes);
//      }else{
//          img.Image image = img.Image.fromBytes(
//              (decodedImage.width ~/ (decodedImage.height / 360)), 360,
//              list);
//          List<int> imageBytes = image.getBytes();
//          tempTest = base64Encode(imageBytes);
//      }

        if (newString.length == 0) {
          newString = tempTest;
        } else {
          newString = newString + "," + tempTest;
        }
      }

      for (int i = 0; i < tempList2.length; i++) {
        var buffer2 = tempList2[i].buffer;
        var list2 = buffer2.asUint8List(
            tempList2[i].offsetInBytes, tempList2[i].lengthInBytes);
        print(list2);
        String tempTest2 = base64Encode(list2);
        print(tempTest2.length.toString());

        if (newString2.length == 0) {
          newString2 = tempTest2;
        } else {
          newString2 = newString2 + "," + tempTest2;
        }
      }

      switch (widget.title) {
        case "FRONT":
          {
//          if (widget.savedPrefs != null) {
//            if (widget.savedPrefs.length > 0) {
//              if(newString.length >0) {
//                AppPrefs.setPosFront(newString + "," + widget.savedPrefs);
//              }else{
//                AppPrefs.setPosFront(widget.savedPrefs);
//              }
//            } else {
//              AppPrefs.setPosFront(newString);
//            }
//          } else {
//            if(newString.length>0) {
//              AppPrefs.setPosFront(newString);
//            }
//          }
            if (newString.length > 0) {
              toSaved = newString;
            }

            if (newString2.length > 0) {
              if (toSaved.length > 0) {
                toSaved = toSaved + "," + newString2;
              } else {
                toSaved = newString2;
              }
            }

            AppPrefs.setPosFront(toSaved);
            AppPrefs.setRemarkFront(et_remark);
          }
          break;

        case "DOOR":
          {
            if (newString.length > 0) {
              toSaved = newString;
            }

            if (newString2.length > 0) {
              if (toSaved.length > 0) {
                toSaved = toSaved + "," + newString2;
              } else {
                toSaved = newString2;
              }
            }

            AppPrefs.setPosDoor(toSaved);
            AppPrefs.setRemarkDoor(et_remark);
          }
          break;

        case "LEFT":
          {
            if (newString.length > 0) {
              toSaved = newString;
            }

            if (newString2.length > 0) {
              if (toSaved.length > 0) {
                toSaved = toSaved + "," + newString2;
              } else {
                toSaved = newString2;
              }
            }

            AppPrefs.setPosLeft(toSaved);
            AppPrefs.setRemarkLeft(et_remark);
          }
          break;

        case "RIGHT":
          {
            if (newString.length > 0) {
              toSaved = newString;
            }

            if (newString2.length > 0) {
              if (toSaved.length > 0) {
                toSaved = toSaved + "," + newString2;
              } else {
                toSaved = newString2;
              }
            }

            AppPrefs.setPosRight(toSaved);
            AppPrefs.setRemarkRight(et_remark);
          }
          break;

        case "TOP":
          {
            if (newString.length > 0) {
              toSaved = newString;
            }

            if (newString2.length > 0) {
              if (toSaved.length > 0) {
                toSaved = toSaved + "," + newString2;
              } else {
                toSaved = newString2;
              }
            }

            AppPrefs.setPosTop(toSaved);
            AppPrefs.setRemarkTop(et_remark);
          }
          break;

        case "BOTTOM":
          {
            if (newString.length > 0) {
              toSaved = newString;
            }

            if (newString2.length > 0) {
              if (toSaved.length > 0) {
                toSaved = toSaved + "," + newString2;
              } else {
                toSaved = newString2;
              }
            }

            AppPrefs.setPosBottom(toSaved);
            AppPrefs.setRemarkBottom(et_remark);
          }
          break;

        case "INSIDE":
          {
            if (newString.length > 0) {
              toSaved = newString;
            }

            if (newString2.length > 0) {
              if (toSaved.length > 0) {
                toSaved = toSaved + "," + newString2;
              } else {
                toSaved = newString2;
              }
            }

            AppPrefs.setPosInside(toSaved);
            AppPrefs.setRemarkInside(et_remark);
          }
          break;

        case "SEAL":
          {
            if (newString.length > 0) {
              toSaved = newString;
            }

            if (newString2.length > 0) {
              if (toSaved.length > 0) {
                toSaved = toSaved + "," + newString2;
              } else {
                toSaved = newString2;
              }
            }

            AppPrefs.setPosSeal(toSaved);
            AppPrefs.setRemarkSeal(et_remark);
            AppPrefs.setSavedSeal(seal_no);
          }
          break;
      }


      Navigator.pop(context, "saved");
    }
  }

  Future<Null> _read() async{

    XFile image = await _picker.pickImage(source: ImageSource.camera);

    final inputImage = InputImage.fromFile(File(image.path));

    RecognizedText regText = await textDetector.processImage(inputImage);

    print("TEXT "+regText.blocks[0].lines[0].text);

    tecSeal.text = regText.blocks[0].lines[0].text.replaceAll(" ", "");

  }

  Future<String> _scanQR() async {
    try {
      var qrResult = await BarcodeScanner.scan();
      print("Scanned");
      result = qrResult.rawContent;
      print(result);

      tecSeal.text = result;
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.cameraAccessDenied) {
        setState(() {
          result = "Camera permission was denied";
          Fluttertoast.showToast(
              msg: result,
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0
          );

        });
      } else {
        setState(() {
          result == "Unknown Error $e";
          //Toast.show(result, context, duration: 2);
        });
      }
    } on FormatException {
      setState(() {
        result = "Cancelled";
        Fluttertoast.showToast(
            msg: result,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );

      });
    } catch (e) {
      setState(() {
        result == "Unknown Error $e";
        //Toast.show(result, context, duration: 2);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
//    SimplePermissions.requestPermission(Permission.WriteExternalStorage);
    return WillPopScope(
      onWillPop: (byts.length> 0 || byts2.length > 0) ? _willPopCallback:_Pop,
      child: Scaffold(
        appBar: new AppBar(
          backgroundColor: AppTheme.colorPrimary,
          title: Text(
            widget.title + " - " + widget.containerNo,
            style: AppTheme.title_white,
          ),
          leading: BackButton(
            color: Colors.white,
          ),
        ),
        body: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 8.0, left: 8, right: 8),
              child: Form(
                key: this._formKey_photoList,
                child: Column(
                  children: [
                    widget.title == "SEAL"? Padding(
                      padding: const EdgeInsets.only(bottom:4.0),
                      child: TextFormField(
//            controller: txtSearchController,
                        keyboardType: TextInputType.text,
                        controller: tecSeal,
                        // Use email input type for emails.
                        // inputFormatters: [UpperCaseTextFormatter()],
                        textCapitalization: TextCapitalization.characters,
                        validator: (value){
                          if( widget.sealRequired){
                            if(value.length<=0){
                              return "Seal No cannot be empty.";
                            }
                          }
                          return null;
                        },
                        decoration: new InputDecoration(
                            focusedBorder: OutlineInputBorder(
                              borderSide:
                              BorderSide(color: AppTheme.colorPrimary, width: 1.0),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide:
                              BorderSide(color: AppTheme.colorMediumGrey, width: 1.0),
                            ),
                            labelText: "Seal No: ",
                            suffixIcon: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                GestureDetector(
                                    onTap:(){
                                      _read();
                                    },child: Image.asset('assets/images/ic_ocr.png',height: 40,width: 40,)),
                                GestureDetector(
                                    onTap:(){
                                      _scanQR();
                                    },child: Image.asset('assets/images/ic_qr.png',height: 40,width:40,)),
                              ],
                            ),
                            border: new OutlineInputBorder()),
                        onSaved: (value) {
                          seal_no = value;
                        },            ),
                    ):SizedBox(),
                    TextFormField(
//            controller: txtSearchController,
                      keyboardType: TextInputType.text,
                      // Use email input type for emails.
                      decoration: new InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: AppTheme.colorPrimary, width: 1.0),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: AppTheme.colorMediumGrey, width: 1.0),
                          ),
                          prefixIcon: Icon(Icons.chat_bubble_outline),
                          hintText: "Remark",
                          border: new OutlineInputBorder()),
                      initialValue: widget.savedRemark,
                      onSaved: (value) {
                        et_remark = value;
                      },            ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
              child: Container(
                color: AppTheme.colorSecondary,
                height: 2,
                width: MediaQuery.of(context).size.width,
              ),
            ),

            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(left: 8.0, right: 8),
                child: buildGridView(),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
              child: Container(
                color: AppTheme.colorSecondary,
                height: 2,
                width: MediaQuery.of(context).size.width,
              ),
            ),
            byts.length > 0
                ? Expanded(
                    child: Padding(
                    padding: const EdgeInsets.only(left: 8.0, right: 8),
                    child: buildGridViewSaved(),
                  ))
                : SizedBox(),
            byts.length > 0
                ? Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Container(
                      color: AppTheme.colorSecondary,
                      height: 2,
                      width: MediaQuery.of(context).size.width,
                    ),
                  )
                : SizedBox(),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Container(
                  color: Colors.transparent,
                  height: 50.0,
                  width: 170.0,
                  margin: new EdgeInsets.only(bottom: 10.0, top: 10.0),
                  child: Card(
                    elevation: 5.0,
                    // this field changes the shadow of the card 1.0 is default
                    child: new RaisedButton(
                      padding: const EdgeInsets.all(12.0),
                      child: new Text(
                        'Save',
                        style: AppTheme.btnText,
                      ),
                      onPressed: () {
                        saveImage(byts2, byts);
                      },
                      color: AppTheme.colorPrimary,
//                              shape: RoundedRectangleBorder(
//                                  borderRadius: new BorderRadius.circular(10.0),
//                                  side: BorderSide(color: Colors.transparent)),
                    ),
                  ),
                ),
              ],
            ),
//          RaisedButton(
//            child: Text("Save Image"),
//            onPressed: () {
//              saveImage(byts2,byts);
//              print("saved");
//            },
//          ),
          ],
        ),
      ),
    );
  }

  deleteSaved(int index) {
    byts.removeAt(index);
    String newString = "";

    for (int i = 0; i < byts.length; i++) {
      var buffer = byts[i].buffer;
      var list =
          buffer.asUint8List(byts[i].offsetInBytes, byts[i].lengthInBytes);
      print(list);
      String tempTest = base64Encode(list);
      print(tempTest);

      if (newString.length == 0) {
        newString = tempTest;
      } else {
        newString = newString + "," + tempTest;
      }
    }

    print(newString);

    setState(() {
      prefSaved = newString;
      byts = byts;
    });
  }

  Future<String> _createFileFromString(Uint8List bytes) async {
    String dir = (await getApplicationDocumentsDirectory()).path;
    String fullPath = '$dir/abc.png';
    print("local file full path ${fullPath}");
    File file = File(fullPath);
    await file.writeAsBytes(bytes);
    print(file.path);

    final result = await ImageGallerySaver.saveImage(bytes);
    print(result);

    return file.path;
  }


  Future<bool> _willPopCallback() async {
    return (await showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Are you sure?',style: AppTheme.titleDlg),
        content: new Text('Do you want to leave without save?',style: AppTheme.titleDescDlg),
        actions: <Widget>[

          new FlatButton(
            onPressed: () => Navigator.of(context).pop(true),
            child: new Text('Discard',style: AppTheme.titleBtnDlgRed,),
          ),
        ],
      ),
    )) ?? false;
  }

  Future<bool> _Pop() async {
    return true;
  }

}
