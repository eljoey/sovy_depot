
import 'package:depot/styles/aboutDetails.dart';
import 'package:depot/styles/appTheme.dart';
import 'package:depot/utils/AppPrefs.dart';
import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';

class AboutScreen extends StatefulWidget {
  @override
  _AboutScreenState createState() => _AboutScreenState();
}

class _AboutScreenState extends State<AboutScreen> {
  String appName ;
  String packageName ;
  String version ;
  String buildNumber;

  @override
  void initState() {
    // TODO: implement initState
    getVersion();
    super.initState();
  }

  getVersion() async{

    print("Checking");

    PackageInfo packageInfo = await PackageInfo.fromPlatform();

    setState(() {
      appName = packageInfo.appName;
      packageName = packageInfo.packageName;
//      print(version);
      version = packageInfo.version;

      buildNumber = packageInfo.buildNumber;
    });

  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: AppTheme.nearlyWhite,
      appBar: new AppBar(
        leading: BackButton(
          color: Colors.white,
        ),
        title: new Text(
          "About",
          style: AppTheme.title_white,),
        backgroundColor: AppTheme.colorPrimary,
      ),
      body: Container
        (child: Column(
        children: <Widget>[
          DetailsDescAbout(titleTxt: "Title",subTxt: "Sovy Depot",),
          DetailsDescAbout(titleTxt: "Version",subTxt: "$version",),
          DetailsDescAbout(titleTxt: "Company",subTxt: "NM Sovy Technology Sdn. Bhd.",),
          DetailsDescAbout(titleTxt: "Website",subTxt: "www.sovy.com.my",),
          Expanded(child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(child: GestureDetector(
                    onTap: (){
//                      deactivateDevice();
                      showDialog(
                          context: context,
                          builder: (context) => new AlertDialog(
                            title: new Text('Are you sure?',style: AppTheme.titleDlg),
                            content: new Text('Do you want to re-activate device?',style: AppTheme.titleDescDlg),
                            actions: <Widget>[

                              new FlatButton(
                                onPressed: () => deactivateDevice(),
                                child: new Text('ReActivate',style: AppTheme.titleBtnDlgRed,),
                              ),
                            ],
                          ));
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.vertical(top: Radius.circular(12))
                      ),
                      height: 60,
                      child: Center(
                        child: Text("Re-Activate Device",style: TextStyle(color: Colors.white),),
                      ),
                    ),
                  ),)
                ],
              )
            ],
          ),)
        ],
      ),),
    );
  }

  deactivateDevice() {
    AppPrefs.setAppServer("");
    AppPrefs.setIsActivated(false);
    Navigator.pushReplacementNamed(context, "/main");
  }
}

