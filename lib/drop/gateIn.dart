import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:barcode_scan2/barcode_scan2.dart';
import 'package:depot/drop/gateInPosition.dart';
import 'package:depot/models/response.dart';
import 'package:depot/styles/appTheme.dart';
import 'package:depot/utils/ApiBaseHelper.dart';
import 'package:depot/utils/AppPrefs.dart';
import 'package:depot/utils/ShareMethod.dart';
import 'package:depot/utils/UrlConfig.dart';
import 'package:depot/utils/force_logout.dart';
import 'package:depot/utils/progress_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_mlkit_text_recognition/google_mlkit_text_recognition.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:depot/models/container.dart';
import 'package:flutter/services.dart';
import 'dart:math' as math;

import '../models/gate_in_model.dart';

class GateIn extends StatefulWidget {
  @override
  _GateInState createState() => _GateInState();
}

class _GateInData {
  String _containerNo = '';
  String _operatorCode = '';
  String _primeMoverRegNo = '';
  String _primeMoverId = '';
  String _haulier = '';
  String _typeSize = '';
  String _containerStatus = '';
  String _containerGrade = '';
  String _iSOCode = '';
  double _mGW = 0;
  double _tareWeight = 0;
  String _manufactureDate = '';
  String _remark = '';
  String _sealNo = '';
}

class _GateInState extends State<GateIn> {
  ApiBaseHelper _httpHelper = ApiBaseHelper();
  final GlobalKey<FormState> _formKey_searchList = new GlobalKey<FormState>();
  final GlobalKey<FormState> _keyContainer = new GlobalKey<FormState>();
  ProgressDialog pr;
  _GateInData _data = new _GateInData();
  String result, baseUrl, defaultStatus;
  List<ContainerModel> listContainer = [];
  TextEditingController txtSearchController = new TextEditingController();
  FocusNode focusContainer = new FocusNode();
  List<Widget> listISO = List(), listGrade = List(), listStatus = List();
  bool _isCheck,
      _hasCalled = false,
      _isSuccess = false,
      _isReadonly = false,
      _isGateInCreated = false;
  List gradeTemp = [],
      jsonTemp = [],
      statusTemp = [],
      defaultTemp = [],
      operatorTemp = [],
      listHaulier = [],
      tempListHaulier = [],
      listOperator = [],
      tempListOperator = [];
  GateInModel gateInModel = new GateInModel();

  bool _isRemarkFocused = false;

  final dobFormat = DateFormat("yyyy/MM");
  final dobFormatStore = DateFormat("yyyy-MM-ddThh:mm:ss");
  String gateInDoc = "", _checkContainer;
  FocusNode focusNode = new FocusNode();
  final ImagePicker _picker = ImagePicker();

  TextEditingController textController = new TextEditingController();
  TextEditingController operatorController = new TextEditingController();

  ScrollController _scrollController = new ScrollController();

  TextEditingController tecContainerNo = new TextEditingController();
  TextEditingController tecOperator = new TextEditingController();
  TextEditingController tecPreGateIn = new TextEditingController();
  TextEditingController tecPrimeReg = new TextEditingController();
  TextEditingController tecPrimeId = new TextEditingController();
  TextEditingController tecHaulier = new TextEditingController();
  TextEditingController tecTypeSize = new TextEditingController();
  TextEditingController tecGateInDocNo =
      new TextEditingController(); // Return value for success gate in
  TextEditingController tecContainerStatus = new TextEditingController();
  TextEditingController tecContainerGrade = new TextEditingController();
  TextEditingController tecISOCode = new TextEditingController();
  TextEditingController tecMGW = new TextEditingController();
  TextEditingController tecTareWeight = new TextEditingController();
  TextEditingController tecManufactureDate = new TextEditingController();
  TextEditingController tecRemark = new TextEditingController();
  TextEditingController tecSealNo = new TextEditingController();
  String _text = "TEXT";
  bool showPreGateIn = false;

  TextRecognizer textDetector =
      TextRecognizer();

  _GateInState();

  // int _ocrCamera = FlutterMobileVision.CAMERA_BACK;
  Future<Null> _read(TextEditingController tec) async {
    XFile image = await _picker.pickImage(source: ImageSource.camera);

    final inputImage = InputImage.fromFile(File(image.path));

    RecognizedText regText =
        await textDetector.processImage(inputImage);

    print("TEXT " + regText.blocks[0].lines[0].text);

    tec.text = regText.blocks[0].lines[0].text.replaceAll(" ", "").toUpperCase();
  }

  Future<Null> _readContainer() async {
    XFile image = await _picker.pickImage(source: ImageSource.camera);

    final inputImage = InputImage.fromFile(File(image.path));

    RecognizedText regText =
        await textDetector.processImage(inputImage);

    print("TEXT " + regText.blocks[0].lines[0].text);

    String text = regText.blocks[0].lines[0].text.replaceAll(" ", "");
    final regex = RegExp(r'^[A-Z]{4}\d{6}$');

    if (regex.hasMatch(text)) {
      tecContainerNo.text = _getCheckDigit(text).toUpperCase();
    } else {
      tecContainerNo.text = text.toUpperCase();
    }
  }

  bool _checkDigit(String containerNo) {
    int value;
    int result = 0;

    if (containerNo.length != 11) {
      Fluttertoast.showToast(
          msg: "Incorrect Container No.",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    } else {
      for (int i = 0; i < 10; i++) {
        if (i < 4) {
          value = (11 * (containerNo[i].codeUnits[0] - 56) ~/ 10) + 1;
          print("value string = " + value.toString());
        } else {
          value = (containerNo[i].codeUnits[0] - 48);
          print("value interger = " + value.toString());
        }

        int newValue = value * math.pow(2, i);

        result += newValue;
        print("resutl = " + result.toString() + " " + i.toString());
      }
    }

    int checkDigit = (result - (result ~/ 11) * 11).toInt();

    if (int.parse(containerNo[10]) != checkDigit) {
      return false;
    } else {
      return true;
    }
  }

  String _getCheckDigit(String containerNo) {
    int value;
    int result = 0;

    for (int i = 0; i < 10; i++) {
      if (i < 4) {
        value = (11 * (containerNo[i].codeUnits[0] - 56) ~/ 10) + 1;
        print("value string = " + value.toString());
      } else {
        value = (containerNo[i].codeUnits[0] - 48);
        print("value interger = " + value.toString());
      }

      int newValue = value * math.pow(2, i);

      result += newValue;
      print("resutl = " + result.toString() + " " + i.toString());
    }

    int checkDigit = (result - (result ~/ 11) * 11).toInt();

    return containerNo + checkDigit.toString();
  }

  @override
  void initState() {
    getData();
    // TODO: implement initState
    super.initState();
  }

  popDialog(String title, List<Widget> list) {
    showCupertinoModalPopup(
      context: context,
      builder: (BuildContext context) => CupertinoActionSheet(
          title: Text(title),
          actions: list,
          cancelButton: CupertinoActionSheetAction(
            child: const Text('Cancel'),
            isDefaultAction: true,
            onPressed: () {
              Navigator.pop(context);
            },
          )),
    );
  }

  void _settingModalBottomSheet(context, var list) {
    showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(15.0)),
        ),
        context: context,
        builder: (BuildContext bc) {
          return DraggableScrollableSheet(
              expand: false,
              builder:
                  (BuildContext context, ScrollController scrollController) {
                return Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Text(
                        "ISO Code Options:",
                        style: TextStyle(color: Colors.black54),
                      ),
                    ),
                    Expanded(
                      child: Container(
                          width: double.infinity,
                          child: Padding(
                            padding: const EdgeInsets.only(
                                top: 8.0, left: 8, right: 8),
                            child: GridView.count(
                              childAspectRatio: 2,
                              crossAxisCount: 3,
                              children: List.generate(list.length, (index) {
                                return GestureDetector(
                                  onTap: () {
                                    Navigator.of(context).pop();
                                    setState(() {
                                      tecISOCode.text = list[index]['Key'];
                                      _data._iSOCode = list[index]['Key'];
                                      tecTypeSize.text = list[index]['Value'];
                                    });
                                  },
                                  child: Card(
                                    child:
                                        Center(child: Text(list[index]['Key'])),
                                  ),
                                );
                              }),
                            ),
                          )),
                    ),
                  ],
                );
              });
        });
  }

  reset() {
    FocusScope.of(context).unfocus();
    _scrollController.animateTo(0,
        duration: Duration(milliseconds: 500), curve: Curves.ease);
    setState(() {
      _data = new _GateInData();

      tecContainerStatus.text = defaultTemp[0]['Value'];
      for (int i = 0; i < statusTemp.length; i++) {
        if (defaultTemp[0]['Value'] == statusTemp[i]["Key"]) {
          tecContainerStatus.text = statusTemp[i]['Value'];
          _data._containerStatus = statusTemp[i]['Key'];
        }
      }
      _isSuccess = false;
      _isReadonly = false;
      _checkContainer = "";
      tecContainerNo.text = "";
      tecTypeSize.text = "";
      tecPrimeId.text = '';
      tecPrimeReg.text = "";
      tecHaulier.text = "";
      tecOperator.text = "";
      tecPreGateIn.text = "";
      tecGateInDocNo.text = "";
      tecContainerGrade.text = "";
      tecISOCode.text = "";
      tecMGW.text = "";
      tecTareWeight.text = "";
      tecManufactureDate.text = "";
      tecSealNo.text = "";
      tecRemark.text = "";
      gateInDoc = "";
      _isCheck = null;
    });
  }

  _showDatePicker() {
    showCupertinoModalPopup(
        context: context,
        builder: (BuildContext builder) {
          return Container(
              height: MediaQuery.of(context).copyWith().size.height * 0.25,
              color: Colors.white,
              child: CupertinoDatePicker(
                mode: CupertinoDatePickerMode.date,
                onDateTimeChanged: (value) {
                  setState(() {
                    tecManufactureDate.text = dobFormat.format(value);
                    this._data._manufactureDate = dobFormatStore.format(value);
                  });
                },
                initialDateTime: DateTime.now(),
                minimumDate: new DateTime(2005, 1, 1),
                maximumDate: new DateTime.now(),
              ));
        });
  }

  getData() async {
    baseUrl = await AppPrefs.getAppServer();
    String lookUp = await AppPrefs.getLookup();
    log("Lookup -- $lookUp");

    jsonTemp = json.decode(json.decode(lookUp))['ISOCodes'];
    statusTemp = json.decode(json.decode(lookUp))['GIContainerStatus'];
    gradeTemp = json.decode(json.decode(lookUp))['ContainerGrade'];
    listOperator = json.decode(json.decode(lookUp))['OperatorCode'];
    defaultTemp = json.decode(json.decode(lookUp))['DefaultContainerStatus'];
    listHaulier = json.decode(json.decode(lookUp))['HaulierCode'];

    setState(() {
      tecContainerStatus.text = defaultTemp[0]['Value'];
      for (int i = 0; i < statusTemp.length; i++) {
        if (defaultTemp[0]['Value'] == statusTemp[i]["Key"]) {
          tecContainerStatus.text = statusTemp[i]['Value'];
          _data._containerStatus = statusTemp[i]['Key'];
        }
      }
    });

    for (int i = 0; i < jsonTemp.length; i++) {
      listISO.add(CupertinoActionSheetAction(
        child: Text(jsonTemp[i]['Key']),
        onPressed: () {
          setState(() {
            tecISOCode.text = jsonTemp[i]['Key'];
            _data._iSOCode = jsonTemp[i]['Key'];
            tecTypeSize.text = jsonTemp[i]['Value'];
//            searchHint = jsonTemp[i]['Value'];
            Navigator.pop(context);
          });
        },
      ));
    }

    for (int i = 0; i < statusTemp.length; i++) {
      listStatus.add(CupertinoActionSheetAction(
        child: Text(statusTemp[i]['Value']),
        onPressed: () {
          setState(() {
            tecContainerStatus.text = statusTemp[i]['Value'];
            _data._containerStatus = statusTemp[i]['Key'];

//            searchHint = jsonTemp[i]['Value'];
            Navigator.pop(context);
          });
        },
      ));
    }

//     for (int i = 0; i < operatorTemp.length; i++) {
//       listOperator.add(CupertinoActionSheetAction(
//         child: Text(operatorTemp[i]['Value']),
//         onPressed: () {
//           setState(() {
//             tecOperator.text = operatorTemp[i]['Value'];
//             _data._operatorCode = operatorTemp[i]['Key'];
//
// //            searchHint = jsonTemp[i]['Value'];
//             Navigator.pop(context);
//           });
//         },
//       ));
//     }

    for (int i = 0; i < gradeTemp.length; i++) {
      listGrade.add(CupertinoActionSheetAction(
        child: Text(gradeTemp[i]['Value']),
        onPressed: () {
          setState(() {
            tecContainerGrade.text = gradeTemp[i]['Value'];
            _data._containerGrade = gradeTemp[i]['Key'];
//            searchHint = jsonTemp[i]['Value'];
            Navigator.pop(context);
          });
        },
      ));
    }

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData media = MediaQuery.of(context);
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);
    // final Size screenSize = media.size;
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(
          color: Colors.white,
        ),
        title: Text(
          "DROP",
          style: AppTheme.title_white,
        ),
        actions: <Widget>[
          InkWell(
              onTap: () {
                reset();
              },
              child: Icon(
                Icons.restore_page_outlined,
                color: Colors.white,
                size: 30,
              )),
          SizedBox(
            width: 16,
          ),
        ],
      ),
      body: new Form(
        key: this._formKey_searchList,
        child: SingleChildScrollView(
          controller: _scrollController,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 10,
                ),
                gateInDoc.length > 0
                    ? Padding(
                        padding: const EdgeInsets.only(bottom: 20.0),
                        child: Row(
                          children: [
                            Container(
                              color: Colors.black26,
                              child: Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Text(
                                  gateInDoc.replaceAll("\"", ""),
                                  maxLines: 1,
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Expanded(
                                child: Row(
                              children: [
                                GestureDetector(
                                    onTap: () {
                                      clearPrefsAll();
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (_) => GateInPosition(
                                                    containerNo:
                                                        _data._containerNo,
                                                    docNo: gateInDoc.replaceAll(
                                                        "\"", ""),
                                                  )));
                                    },
                                    child: Image.asset(
                                      'assets/images/camera.png',
                                      height: 30,
                                      width: 30,
                                    )),
                              ],
                            )),
                          ],
                        ),
                      )
                    : SizedBox(),
                Focus(
                  onFocusChange: (hasFocus) {
                    if (!hasFocus) {
                      if (_checkContainer != tecContainerNo.text) {
                        _checkContainer = tecContainerNo.text;
                        if (_keyContainer.currentState.validate()) {
                          if (!_checkDigit(tecContainerNo.text)) {
                            showDialog<void>(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Text('Error'),
                                    content: SingleChildScrollView(
                                      child: ListBody(
                                        children: <Widget>[
                                          Padding(
                                            padding:
                                                const EdgeInsets.only(top: 8.0),
                                            child: new Text(
                                              "Container No. is not comply to ISO6346.!\nDo you wish to continue ?",
                                              style: TextStyle(
                                                fontSize: 20,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    actions: <Widget>[
                                      FlatButton(
                                        child: const Text('Continue'),
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                          _submitBLCheck();
                                        },
                                      ),
                                      FlatButton(
                                        child: const Text('Cancel'),
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                          focusNode.requestFocus();
                                        },
                                      ),
                                    ],
                                  );
                                });
                          } else {
                            _submitBLCheck();
                          }
                        } else {
                          _submitBLCheck();
                        }
                      }
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: Form(
                      key: _keyContainer,
                      child: TextFormField(
                        controller: tecContainerNo,
                        focusNode: focusNode,
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.characters,
                        // Use email input type for emails.
                        // inputFormatters: [
                        //   UpperCaseTextFormatter(),
                        // ],
                        validator: (value) {
                          if (value.length != 11) {
                            return "Container No. must be 11 characters.";
                          }
                          return null;
                        },
                        onChanged: (value) {
                          setState(() {
                            _hasCalled = false;
                          });
                        },
                        maxLength: 11,
                        decoration: new InputDecoration(
                            suffixIcon: Container(
                              color: Colors.transparent,
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  _isCheck != null
                                      ? _isCheck
                                          ? Icon(
                                              Icons.check,
                                              color: AppTheme.colorPrimary,
                                            )
                                          : Icon(
                                              Icons.clear,
                                              color: Colors.red,
                                            )
                                      : SizedBox(),
                                  GestureDetector(
                                      onTap: () {
                                        _readContainer();
                                      },
                                      child: Image.asset(
                                        "assets/images/ic_ocr.png",
                                        height: 35,
                                      ))
                                ],
                              ),
                            ),
                            labelText: "Container No* :",
                            labelStyle:
                                TextStyle(fontSize: 18, color: Colors.black38),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: AppTheme.colorPrimary, width: 1.0),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: AppTheme.colorMediumGrey, width: 1.0),
                            ),
                            border: new OutlineInputBorder(),
                            counterText: ""),
                        autofocus: false,
                        onSaved: (value) {
                          this._data._containerNo = value;
                        },
                      ),
                    ),
                  ),
                ),
                 Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: TextFormField(
                      onTap: (){
                      if (_hasCalled) {
                        if (gateInModel.isCanEditOperator != null &&
                            gateInModel.isCanEditOperator == "1") {
                          print("no hafve");
                          _showModalOperator(context);
                        }else{
                          print("no hafve");

                        }
                      } else {
                        print("no hafve");

                        FocusScope.of(context).unfocus();
                      }
                    },
                      controller: tecOperator,
                      keyboardType: TextInputType.text,
                      textCapitalization: TextCapitalization.characters,
                      // inputFormatters: [
                      //   UpperCaseTextFormatter(),
                      // ],
                      readOnly: true,
                      validator: (value) {
                        if (value.length <= 0) {
                          return "Operator Code cannot be empty.";
                        }
                        return null;
                      },
                      decoration: new InputDecoration(
                          labelText: "Operator :",
                          labelStyle:
                              TextStyle(fontSize: 18, color: Colors.black38),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorPrimary, width: 1.0),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorMediumGrey, width: 1.0),
                          ),
                          disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorMediumGrey, width: 1.0),
                          ),
                          border: new OutlineInputBorder(),
                          errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Theme.of(context).errorColor,
                                width: 1.0),
                          ),
                          errorStyle: TextStyle(
                            color: Theme.of(context)
                                .errorColor, // or any other color
                          ),
                          counterText: ""),
                      autofocus: false,
                      onSaved: (value) {
                        if (value.length > 0) this._data._operatorCode = value;
                      },
                    ),
                  ),
                if (tecPreGateIn.text.length > 0)
                  Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: TextFormField(
                      controller: tecPreGateIn,
                      keyboardType: TextInputType.text,
                      textCapitalization: TextCapitalization.characters,
                      // inputFormatters: [
                      //   UpperCaseTextFormatter(),
                      // ],
                      readOnly: _isReadonly,
                      decoration: new InputDecoration(
                          labelText: "Pre-Gate In No. :",
                          labelStyle:
                              TextStyle(fontSize: 18, color: Colors.black38),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorPrimary, width: 1.0),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorMediumGrey, width: 1.0),
                          ),
                          disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorMediumGrey, width: 1.0),
                          ),
                          border: new OutlineInputBorder(),
                          errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Theme.of(context).errorColor,
                                width: 1.0),
                          ),
                          errorStyle: TextStyle(
                            color: Theme.of(context)
                                .errorColor, // or any other color
                          ),
                          counterText: ""),
                      autofocus: false,
                      enabled: false,
                    ),
                  ),
                Focus(
                  onFocusChange: (hasFocus) {
                    if (!hasFocus) {
                      if (tecPrimeReg.text != _data._primeMoverRegNo) {
                        if (!_isReadonly) {
                          _submitPrimeSearch();
                          _data._primeMoverRegNo = tecPrimeReg.text;
                        }
                      }
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: TextFormField(
                      controller: tecPrimeReg,
                      keyboardType: TextInputType.text,
                      textCapitalization: TextCapitalization.characters,
                      // inputFormatters: [UpperCaseTextFormatter()],
                      readOnly: gateInDoc.length > 0 ? true : false,
                      enabled: gateInDoc.length > 0 ? false : true,
                      validator: (value) {
                        if (value.length <= 0) {
                          return "Prime Mover Reg No cannot be empty.";
                        }
                        return null;
                      },
                      decoration: new InputDecoration(
                          labelText: "Prime Mover Reg No:",
                          labelStyle:
                              TextStyle(fontSize: 18, color: Colors.black38),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorPrimary, width: 1.0),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorMediumGrey, width: 1.0),
                          ),
                          disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorMediumGrey, width: 1.0),
                          ),
                          errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Theme.of(context).errorColor,
                                width: 1.0),
                          ),
                          errorStyle: TextStyle(
                            color: Theme.of(context)
                                .errorColor, // or any other color
                          ),
                          border: new OutlineInputBorder(),
                          suffixIcon: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              GestureDetector(
                                  onTap: () {
                                    if (gateInDoc.length <= 0)
                                      _read(tecPrimeReg);
                                  },
                                  child: Image.asset(
                                    "assets/images/ic_ocr.png",
                                    height: 35,
                                  )),
                            ],
                          ),
                          counterText: ""),
                      autofocus: false,
                      onSaved: (value) {
                        if (value.length > 0)
                          this._data._primeMoverRegNo = value;
                      },
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: TextFormField(
                    controller: tecPrimeId,
                    keyboardType: TextInputType.text,
                    // Use email input type for emails.
                    textCapitalization: TextCapitalization.characters,
                    // inputFormatters: [UpperCaseTextFormatter()],
                    readOnly: gateInDoc.length <= 0 ? false : true,

                    decoration: new InputDecoration(
                        labelText: "Prime Mover Id :",
                        labelStyle:
                            TextStyle(fontSize: 18, color: Colors.black38),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: AppTheme.colorPrimary, width: 1.0),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: AppTheme.colorMediumGrey, width: 1.0),
                        ),
                        border: new OutlineInputBorder(),
                        disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: AppTheme.colorMediumGrey, width: 1.0),
                        ),
                        counterText: ""),
                    autofocus: false,
                    onSaved: (value) {
                      if (value.length > 0) this._data._primeMoverId = value;
                    },
                  ),
                ),
                Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: TextFormField(
                      controller: tecHaulier,
                      onTap: () {
                        if (gateInDoc.length <= 0) {
                          _showModal(context);
                        } else {
                          if (gateInModel.isCanEditHaulier != null &&
                              gateInModel.isCanEditHaulier == "1")
                            _showModal(context);
                        }
                      },
                      keyboardType: TextInputType.text,
                      textCapitalization: TextCapitalization.characters,
                      // inputFormatters: [UpperCaseTextFormatter()],
                      readOnly: true,
                      decoration: new InputDecoration(
                          labelText: "Haulier Code:",
                          labelStyle:
                              TextStyle(fontSize: 18, color: Colors.black38),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorPrimary, width: 1.0),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorMediumGrey, width: 1.0),
                          ),
                          disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorMediumGrey, width: 1.0),
                          ),
                          errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Theme.of(context).errorColor,
                                width: 1.0),
                          ),
                          errorStyle: TextStyle(
                            color: Theme.of(context)
                                .errorColor, // or any other color
                          ),
                          border: new OutlineInputBorder(),
                          counterText: ""),
                      autofocus: false,
                      validator: (value) {
                        if (value.length <= 0) {
                          return "Haulier Code cannot be empty.";
                        }
                        return null;
                      },
                      onSaved: (value) {
                        if (value.length > 0) this._data._haulier = value;
                      },
                    ),
                  ),
                Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: TextFormField(
                      onTap: () {
                        popDialog("Container Status Options", listStatus);
                      },
                      controller: tecContainerStatus,
                      keyboardType: TextInputType.text,
                      textCapitalization: TextCapitalization.characters,
                      // inputFormatters: [
                      //   UpperCaseTextFormatter(),
                      // ],
                      readOnly: true,
                      decoration: new InputDecoration(
                          suffixIcon: Icon(Icons.arrow_drop_down),
                          labelText: "Container Status :",
                          labelStyle:
                              TextStyle(fontSize: 18, color: Colors.black38),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorPrimary, width: 1.0),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorMediumGrey, width: 1.0),
                          ),
                          border: new OutlineInputBorder(),
                          disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorMediumGrey, width: 1.0),
                          ),
                          counterText: ""),
                      autofocus: false,
                    ),
                ),
                Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: TextFormField(
                      controller: tecContainerGrade,
                      keyboardType: TextInputType.text,
                      textCapitalization: TextCapitalization.characters,
                      // inputFormatters: [
                      //   UpperCaseTextFormatter(),
                      // ],
                      onTap: () {
                        popDialog("Container Grade Option :", listGrade);
                      },
                      readOnly: true,
                      maxLength: 11,
                      decoration: new InputDecoration(
                          suffixIcon: Icon(Icons.arrow_drop_down),
                          disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorMediumGrey, width: 1.0),
                          ),
                          labelText: "Container Grade :",
                          labelStyle:
                              TextStyle(fontSize: 18, color: Colors.black38),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorPrimary, width: 1.0),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorMediumGrey, width: 1.0),
                          ),
                          border: new OutlineInputBorder(),
                          counterText: ""),
                      autofocus: false,
                    ),
                  ),
                Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: TextFormField(
                      controller: tecISOCode,
                      keyboardType: TextInputType.text,
                      textCapitalization: TextCapitalization.characters,
                      // inputFormatters: [
                      //   UpperCaseTextFormatter(),
                      // ],
                      validator: (value) {
                        if (value.length <= 0) {
                          return "ISO Code cannot be empty.";
                        }
                        return null;
                      },
                      readOnly: true,
                      onTap: () {
                        if (gateInDoc.length <= 0)
                          _settingModalBottomSheet(context, jsonTemp);
                      },
                      decoration: new InputDecoration(
                          suffixIcon: Icon(Icons.arrow_drop_down),
                          labelText: "ISO Code :",
                          labelStyle:
                              TextStyle(fontSize: 18, color: Colors.black38),
                          disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorMediumGrey, width: 1.0),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorPrimary, width: 1.0),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorMediumGrey, width: 1.0),
                          ),
                          border: new OutlineInputBorder(),
                          errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Theme.of(context).errorColor,
                                width: 1.0),
                          ),
                          errorStyle: TextStyle(
                            color: Theme.of(context)
                                .errorColor, // or any other color
                          ),
                          counterText: ""),
                      autofocus: false,
                    ),
                  ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: TextFormField(
                    controller: tecTypeSize,
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.characters,
                    // inputFormatters: [
                    //   UpperCaseTextFormatter(),
                    // ],
                    maxLength: 20,
                    enabled: false,
                    decoration: new InputDecoration(
                        labelText: "Type Size :",
                        labelStyle:
                            TextStyle(fontSize: 18, color: Colors.black38),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: AppTheme.colorPrimary, width: 1.0),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: AppTheme.colorMediumGrey, width: 1.0),
                        ),
                        disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: AppTheme.colorMediumGrey, width: 1.0),
                        ),
                        border: new OutlineInputBorder(),
                        counterText: ""),
                    autofocus: false,
                    onSaved: (value) {
                      this._data._typeSize = value;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: TextFormField(
                    controller: tecMGW,
                    keyboardType:
                        TextInputType.numberWithOptions(decimal: true),
                    // Use email input type for emails.
                    inputFormatters: [
                      DecimalTextInputFormatter(decimalRange: 4)
                    ],
                    decoration: new InputDecoration(
                        labelText: "MGW :",
                        labelStyle:
                            TextStyle(fontSize: 18, color: Colors.black38),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: AppTheme.colorPrimary, width: 1.0),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: AppTheme.colorMediumGrey, width: 1.0),
                        ),
                        border: new OutlineInputBorder(),
                        errorMaxLines: 2,
                        counterText: ""),
                    validator: (value) {
                      if (value.length > 0) {
                        double newValue = double.parse(value);
                        if (newValue != 0) if (newValue < 10000 ||
                            newValue > 55000) {
                          return "MGW is out of range ! The valid value must between 10,000 and 55,000 (Kg)!";
                        }
                      }
                      return null;
                    },
                    autofocus: false,
                    readOnly: gateInDoc.length <= 0 ? false : true,
                    onSaved: (value) {
                      if (value.length > 0)
                        this._data._mGW = double.parse(value);
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: TextFormField(
                    controller: tecTareWeight,
                    keyboardType:
                        TextInputType.numberWithOptions(decimal: true),
                    // Use email input type for emails.
                    inputFormatters: [
                      DecimalTextInputFormatter(decimalRange: 4)
                    ],
                    decoration: new InputDecoration(
                        labelText: "Tare Weight :",
                        labelStyle:
                            TextStyle(fontSize: 18, color: Colors.black38),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: AppTheme.colorPrimary, width: 1.0),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: AppTheme.colorMediumGrey, width: 1.0),
                        ),
                        border: new OutlineInputBorder(),
                        errorMaxLines: 2,
                        counterText: ""),
                    autofocus: false,
                    validator: (value) {
                      if (value.length > 0) {
                        double newValue = double.parse(value);
                        if (newValue != 0) if (newValue < 1000 ||
                            newValue > 6500)
                          return "Tare Weight is out of range ! The valid value must between 1,000 and 6,500 (Kg)!";
                      }
                      return null;
                    },
                    readOnly: gateInDoc.length <= 0 ? false : true,
                    onSaved: (value) {
                      if (value.length > 0)
                        this._data._tareWeight = double.parse(value);
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: TextFormField(
                    controller: tecManufactureDate,
                    keyboardType: TextInputType.number,
                    textCapitalization: TextCapitalization.characters,
                    // inputFormatters: [
                    //   UpperCaseTextFormatter(),
                    // ],
                    decoration: new InputDecoration(
                        suffixIcon: GestureDetector(
                            onTap: () {
                              if (gateInDoc.length <= 0) _showDatePicker();
                            },
                            child: Icon(Icons.calendar_today)),
                        labelText: "Manufacture Date :",
                        hintText: "2022/05",
                        hintStyle:
                            TextStyle(color: Colors.grey.withOpacity(0.4)),
                        labelStyle:
                            TextStyle(fontSize: 18, color: Colors.black38),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: AppTheme.colorPrimary, width: 1.0),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: AppTheme.colorMediumGrey, width: 1.0),
                        ),
                        border: new OutlineInputBorder(),
                        disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: AppTheme.colorMediumGrey, width: 1.0),
                        ),
                        counterText: ""),
                    maxLength: 7,
                    readOnly: gateInDoc.length <= 0 ? false : true,
                    validator: (value) {
                      if (value.length > 0) {
                        if (!(value.length >= 6)) {
                          return "Enter correct format (YYYY/MM)";
                        } else if (value.length == 6) {
                          if (value[5] == "0")
                            return "Enter correct format (YYYY/MM)";
                        }
                      }
                      return null;
                    },
                    onChanged: (value) {
                      final sanitizedText = value.replaceAll('/', '');
                      tecManufactureDate.text = _addDashes(sanitizedText);
                      tecManufactureDate.selection = TextSelection.collapsed(
                          offset: tecManufactureDate.text.length);
                    },
                    onSaved: (value) {
                      String newValue = value.replaceAll("/", "-");
                      String dateString;
                      if (newValue.length == 6) {
                        dateString = newValue.split('-')[0] +
                            "0" +
                            newValue.split('-')[1] +
                            "-01";
                      } else if (newValue.length == 7) {
                        dateString = newValue.split('-')[0] +
                            newValue.split('-')[1] +
                            "-01";
                      }

                      if (value.length > 0) {
                        _data._manufactureDate =
                            dobFormatStore.format(DateTime.parse(dateString));
                      } else {
                        _data._manufactureDate = "";
                      }
                    },
                    autofocus: false,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: Focus(
                    onFocusChange: (hasFocus) {
                      if (hasFocus) {
                        setState(() {
                          _isRemarkFocused = true;
                        });
                      } else {
                        setState(() {
                          _isRemarkFocused = false;
                        });
                      }
                    },
                    child: TextFormField(
                      controller: tecRemark,
                      keyboardType: TextInputType.multiline,
                      textCapitalization: TextCapitalization.characters,
                      // inputFormatters: [
                      //   UpperCaseTextFormatter(),
                      // ],
                      maxLines: 5,
                      minLines: 1,
                      textInputAction: TextInputAction.newline,
                      decoration: new InputDecoration(
                          labelText: "Remark :",
                          labelStyle:
                              TextStyle(fontSize: 18, color: Colors.black38),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorPrimary, width: 1.0),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorMediumGrey, width: 1.0),
                          ),
                          border: new OutlineInputBorder(),
                          disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorMediumGrey, width: 1.0),
                          ),
                          counterText: ""),
                      autofocus: false,
                      onSaved: (value) {
                        this._data._remark = value;
                      },
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: TextFormField(
                    controller: tecSealNo,
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.characters,
                    // inputFormatters: [
                    //   UpperCaseTextFormatter(),
                    // ],
                    maxLength: 10,
                    decoration: new InputDecoration(
                        suffixIcon: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            GestureDetector(
                                onTap: () {
                                  // if (gateInDoc.length <= 0) _read(tecSealNo);
                                },
                                child: Image.asset(
                                  "assets/images/ic_ocr.png",
                                  height: 35,
                                )),
                          ],
                        ),
                        labelText: "Seal No :",
                        labelStyle:
                            TextStyle(fontSize: 18, color: Colors.black38),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: AppTheme.colorPrimary, width: 1.0),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: AppTheme.colorMediumGrey, width: 1.0),
                        ),
                        disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: AppTheme.colorMediumGrey, width: 1.0),
                        ),
                        border: new OutlineInputBorder(),
                        counterText: ""),
                    autofocus: false,
                    readOnly: gateInDoc.length <= 0 ? false : true,
                    onSaved: (value) {
                      this._data._sealNo = value;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 16.0, bottom: 16),
                  child: SizedBox(
                    width: 150,
                    height: 50,
                    child: ElevatedButton(
                      child: Text(
                        "Submit",
                        style: TextStyle(color: Colors.white),
                      ),
                      onPressed: () {
                        if (_isCheck != null && _isCheck) {
                          if (_formKey_searchList.currentState.validate()) {
                            // Save our form now.
                            _formKey_searchList.currentState.save();
                            if (gateInModel.isEmptyReject != null &&
                                gateInModel.isEmptyReject == "1") {
                              showDialog<void>(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      content: Text("Is empty reject? (Y/N)?"),
                                      actions: <Widget>[
                                        FlatButton(
                                          child: const Text('Yes'),
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                            _submitGateIn(true);
                                          },
                                        ),
                                        FlatButton(
                                          child: const Text('No'),
                                          onPressed: () {
                                            Navigator.of(context).pop();

                                            _submitGateIn(false);
                                          },
                                        ),
                                      ],
                                    );
                                  });
                            } else {
                              _submitGateIn(false);
                            }
                          }
                        } else {
                          setState(() {
                            _isCheck = false;
                            _scrollController.animateTo(0,
                                duration: Duration(milliseconds: 300),
                                curve: Curves.ease);
                            focusNode.requestFocus();
                            Fluttertoast.showToast(
                                msg: "Invalid Container No.",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIosWeb: 1,
                                backgroundColor: Colors.red,
                                textColor: Colors.white,
                                fontSize: 16.0);
                          });
                        }
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  String _addDashes(String text) {
    const int addDashEvery = 4;
    String out = '';

    for (int i = 0; i < text.length; i++) {
      if (i + 1 > addDashEvery && i % addDashEvery == 0) {
        out += '/';
      }

      out += text[i];
    }

    if (text.length == 4) {
      String newInt = text[0] + text[1] + text[2] + text[3];
      if (int.parse(newInt) > DateTime.now().year) {
        return out.replaceRange(0, 4, DateTime.now().year.toString());
      } else if (int.parse(newInt) < DateTime.now().year - 30) {
        return out.replaceRange(0, 4, (DateTime.now().year - 30).toString());
      }
    }

    if (text.length == 6) {
      String newInt = text[4] + text[5];
      String newYear = text[0] + text[1] + text[2] + text[3];

      if (int.parse(newYear) == DateTime.now().year) {
        if (int.parse(newInt) > DateTime.now().month)
          return out.replaceRange(
              5, 7, DateTime.now().month.toString().padLeft(2, "0"));
        else if (int.parse(newInt) == 0) {
          return out.replaceRange(5, 7, "01");
        }
      } else if (int.parse(newInt) > 12) {
        return out.replaceRange(5, 7, "12");
      } else if (int.parse(newInt) <= 0) {
        return out.replaceRange(5, 7, "01");
      }
    }

    return out;
  }

  Future<String> _scanQR() async {
    try {
      var qrResult = await BarcodeScanner.scan();
      print("Scanned");
      result = qrResult.rawContent;
      print(result);

      txtSearchController.text = result;
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.cameraAccessDenied) {
        setState(() {
          result = "Camera permission was denied";
          Fluttertoast.showToast(
              msg: result,
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0);
        });
      } else {
        setState(() {
          result == "Unknown Error $e";
          //Toast.show(result, context, duration: 2);
        });
      }
    } on FormatException {
      setState(() {
        result = "Cancelled";
        Fluttertoast.showToast(
            msg: result,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      });
    } catch (e) {
      setState(() {
        result == "Unknown Error $e";
        //Toast.show(result, context, duration: 2);
      });
    }
  }

  void _submitPrimeSearch() async {
    // Save our form now.
    FocusScope.of(context).unfocus();
    Map dataValue = {"Key": "PrimeMoverRegNo", "Value": tecPrimeReg.text};

    List list = [];
    list.add(json.encode(dataValue));

    Map data = {'Key': 'Data', 'Value': "$list"};

    final response = await _httpHelper.post(
        baseUrl, UrlConfig.GET_PRIME_HAULIER, json.encode(data));
    Response res = Response.fromJson(response);
    print("PKM" + res.statusCode.toString());
//      pr.hide();
    if (res.statusCode == 200) {
      Map<String, dynamic> tempResult = json.decode(res.results[0].value);
      if (tempResult['Result'] != null && tempResult['Result'] != "null") {
        var listResult = json.decode(tempResult['Result']);
        tecPrimeId.text = listResult[0]['PrimeMoverId'];
        tecHaulier.text = listResult[0]['HaulierCode'];
      } else {
        tecPrimeId.text = "";
        tecHaulier.text = "";
      }
    } else {
      Fluttertoast.showToast(
          msg: res.errorMessage,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
      setState(() {
        FocusScope.of(context).unfocus();
      });

      if (res.errorMessage.contains("AccessToken"))
        new force_logout(context).doForceLogout();
    }
  }

  void _submitBLCheck() async {
    // Save our form now.
    FocusScope.of(context).unfocus();
    if (_keyContainer.currentState.validate()) {
      _keyContainer.currentState.save();

      Map dataValue = {"Key": "ContainerNumber", "Value": tecContainerNo.text};

      List list = [];
      list.add(json.encode(dataValue));

      Map data = {'Key': 'Data', 'Value': "$list"};

      final response = await _httpHelper.post(
          baseUrl, UrlConfig.GATE_IN_BL_CHECK, json.encode(data));
      Response res = Response.fromJson(response);
      setState(() {
        _hasCalled = true;
      });
      print("PKM" + res.statusCode.toString());
//      pr.hide();
      if (res.statusCode == 200) {
        if (res.results.length > 0) {
          print("RESULT -- " + res.results[0].value);
          var tempResult = json.decode(res.results[0].value);

          if (tempResult == null || tempResult == "") {
            _submitGateInSearch();
          } else {
            setState(() {
              _isCheck = false;
              _isSuccess = false;
              _isReadonly = false;
              gateInDoc = "";
              tecTypeSize.text = "";
              tecGateInDocNo.text = "";
              tecContainerGrade.text = "";
              tecPrimeReg.text = "";
              tecPrimeId.text = "";
              tecHaulier.text = "";
              tecOperator.text = "";
              tecISOCode.text = "";
              tecMGW.text = "";
              tecTareWeight.text = "";
              tecManufactureDate.text = "";
              tecSealNo.text = "";
              tecRemark.text = "";
            });
            showDialog<void>(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text('BlackListed'),
                  content: SingleChildScrollView(
                    child: ListBody(
                      children: <Widget>[
                        const Text(
                          'tempResult',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20),
                        ),
                      ],
                    ),
                  ),
                  actions: <Widget>[
                    FlatButton(
                      child: const Text('Ok'),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                );
              },
            );
          }
        }
      } else if (res.statusCode == 400) {
        FocusScope.of(context).unfocus();
        showDialog<void>(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text(res.errorDetail),
                content: SingleChildScrollView(
                  child: ListBody(
                    children: <Widget>[
                      Text(
                        res.errorMessage,
                        style: TextStyle(),
                      ),
                    ],
                  ),
                ),
                actions: <Widget>[
                  FlatButton(
                    child: const Text('Ok'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              );
            });
        setState(() {
          _isCheck = false;
          gateInDoc = "";
          gateInModel = new GateInModel();
          tecTypeSize.text = "";
          tecGateInDocNo.text = "";
          tecContainerGrade.text = "";
          tecPrimeReg.text = "";
          tecPrimeId.text = "";
          tecHaulier.text = "";
          tecOperator.text = "";
          tecISOCode.text = "";
          tecMGW.text = "";
          tecTareWeight.text = "";
          tecManufactureDate.text = "";
          tecSealNo.text = "";
          tecRemark.text = "";
        });
      } else {
        Fluttertoast.showToast(
            msg: res.errorMessage,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
        setState(() {
          listContainer = [];
          FocusScope.of(context).unfocus();
        });

        if (res.errorMessage.contains("AccessToken"))
          new force_logout(context).doForceLogout();
      }
    } else {
      _isCheck = false;
      gateInModel = new GateInModel();
      gateInDoc = "";
      tecTypeSize.text = "";
      tecGateInDocNo.text = "";
      tecContainerGrade.text = "";
      tecPrimeReg.text = "";
      tecPrimeId.text = "";
      tecHaulier.text = "";
      tecOperator.text = "";
      tecISOCode.text = "";
      tecMGW.text = "";
      tecTareWeight.text = "";
      tecManufactureDate.text = "";
      tecSealNo.text = "";
      tecRemark.text = "";
    }
  }

  void _submitGateInSearch() async {
    // Save our form now.
    FocusScope.of(context).unfocus();
    Map dataValue = {"Key": "ContainerNumber", "Value": tecContainerNo.text};

    List list = [];
    list.add(json.encode(dataValue));

    Map data = {'Key': 'Data', 'Value': "$list"};

    final response = await _httpHelper.post(
        baseUrl, UrlConfig.GATE_IN_CHECK, json.encode(data));
    Response res = Response.fromJson(response);
    print("PKM" + res.statusCode.toString());
//      pr.hide();
    if (res.statusCode == 200) {
      if (res.results.length > 0) {
        print("RESULT -- " + res.results[0].value.replaceAll("\\", ""));
        Map<String, dynamic> tempResult = json.decode(res.results[0].value);

        if (tempResult['Result'] != null) {
          List<dynamic> listResults =
              json.decode(tempResult['Result'].replaceAll("\n", "\\n"));

          gateInModel = GateInModel.fromJson(listResults[0]);

          if (listResults[0]['GateInDocNo'] != null &&
              listResults[0]['GateInDocNo'].length > 0) {
            setState(() {
              gateInDoc = listResults[0]['GateInDocNo'];
              _isGateInCreated = true;
              _isSuccess = true;
            });
          } else {
            setState(() {
              gateInDoc = "";
              _isGateInCreated = false;
              _isSuccess = false;
            });
          }

          setState(() {
            _isCheck = true;
            tecISOCode.text = listResults[0]['ISOCode'] != null
                ? listResults[0]['ISOCode']
                : "";
            tecContainerGrade.text = listResults[0]['ContainerGrade'] != null
                ? listResults[0]['ContainerGrade']
                : "";

            if (listResults[0]['ISOCode'] != null)
              for (int i = 0; i < jsonTemp.length; i++) {
                if (listResults[0]['ISOCode'] == jsonTemp[i]['Key']) {
                  tecISOCode.text = jsonTemp[i]['Key'];
                  _data._iSOCode = jsonTemp[i]['Key'];
                  tecTypeSize.text = jsonTemp[i]['Value'];
                }
              }

            if (listResults[0]['ContainerStatus'] != null)
              for (int i = 0; i < statusTemp.length; i++) {
                if (listResults[0]['ContainerStatus'] == statusTemp[i]['Key']) {
                  tecContainerStatus.text = statusTemp[i]['Value'];
                  _data._containerStatus = statusTemp[i]['Key'];
                }
              }

            if (listResults[0]['ContainerGrade'] != null)
              for (int i = 0; i < gradeTemp.length; i++) {
                if (listResults[0]['ContainerGrade'] == gradeTemp[i]['Key']) {
                  tecContainerGrade.text = gradeTemp[i]['Value'];
                  _data._containerGrade = gradeTemp[i]['Key'];
                }
              }
            tecMGW.text =
                listResults[0]['MGW'] != null && listResults[0]['MGW'] != "0"
                    ? listResults[0]['MGW']
                    : "";
            tecTareWeight.text = listResults[0]['TareWeight'] != null &&
                    listResults[0]['TareWeight'] != "0"
                ? listResults[0]['TareWeight']
                : "";
            if (listResults[0]['ManufactureDate'] != null) {
              tecManufactureDate.text = dobFormat
                  .format(DateTime.parse(listResults[0]['ManufactureDate']));
              _data._manufactureDate = dobFormatStore
                  .format(DateTime.parse(listResults[0]['ManufactureDate']));
            } else {
              tecManufactureDate.text = "";
              _data._manufactureDate = "";
            }
            tecSealNo.text = listResults[0]['SealNo'] ?? "";
            tecPrimeReg.text = listResults[0]['PrimeMoverRegNo'] ?? "";
            tecPrimeId.text = listResults[0]['PrimeMoverId'] ?? "";
            tecHaulier.text = listResults[0]['HaulierCode'] ?? "";
            tecOperator.text = listResults[0]['OperatorCode'] ?? "";
            tecPreGateIn.text = listResults[0]['PreGateInNo'] ?? "";
            tecTypeSize.text = listResults[0]['TypeSize'] ?? "";
            tecRemark.text = listResults[0]['Remarks'] ?? "";
            if (listResults[0]['PrimeMoverId'] != null &&
                listResults[0]['PrimeMoverId'].length > 0) {
              _isReadonly = true;
            }
          });
        } else {
          setState(() {
            _isCheck = true;
            _isSuccess = false;
            _isGateInCreated = false;
            gateInModel = new GateInModel();
            _isReadonly = false;
            gateInDoc = "";
            tecTypeSize.text = "";
            tecGateInDocNo.text = "";
            tecContainerGrade.text = "";
            tecPrimeReg.text = "";
            tecPrimeId.text = "";
            tecHaulier.text = "";
            tecOperator.text = "";
            tecPreGateIn.text = "";
            tecISOCode.text = "";
            tecMGW.text = "";
            tecTareWeight.text = "";
            tecManufactureDate.text = "";
            tecSealNo.text = "";
            tecRemark.text = "";
          });
        }
      } else {
        Fluttertoast.showToast(
            msg: res.errorMessage,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
        setState(() {
          _isCheck = false;
          _isGateInCreated = false;
          _isReadonly = false;
          gateInDoc = "";
          tecTypeSize.text = "";
          gateInModel = new GateInModel();
          tecGateInDocNo.text = "";
          tecContainerGrade.text = "";
          tecPrimeReg.text = "";
          tecPrimeId.text = "";
          tecHaulier.text = "";
          tecOperator.text = "";
          tecPreGateIn.text = "";
          tecISOCode.text = "";
          tecMGW.text = "";
          tecTareWeight.text = "";
          tecManufactureDate.text = "";
          tecSealNo.text = "";
          tecRemark.text = "";
          FocusScope.of(context).unfocus();
        });

        if (res.errorMessage.contains("AccessToken"))
          new force_logout(context).doForceLogout();
      }
    }
  }

  void _submitGateIn(bool isEmpty) async {
    pr.show();
    Map dataValue = {
      "ContainerNo": _data._containerNo,
      "OperatorCode": _data._operatorCode,
      "PrimeMoverRegNo": _data._primeMoverRegNo,
      "PrimeMoverId": _data._primeMoverId,
      "HaulierCode": _data._haulier,
      "ContainerStatus": _data._containerStatus,
      "ContainerGrade": _data._containerGrade,
      "ISOCode": _data._iSOCode,
      "TypeSize": _data._typeSize,
      "MGW": _data._mGW,
      "TareWeight": _data._tareWeight,
      "ManufactureDate": _data._manufactureDate.length <= 0 ||
              tecManufactureDate.text.length <= 0
          ? "1900-01-01T00:00:00"
          : _data._manufactureDate,
      "Remarks": _data._remark,
      "SealNo": _data._sealNo
    };

    if (gateInDoc.length > 0)
      dataValue['GateInDocNo'] = gateInDoc.replaceAll("\"", "");

    if (!isEmpty)
      dataValue['IsEmptyReject'] = 0;
    else
      dataValue['IsEmptyReject'] = 1;

    List list = [];
    list.add(json.encode(dataValue));

    Map data = {'Key': 'Data', 'Value': "${json.encode(dataValue)}"};

    final response =
        await _httpHelper.post(baseUrl, UrlConfig.GATE_IN, json.encode(data));
    Response res = Response.fromJson(response);
    print("PKM" + res.statusCode.toString());
    pr.hide();
    if (res.statusCode == 200) {
      FocusScope.of(context).unfocus();
      if (res.results.length > 0) {
        print("RESULT -- " + res.results[0].value);
        if (res.results[0].value != null) {
          setState(() {
            gateInDoc = res.results[0].value;
            _isGateInCreated = true;
            _isSuccess = true;
            _scrollController.animateTo(0,
                duration: Duration(milliseconds: 1000), curve: Curves.ease);
            _submitGateInSearch();
          });
          showDialog<void>(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('Success Gate In'),
                content: SingleChildScrollView(
                  child: ListBody(
                    children: <Widget>[
                      const Text(
                        'Gate In No. :',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: new Text(
                          res.results[0].value,
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                actions: <Widget>[
                  FlatButton(
                    child: const Text('Ok'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              );
            },
          );
        } else {
          Fluttertoast.showToast(
              msg: "No result",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0);
        }
      }
    } else {
      Fluttertoast.showToast(
          msg: res.errorMessage,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
      setState(() {
        FocusScope.of(context).unfocus();
      });

      if (res.errorMessage.contains("AccessToken"))
        new force_logout(context).doForceLogout();
    }
  }

  clearPrefsAll() {
    AppPrefs.setPosFront("");
    AppPrefs.setPosDoor("");
    AppPrefs.setPosLeft("");
    AppPrefs.setPosRight("");
    AppPrefs.setPosTop("");
    AppPrefs.setPosBottom("");
    AppPrefs.setPosInside("");
    AppPrefs.setPosSeal("");

    AppPrefs.setRemarkFront("");
    AppPrefs.setRemarkDoor("");
    AppPrefs.setRemarkLeft("");
    AppPrefs.setRemarkRight("");
    AppPrefs.setRemarkTop("");
    AppPrefs.setRemarkBottom("");
    AppPrefs.setRemarkInside("");
    AppPrefs.setRemarkSeal("");

    AppPrefs.setSavedSeal("");
  }

  Widget txtField(
      String lbl,
      TextInputType type,
      int charCount,
      List<TextInputFormatter> inputFormat,
      List<FormFieldValidator> inputValidator,
      TextEditingController textEditingController) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: TextFormField(
        controller: textEditingController,
        keyboardType: type,
        // Use email input type for emails.
        inputFormatters: inputFormat,
        maxLength: 10,
        decoration: new InputDecoration(
            labelText: lbl,
            labelStyle: TextStyle(fontSize: 18),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: AppTheme.colorPrimary, width: 1.0),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide:
                  BorderSide(color: AppTheme.colorMediumGrey, width: 1.0),
            ),
            border: new OutlineInputBorder(),
            counterText: ""),
        autofocus: false,
      ),
    );
  }

  void _showModal(context) {
    showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(15.0)),
        ),
        context: context,
        builder: (context) {
          //3
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
            return DraggableScrollableSheet(
                expand: false,
                builder:
                    (BuildContext context, ScrollController scrollController) {
                  return Column(children: <Widget>[
                    Padding(
                        padding: EdgeInsets.all(8),
                        child: Row(children: <Widget>[
                          Expanded(
                              child: TextField(
                                  controller: textController,
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.all(8),
                                    border: new OutlineInputBorder(
                                      borderRadius:
                                          new BorderRadius.circular(15.0),
                                      borderSide: new BorderSide(),
                                    ),
                                    prefixIcon: Icon(Icons.search),
                                  ),
                                  onChanged: (value) {
                                    //4
                                    setState(() {
                                      tempListHaulier =
                                          _buildSearchList(value, listHaulier);
                                    });
                                  })),
                        ])),
                    Expanded(
                      child: ListView.separated(
                          controller: scrollController,
                          //5
                          itemCount: (tempListHaulier != null &&
                                  tempListHaulier.length > 0)
                              ? tempListHaulier.length
                              : listHaulier.length,
                          separatorBuilder: (context, int) {
                            return Divider();
                          },
                          itemBuilder: (context, index) {
                            return InkWell(
                                child: (tempListHaulier != null &&
                                        tempListHaulier.length > 0)
                                    ? _showBottomSheetWithSearch(
                                        index, tempListHaulier)
                                    : _showBottomSheetWithSearch(
                                        index, listHaulier),
                                onTap: () {
                                  if (tempListHaulier.length > 0) {
                                    tecHaulier.text =
                                        tempListHaulier[index]['Key'];
                                    _data._haulier =
                                        tempListHaulier[index]['Key'];
                                    print(
                                        "id =  ${tempListHaulier[index]['Key']}");
                                  } else {
                                    tecHaulier.text = listHaulier[index]['Key'];
                                    _data._haulier = listHaulier[index]['Key'];
                                    print("id =  ${listHaulier[index]['Key']}");
                                  }
                                  Navigator.of(context).pop();
                                });
                          }),
                    )
                  ]);
                });
          });
        });
  }

  void _showModalOperator(context) {
    showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(15.0)),
        ),
        context: context,
        builder: (context) {
          //3
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
            return DraggableScrollableSheet(
                expand: false,
                builder:
                    (BuildContext context, ScrollController scrollController) {
                  return Column(children: <Widget>[
                    Padding(
                        padding: EdgeInsets.all(8),
                        child: Row(children: <Widget>[
                          Expanded(
                              child: TextField(
                                  controller: operatorController,
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.all(8),
                                    border: new OutlineInputBorder(
                                      borderRadius:
                                          new BorderRadius.circular(15.0),
                                      borderSide: new BorderSide(),
                                    ),
                                    prefixIcon: Icon(Icons.search),
                                  ),
                                  onChanged: (value) {
                                    //4
                                    setState(() {
                                      tempListOperator =
                                          _buildSearchList(value, listOperator);
                                    });
                                  })),
                        ])),
                    Expanded(
                      child: ListView.separated(
                          controller: scrollController,
                          //5
                          itemCount: (tempListOperator != null &&
                                  tempListOperator.length > 0)
                              ? tempListOperator.length
                              : listOperator.length,
                          separatorBuilder: (context, int) {
                            return Divider();
                          },
                          itemBuilder: (context, index) {
                            return InkWell(
                                child: (tempListOperator != null &&
                                        tempListOperator.length > 0)
                                    ? _showBottomSheetWithSearch(
                                        index, tempListOperator)
                                    : _showBottomSheetWithSearch(
                                        index, listOperator),
                                onTap: () {
                                  if (tempListOperator.length > 0) {
                                    tecOperator.text =
                                        tempListOperator[index]['Key'];
                                    _data._operatorCode =
                                        tempListOperator[index]['Key'];
                                    print(
                                        "id =  ${tempListOperator[index]['Key']}");
                                  } else {
                                    tecOperator.text =
                                        listOperator[index]['Key'];
                                    _data._operatorCode =
                                        listOperator[index]['Key'];
                                    print(
                                        "id =  ${listOperator[index]['Key']}");
                                  }
                                  Navigator.of(context).pop();
                                });
                          }),
                    )
                  ]);
                });
          });
        });
  }

  Widget _showBottomSheetWithSearch(int index, List listAM) {
    return Text(listAM[index]["Value"],
        style: TextStyle(color: Colors.black, fontSize: 16),
        textAlign: TextAlign.center);
  }

  List _buildSearchList(String userSearchTerm, List listed) {
    List _searchList = [];

    for (int i = 0; i < listed.length; i++) {
      String name = listed[i]["Value"];
      if (name.toLowerCase().contains(userSearchTerm.toLowerCase())) {
        _searchList.add(listed[i]);
      }
    }
    return _searchList;
  }
}

class ContainerListView extends StatelessWidget {
  final VoidCallback callback;
  final ContainerModel post;

  const ContainerListView({Key key, this.post, this.callback})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Padding(
      padding: const EdgeInsets.only(left: 24, right: 24, top: 8, bottom: 8),
      child: InkWell(
        splashColor: Colors.transparent,
        onTap: () {
          callback();
        },
        child: Card(
          elevation: 3.0,
          child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(12.0)),
              child: Container(
                height: 100,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(4)),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                        child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                              height: 40,
                              child:
                                  Image.asset("assets/images/container.png")),
                          SizedBox(
                            width: 12,
                          ),
                          Expanded(
                            child: Column(
                              children: <Widget>[
                                Expanded(
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                          child: Text(
                                        post.containerNo,
                                        style: AppTheme.title,
                                      )),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                          child: Text(
                                        post.typeSize,
                                        style: AppTheme.titleDesc,
                                      )),
                                      Expanded(
                                          child: Text(post.operatorCode,
                                              style: AppTheme.titleDesc,
                                              textAlign: TextAlign.end)),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                          child: Text(
                                        DateFormat("dd-MM-yyyy").format(
                                            DateTime.parse(post.pickDropTime)),
                                        style: AppTheme.titleDesc,
                                        maxLines: 1,
                                      )),
                                      Text(
                                        DateFormat("HH : mm").format(
                                            DateTime.parse(post.pickDropTime)),
                                        style: AppTheme.titleDesc,
                                        textAlign: TextAlign.end,
                                        maxLines: 1,
                                      ),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                          child: Text(
                                        post.primeMoverId,
                                        style: AppTheme.titleDesc,
                                        maxLines: 1,
                                      )),
                                      Text(
                                        post.primeMoverRegNo,
                                        style: AppTheme.titleDesc,
                                        textAlign: TextAlign.end,
                                        maxLines: 1,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 30,
                          ),
                          !post.isFileAttached
                              ? Container(
                                  height: 100,
                                  child: Center(
                                      child: Container(
                                          height: 60,
                                          child: Image.asset(
                                              "assets/images/camera.png"))))
                              : Container(
                                  height: 100,
                                  child: Center(
                                      child: Container(
                                          height: 60,
                                          child: Image.asset(
                                              "assets/images/camera_taken.png")))),
                        ],
                      ),
                    )),
                  ],
                ),
              )),
        ),
      ),
    ));
  }

  timeFormater(String lala) {
    String dateFormate =
        DateFormat("dd-MM-yyyy").format(DateTime.parse(post.pickDropTime));
  }
}
