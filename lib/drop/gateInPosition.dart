import 'dart:convert';

import 'package:depot/drop/gateIn.dart';
import 'package:depot/depotOut/depot_out_images.dart';
import 'package:depot/models/container.dart';
import 'package:depot/models/response.dart';
import 'package:depot/styles/appTheme.dart';
import 'package:depot/utils/ApiBaseHelper.dart';
import 'package:depot/utils/AppPrefs.dart';
import 'package:depot/utils/UrlConfig.dart';
import 'package:depot/utils/force_logout.dart';
import 'package:depot/utils/progress_dialog.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'gateInImages.dart';

class GateInPosition extends StatefulWidget {
  final String containerNo;
  final String docNo;

  GateInPosition({Key key, this.containerNo, this.docNo}) : super(key: key);

  @override
  _GateInPositionState createState() => _GateInPositionState();
}

class _GateInPositionState extends State<GateInPosition> {
  ProgressDialog pr;
  ApiBaseHelper _httpHelper = ApiBaseHelper();
  String result, baseUrl;
  String dataFront = "",
      dataDoor = "",
      dataLeft = "",
      dataRight = "",
      dataTop = "",
      dataBottom = "",
      dataInside = "",
      dataSeal = "";

  int numFront = 0,
      numDoor = 0,
      numLeft = 0,
      numRight = 0,
      numTop = 0,
      numBottom = 0,
      numInside = 0,
      numSeal = 0;

  String remarkFront,
      remarkDoor,
      remarkLeft,
      remarkRight,
      remarkTop,
      remarkBottom,
      remarkInside,
      remarkSeal;

  @override
  void initState() {
    // TODO: implement initState
    getPrefData();

    super.initState();
  }

  getPrefData() async {
    baseUrl = await AppPrefs.getAppServer();

    dataFront = await AppPrefs.getPosFront();
    dataDoor = await AppPrefs.getPosDoor();
    dataLeft = await AppPrefs.getPosLeft();
    dataRight = await AppPrefs.getPosRight();
    dataTop = await AppPrefs.getPosTop();
    dataBottom = await AppPrefs.getPosBottom();
    dataInside = await AppPrefs.getPosInside();
    dataSeal = await AppPrefs.getPosSeal();

    remarkFront = await AppPrefs.getRemarkFront();
    remarkDoor = await AppPrefs.getRemarkDoor();
    remarkLeft = await AppPrefs.getRemarkLeft();
    remarkRight = await AppPrefs.getRemarkRight();
    remarkTop = await AppPrefs.getRemarkTop();
    remarkBottom = await AppPrefs.getRemarkBottom();
    remarkInside = await AppPrefs.getRemarkInside();
    remarkSeal = await AppPrefs.getRemarkSeal();

    if (dataFront.length > 0) {
      numFront = dataFront.split(",").length;
    } else {
      numFront = 0;
    }

    if (dataDoor.length > 0) {
      numDoor = dataDoor.split(",").length;
    } else {
      numDoor = 0;
    }

    if (dataLeft.length > 0) {
      numLeft = dataLeft.split(",").length;
    } else {
      numLeft = 0;
    }

    if (dataRight.length > 0) {
      numRight = dataRight.split(",").length;
    } else {
      numRight = 0;
    }

    if (dataTop.length > 0) {
      numTop = dataTop.split(",").length;
    } else {
      numTop = 0;
    }

    if (dataBottom.length > 0) {
      numBottom = dataBottom.split(",").length;
    } else {
      numBottom = 0;
    }

    if (dataInside.length > 0) {
      numInside = dataInside.split(",").length;
    } else {
      numInside = 0;
    }

    if (dataSeal.length > 0) {
      numSeal = dataSeal.split(",").length;
    } else {
      numSeal = 0;
    }

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);
    return WillPopScope(
      onWillPop: (dataFront.length > 0 ||
              dataDoor.length > 0 ||
              dataLeft.length > 0 ||
              dataRight.length > 0 ||
              dataTop.length > 0 ||
              dataBottom.length > 0 ||
              dataInside.length > 0 ||
              dataSeal.length > 0)
          ? _willPopCallback
          : _Pop,
      child: Scaffold(
          backgroundColor: AppTheme.nearlyWhite,
          appBar: new AppBar(
            leading: BackButton(
              color: Colors.white,
//              onPressed: (){
//              showDialog(
//                context: context,
//                builder: (context) => new AlertDialog(
//                  title: new Text('Are you sure?'),
//                  content: new Text('Do you want to leave without save?'),
//                  actions: <Widget>[
//                    new FlatButton(
//                      onPressed: () => Navigator.of(context).pop(false),
//                      child: new Text('No'),
//                    ),
//                    new FlatButton(
//                      onPressed: () {
//
//                        Navigator.of(context).pop(true);
//                        },
//                      child: new Text('Yes'),
//                    ),
//                  ],
//                ),
//              );
//            },
            ),
            title: new Text(
              "${widget.docNo} - GATE IN",
              style: AppTheme.title_white,
            ),
            backgroundColor: AppTheme.colorPrimary,
          ),
          body: ListView(children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(
                  left: 16.0, right: 16, top: 16, bottom: 8),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Stack(
                      children: <Widget>[
                        GestureDetector(
                          onTap: () {
//                          Navigator.push(
//                              context,
//                              MaterialPageRoute(
//                                  builder: (_) => DepotOutImages(
//                                        title: "FRONT",
//                                        savedPrefs: dataFront,
//                                      )));
                            startActivityWithResult(
                                "FRONT", dataFront, remarkFront);
                          },
                          child: Container(
                            height: 120,
                            width: double.infinity,
                            child: Card(
                              elevation: 3.0,
                              child: Padding(
                                padding:
                                    const EdgeInsets.only(top: 8.0, bottom: 8),
                                child: Column(
                                  children: <Widget>[
                                    Expanded(
                                        child: Image.asset(
                                            "assets/images/front.png")),
                                    SizedBox(
                                      height: 8,
                                    ),
                                    Text("FRONT", style: AppTheme.titlePosition)
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                        numFront != 0
                            ? Positioned(
                                right: 8,
                                top: 8,
                                child: Container(
                                    height: 20,
                                    width: 20,
                                    decoration: BoxDecoration(
                                        color: Colors.red,
                                        borderRadius:
                                            BorderRadius.circular(20)),
                                    child: Center(
                                        child: Text(numFront.toString(),
                                            style: TextStyle(
                                                color: Colors.white)))),
                              )
                            : SizedBox()
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Expanded(
                    child: Stack(
                      children: <Widget>[
                        GestureDetector(
                          onTap: () {
//                          Navigator.push(
//                              context,
//                              MaterialPageRoute(
//                                  builder: (_) => DepotOutImages(
//                                        title: "DOOR",
//                                        savedPrefs: dataDoor,
//                                      )));
                            startActivityWithResult(
                                "DOOR", dataDoor, remarkDoor);
                          },
                          child: Container(
                            height: 120,
                            width: double.infinity,
                            child: Card(
                              elevation: 3.0,
                              child: Padding(
                                padding:
                                    const EdgeInsets.only(top: 8.0, bottom: 8),
                                child: Column(
                                  children: <Widget>[
                                    Expanded(
                                        child: Image.asset(
                                            "assets/images/door.png")),
                                    SizedBox(
                                      height: 8,
                                    ),
                                    Text("DOOR", style: AppTheme.titlePosition)
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                        numDoor != 0
                            ? Positioned(
                                right: 8,
                                top: 8,
                                child: Container(
                                    height: 20,
                                    width: 20,
                                    decoration: BoxDecoration(
                                        color: Colors.red,
                                        borderRadius:
                                            BorderRadius.circular(20)),
                                    child: Center(
                                        child: Text(numDoor.toString(),
                                            style: TextStyle(
                                                color: Colors.white)))),
                              )
                            : SizedBox()
                      ],
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 16.0, right: 16, top: 8, bottom: 8),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Stack(
                      children: <Widget>[
                        GestureDetector(
                          onTap: () {
//                          Navigator.push(
//                              context,
//                              MaterialPageRoute(
//                                  builder: (_) => DepotOutImages(
//                                        title: "LEFT",
//                                        savedPrefs: dataLeft,
//                                      )));
                            startActivityWithResult(
                                "LEFT", dataLeft, remarkLeft);
                          },
                          child: Container(
                            height: 120,
                            width: double.infinity,
                            child: Card(
                              elevation: 3.0,
                              child: Padding(
                                padding:
                                    const EdgeInsets.only(top: 8.0, bottom: 8),
                                child: Column(
                                  children: <Widget>[
                                    Expanded(
                                        child: Image.asset(
                                            "assets/images/left.png")),
                                    SizedBox(
                                      height: 8,
                                    ),
                                    Text("LEFT", style: AppTheme.titlePosition)
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                        numLeft != 0
                            ? Positioned(
                                right: 8,
                                top: 8,
                                child: Container(
                                    height: 20,
                                    width: 20,
                                    decoration: BoxDecoration(
                                        color: Colors.red,
                                        borderRadius:
                                            BorderRadius.circular(20)),
                                    child: Center(
                                        child: Text(numLeft.toString(),
                                            style: TextStyle(
                                                color: Colors.white)))),
                              )
                            : SizedBox()
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Expanded(
                    child: Stack(
                      children: <Widget>[
                        GestureDetector(
                          onTap: () {
//                          Navigator.push(
//                              context,
//                              MaterialPageRoute(
//                                  builder: (_) => DepotOutImages(
//                                        title: "RIGHT",
//                                        savedPrefs: dataRight,
//                                      )));
                            startActivityWithResult(
                                "RIGHT", dataRight, remarkRight);
                          },
                          child: Container(
                            height: 120,
                            width: double.infinity,
                            child: Card(
                              elevation: 3.0,
                              child: Padding(
                                padding:
                                    const EdgeInsets.only(top: 8.0, bottom: 8),
                                child: Column(
                                  children: <Widget>[
                                    Expanded(
                                        child: Image.asset(
                                            "assets/images/right.png")),
                                    SizedBox(
                                      height: 8,
                                    ),
                                    Text("RIGHT", style: AppTheme.titlePosition)
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                        numRight != 0
                            ? Positioned(
                                right: 8,
                                top: 8,
                                child: Container(
                                    height: 20,
                                    width: 20,
                                    decoration: BoxDecoration(
                                        color: Colors.red,
                                        borderRadius:
                                            BorderRadius.circular(20)),
                                    child: Center(
                                        child: Text(numRight.toString(),
                                            style: TextStyle(
                                                color: Colors.white)))),
                              )
                            : SizedBox()
                      ],
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 16.0, right: 16, top: 8, bottom: 8),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Stack(
                      children: <Widget>[
                        GestureDetector(
                          onTap: () {
//                          Navigator.push(
//                              context,
//                              MaterialPageRoute(
//                                  builder: (_) => DepotOutImages(
//                                        title: "TOP",
//                                        savedPrefs: dataTop,
//                                      )));
                            startActivityWithResult("TOP", dataTop, remarkTop);
                          },
                          child: Container(
                            height: 120,
                            width: double.infinity,
                            child: Card(
                              elevation: 3.0,
                              child: Padding(
                                padding:
                                    const EdgeInsets.only(top: 8.0, bottom: 8),
                                child: Column(
                                  children: <Widget>[
                                    Expanded(
                                        child: Image.asset(
                                            "assets/images/top.png")),
                                    SizedBox(
                                      height: 8,
                                    ),
                                    Text("TOP", style: AppTheme.titlePosition)
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                        numTop != 0
                            ? Positioned(
                                right: 8,
                                top: 8,
                                child: Container(
                                    height: 20,
                                    width: 20,
                                    decoration: BoxDecoration(
                                        color: Colors.red,
                                        borderRadius:
                                            BorderRadius.circular(20)),
                                    child: Center(
                                        child: Text(numTop.toString(),
                                            style: TextStyle(
                                                color: Colors.white)))),
                              )
                            : SizedBox()
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Expanded(
                    child: Stack(
                      children: <Widget>[
                        GestureDetector(
                          onTap: () {
//                          Navigator.push(
//                              context,
//                              MaterialPageRoute(
//                                  builder: (_) => DepotOutImages(
//                                        title: "BOTTOM",
//                                        savedPrefs: dataBottom,
//                                      )));
                            startActivityWithResult(
                                "BOTTOM", dataBottom, remarkBottom);
                          },
                          child: Container(
                            height: 120,
                            width: double.infinity,
                            child: Card(
                              elevation: 3.0,
                              child: Padding(
                                padding:
                                    const EdgeInsets.only(top: 8.0, bottom: 8),
                                child: Column(
                                  children: <Widget>[
                                    Expanded(
                                        child: Image.asset(
                                            "assets/images/bottom.png")),
                                    SizedBox(
                                      height: 8,
                                    ),
                                    Text("BOTTOM",
                                        style: AppTheme.titlePosition)
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                        numBottom != 0
                            ? Positioned(
                                right: 8,
                                top: 8,
                                child: Container(
                                    height: 20,
                                    width: 20,
                                    decoration: BoxDecoration(
                                        color: Colors.red,
                                        borderRadius:
                                            BorderRadius.circular(20)),
                                    child: Center(
                                        child: Text(numBottom.toString(),
                                            style: TextStyle(
                                                color: Colors.white)))),
                              )
                            : SizedBox()
                      ],
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 16.0, right: 16, top: 8, bottom: 8),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Stack(
                      children: <Widget>[
                        GestureDetector(
                          onTap: () {
//                          Navigator.push(
//                              context,
//                              MaterialPageRoute(
//                                  builder: (_) => DepotOutImages(
//                                        title: "INSIDE",
//                                        savedPrefs: dataInside,
//                                      )));
                            startActivityWithResult(
                                "INSIDE", dataInside, remarkInside);
                          },
                          child: Container(
                            height: 120,
                            width: double.infinity,
                            child: Card(
                              elevation: 3.0,
                              child: Padding(
                                padding:
                                    const EdgeInsets.only(top: 8.0, bottom: 8),
                                child: Column(
                                  children: <Widget>[
                                    Expanded(
                                        child: Image.asset(
                                            "assets/images/inside.png")),
                                    SizedBox(
                                      height: 8,
                                    ),
                                    Text("INSIDE",
                                        style: AppTheme.titlePosition)
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                        numInside != 0
                            ? Positioned(
                                right: 8,
                                top: 8,
                                child: Container(
                                    height: 20,
                                    width: 20,
                                    decoration: BoxDecoration(
                                        color: Colors.red,
                                        borderRadius:
                                            BorderRadius.circular(20)),
                                    child: Center(
                                        child: Text(numInside.toString(),
                                            style: TextStyle(
                                                color: Colors.white)))),
                              )
                            : SizedBox()
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Expanded(
                    child: Stack(
                      children: <Widget>[
                        GestureDetector(
                          onTap: () {
//                          Navigator.push(
//                              context,
//                              MaterialPageRoute(
//                                  builder: (_) => DepotOutImages(
//                                        title: "SEAL",
//                                        savedPrefs: dataSeal,
//                                      )));
                            startActivityWithResult(
                                "SEAL", dataSeal, remarkSeal);
                          },
                          child: Container(
                            height: 120,
                            width: double.infinity,
                            child: Card(
                              elevation: 3.0,
                              child: Padding(
                                padding:
                                    const EdgeInsets.only(top: 8.0, bottom: 8),
                                child: Column(
                                  children: <Widget>[
                                    Expanded(
                                        child: Image.asset(
                                            "assets/images/seal.png")),
                                    SizedBox(
                                      height: 8,
                                    ),
                                    Text("SEAL", style: AppTheme.titlePosition)
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                        numSeal != 0
                            ? Positioned(
                                right: 8,
                                top: 8,
                                child: Container(
                                    height: 20,
                                    width: 20,
                                    decoration: BoxDecoration(
                                        color: Colors.red,
                                        borderRadius:
                                            BorderRadius.circular(20)),
                                    child: Center(
                                        child: Text(numSeal.toString(),
                                            style: TextStyle(
                                                color: Colors.white)))),
                              )
                            : SizedBox()
                      ],
                    ),
                  )
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Container(
                  color: Colors.transparent,
                  width: 170.0,
                  margin: new EdgeInsets.only(bottom: 10.0, top: 10.0),
                  child: new RaisedButton(
                    padding: const EdgeInsets.all(12.0),
                    child: new Text(
                      'Upload',
                      style: AppTheme.btnText,
                    ),
                    onPressed: () {
                      if (dataFront.length > 0 ||
                          dataDoor.length > 0 ||
                          dataLeft.length > 0 ||
                          dataRight.length > 0 ||
                          dataTop.length > 0 ||
                          dataBottom.length > 0 ||
                          dataInside.length > 0 ||
                          dataSeal.length > 0) {
                        _uploadImage();
                      } else {
                        Fluttertoast.showToast(
                            msg: "No attachment added to the Container.",
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.CENTER,
                            timeInSecForIosWeb: 1,
                            backgroundColor: Colors.red,
                            textColor: Colors.white,
                            fontSize: 16.0
                        );
                      }
                    },
                    color: dataFront.length > 0 ||
                            dataDoor.length > 0 ||
                            dataLeft.length > 0 ||
                            dataRight.length > 0 ||
                            dataTop.length > 0 ||
                            dataBottom.length > 0 ||
                            dataInside.length > 0 ||
                            dataSeal.length > 0
                        ? AppTheme.colorPrimary
                        : Colors.grey,
//                              shape: RoundedRectangleBorder(
//                                  borderRadius: new BorderRadius.circular(10.0),
//                                  side: BorderSide(color: Colors.transparent)),
                  ),
                ),
              ],
            ),
          ])),
    );
  }

  startActivityWithResult(String title, String data, String remarks) async {
    var result = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (_) => GateInImages(
                  title: title,
                  savedPrefs: data,
                  savedRemark: remarks,
                  docNo: widget.docNo,
                )));

    if (result == "saved") {
      getPrefData();
    }
  }

//  void _submitCondition() async {
//
////      pr.show();
//      Map dataKey = {"TruckInDocNo":widget.containerModel.truckInDocNo,"ContainerNo":widget.containerModel.containerNo,"IsAcceptOrReject":'1',"ContainerStatus":"AV","Remark":""
//      };
//
//      List list = [];
//      list.add(json.encode(dataKey));
//
//      Map data = {'Key': 'Data',
//        'Value':"$list"};
//
//      final response =
//      await _httpHelper.post(baseUrl,UrlConfig.APP_CONDITION_CHECK, json.encode(data));
////      pr.dismiss();
//      Response res = Response.fromJson(response);
//      print("PKM" + res.statusCode.toString());
////      pr.hide();
//      if (res.statusCode == 200) {
//        _uploadImage();
//      } else {
//        Toast.show(res.errorMessage, context, duration: 3);
//      }
//  }

  void _uploadImage() async {
    Map dataKey;
    List list = [];

    pr.show();
    if (dataFront.length > 0) {
      for (int i = 0; i < dataFront.split(',').length; i++) {
        dataKey = {
          "Position": "F",
          "DocType": "GI",
          "DocumentNo": widget.docNo,
          "FileType": "jpg",
          "FileContent": dataFront.split(",")[i],
          "ContainerNo": widget.containerNo,
          "Remarks": remarkFront
        };
        list.add(json.encode(dataKey));
      }
    }

    if (dataDoor.length > 0) {
      for (int i = 0; i < dataDoor.split(',').length; i++) {
        dataKey = {
          "Position": "D",
          "DocType": "GI",
          "DocumentNo": widget.docNo,
          "FileType": "jpg",
          "FileContent": dataDoor.split(",")[i],
          "ContainerNo": widget.containerNo,
          "Remarks": remarkDoor
        };
        list.add(json.encode(dataKey));
      }
    }

    if (dataLeft.length > 0) {
      for (int i = 0; i < dataLeft.split(',').length; i++) {
        dataKey = {
          "Position": "L",
          "DocType": "GI",
          "DocumentNo": widget.docNo,
          "FileType": "jpg",
          "FileContent": dataLeft.split(",")[i],
          "ContainerNo": widget.containerNo,
          "Remarks": remarkLeft
        };
        list.add(json.encode(dataKey));
      }
    }

    if (dataRight.length > 0) {
      for (int i = 0; i < dataRight.split(',').length; i++) {
        dataKey = {
          "Position": "R",
          "DocType": "GI",
          "DocumentNo": widget.docNo,
          "FileType": "jpg",
          "FileContent": dataRight.split(",")[i],
          "ContainerNo": widget.containerNo,
          "Remarks": remarkRight
        };
        list.add(json.encode(dataKey));
      }
    }

    if (dataTop.length > 0) {
      for (int i = 0; i < dataTop.split(',').length; i++) {
        dataKey = {
          "Position": "T",
          "DocType": "GI",
          "DocumentNo": widget.docNo,
          "FileType": "jpg",
          "FileContent": dataTop.split(",")[i],
          "ContainerNo": widget.containerNo,
          "Remarks": remarkTop
        };
        list.add(json.encode(dataKey));
      }
    }

    if (dataBottom.length > 0) {
      for (int i = 0; i < dataBottom.split(',').length; i++) {
        dataKey = {
          "Position": "B",
          "DocType": "GI",
          "DocumentNo": widget.docNo,
          "FileType": "jpg",
          "FileContent": dataBottom.split(",")[i],
          "ContainerNo": widget.containerNo,
          "Remarks": remarkBottom
        };
        list.add(json.encode(dataKey));
      }
    }

    if (dataInside.length > 0) {
      for (int i = 0; i < dataInside.split(',').length; i++) {
        dataKey = {
          "Position": "I",
          "DocType": "GI",
          "DocumentNo": widget.docNo,
          "FileType": "jpg",
          "FileContent": dataInside.split(",")[i],
          "ContainerNo": widget.containerNo,
          "Remarks": remarkInside
        };
        list.add(json.encode(dataKey));
      }
    }

    if (dataSeal.length > 0) {
      for (int i = 0; i < dataSeal.split(',').length; i++) {
        dataKey = {
          "Position": "S",
          "DocType": "GI",
          "DocumentNo": widget.containerNo,
          "FileType": "jpg",
          "FileContent": dataSeal.split(",")[i],
          "ContainerNo": widget.containerNo,
          "Remarks": remarkSeal
        };
        list.add(json.encode(dataKey));
      }
    }

    Map data = {'Key': 'Data', 'Value': "$list"};

    final response = await _httpHelper.post(
        baseUrl, UrlConfig.APP_UPLOAD_IMAGES, json.encode(data));
    Response res = Response.fromJson(response);
    print("PKM" + res.statusCode.toString());
    pr.hide();
    if (res.statusCode == 200) {
      if (res.results.length > 0) {
        if (pr.isShowing() == true) {
          pr.dismiss();
          Fluttertoast.showToast(
              msg: "Gate In Item [${widget.containerNo}] attachment uploaded.",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0
          );
          Navigator.pop(context, "success");
        } else {
          Fluttertoast.showToast(
              msg: "Gate In Item [${widget.containerNo}] attachment uploaded.",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0
          );
          Navigator.pop(context, "success");
        }
      }
    } else {
      if (res.errorMessage.length > 0) {
        Fluttertoast.showToast(
            msg: res.errorMessage,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );

        if (res.errorMessage.contains("AccessToken"))
          new force_logout(context).doForceLogout();
      } else {
        Fluttertoast.showToast(
            msg: response['message'],
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
      }
    }
  }

  Future<bool> _willPopCallback() async {
    return (await showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            title: new Text('Are you sure?', style: AppTheme.titleDlg),
            content: new Text('Do you want to leave without save?',
                style: AppTheme.titleDescDlg),
            actions: <Widget>[
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: new Text(
                  'Discard',
                  style: AppTheme.titleBtnDlgRed,
                ),
              ),
            ],
          ),
        )) ??
        false;
  }

  Future<bool> _Pop() async {
    return true;
  }
}
