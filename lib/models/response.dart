class Response {
  String errorDetail;
  String errorMessage;
  List<Results> results;
  int statusCode;
  String statusDescription;

  Response(
      {this.errorDetail,
        this.errorMessage,
        this.results,
        this.statusCode,
        this.statusDescription});

  Response.fromJson(Map<String, dynamic> json) {
    errorDetail = json['ErrorDetail'];
    errorMessage = json['ErrorMessage'];
    if (json['Results'] != null) {
      results = new List<Results>();
      json['Results'].forEach((v) {
        results.add(new Results.fromJson(v));
      });
    }
    statusCode = json['StatusCode'];
    statusDescription = json['StatusDescription'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ErrorDetail'] = this.errorDetail;
    data['ErrorMessage'] = this.errorMessage;
    if (this.results != null) {
      data['Results'] = this.results.map((v) => v.toJson()).toList();
    }
    data['StatusCode'] = this.statusCode;
    data['StatusDescription'] = this.statusDescription;
    return data;
  }
}

class Results {
  String key;
  var value;

  Results({this.key, this.value});

  Results.fromJson(Map<String, dynamic> json) {
    key = json['Key'];
    value = json['Value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Key'] = this.key;
    data['Value'] = this.value;
    return data;
  }
}