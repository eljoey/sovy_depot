class TaskDetailsModel {
  List<Header> header;
  List<TaskList> taskList;

  TaskDetailsModel({this.header, this.taskList});

  TaskDetailsModel.fromJson(Map<String, dynamic> json) {
    if (json['Header'] != null) {
      header = new List<Header>();
      json['Header'].forEach((v) {
        header.add(new Header.fromJson(v));
      });
    }
    if (json['TaskList'] != null) {
      taskList = new List<TaskList>();
      json['TaskList'].forEach((v) {
        taskList.add(new TaskList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.header != null) {
      data['Header'] = this.header.map((v) => v.toJson()).toList();
    }
    if (this.taskList != null) {
      data['TaskList'] = this.taskList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Header {
  String eORNo;
  String operatorCode;
  String containerNo;
  String iSOCode;
  String allowToEdit;

  Header({this.eORNo, this.operatorCode, this.containerNo, this.iSOCode,this.allowToEdit});

  Header.fromJson(Map<String, dynamic> json) {
    eORNo = json['EORNo'];
    operatorCode = json['OperatorCode'];
    containerNo = json['ContainerNo'];
    iSOCode = json['ISOCode'];
    allowToEdit = json['AllowToEdit'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['EORNo'] = this.eORNo;
    data['OperatorCode'] = this.operatorCode;
    data['ContainerNo'] = this.containerNo;
    data['ISOCode'] = this.iSOCode;
    data['AllowToEdit'] = this.allowToEdit;
    return data;
  }
}

class TaskList {
  String eORCategory;
  String customerRef;
  String customerName;
  String typeSize;
  String startDate;
  String endDate;
  String status;

  TaskList(
      {this.eORCategory,
        this.customerRef,
        this.customerName,
        this.typeSize,
        this.startDate,
        this.endDate,
        this.status});

  TaskList.fromJson(Map<String, dynamic> json) {
    eORCategory = json['EORCategory'];
    customerRef = json['CustomerRef'];
    customerName = json['CustomerName'];
    typeSize = json['TypeSize'];
    startDate = json['StartDate'];
    endDate = json['EndDate'];
    status = json['Status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['EORCategory'] = this.eORCategory;
    data['CustomerRef'] = this.customerRef;
    data['CustomerName'] = this.customerName;
    data['TypeSize'] = this.typeSize;
    data['StartDate'] = this.startDate;
    data['EndDate'] = this.endDate;
    data['Status'] = this.status;
    return data;
  }
}