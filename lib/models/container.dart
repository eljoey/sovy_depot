class ContainerModel {
  String type;
  String containerNo;
  String typeSize;
  String operatorCode;
  String pickDropTime;
  bool isFileAttached;
  bool isSealRequired;
  String truckInDocNo;
  String shipper;
  String commodity;
  String primeMoverId;
  String primeMoverRegNo;
  String remarks;

  ContainerModel(
      {this.type,
        this.containerNo,
        this.typeSize,
        this.operatorCode,
        this.pickDropTime,
        this.isFileAttached,
        this.isSealRequired,
        this.truckInDocNo,
        this.shipper,
        this.commodity,
        this.primeMoverId,
        this.primeMoverRegNo,
        this.remarks});

  ContainerModel.fromJson(Map<String, dynamic> json) {
    type = json['Type'];
    containerNo = json['ContainerNo'];
    typeSize = json['TypeSize'];
    operatorCode = json['OperatorCode'];
    pickDropTime = json['PickDropTime'];
    isFileAttached = json['IsFileAttached'];
    isSealRequired = json['IsSealRequired'];
    truckInDocNo = json['TruckInDocNo'];
    shipper = json['Shipper'];
    commodity = json['Commodity'];
    primeMoverId = json['PrimeMoverId'];
    primeMoverRegNo = json['PrimeMoverRegNo'];
    remarks = json['Remarks'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Type'] = this.type;
    data['ContainerNo'] = this.containerNo;
    data['TypeSize'] = this.typeSize;
    data['OperatorCode'] = this.operatorCode;
    data['PickDropTime'] = this.pickDropTime;
    data['IsFileAttached'] = this.isFileAttached;
    data['IsSealRequired'] = this.isSealRequired;
    data['TruckInDocNo'] = this.truckInDocNo;
    data['Shipper'] = this.shipper;
    data['Commodity'] = this.commodity;
    data['PrimeMoverId'] = this.primeMoverId;
    data['PrimeMoverRegNo'] = this.primeMoverRegNo;
    data['Remarks'] = this.remarks;
    return data;
  }
}