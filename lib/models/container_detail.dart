class ContainerDetailModel {
  String containerNo;
  String containerStatus;
  String typeSize;
  String iSOCode;
  String containerGrade;
  String mGW;
  String manufactureDate;
  String gateInDate;
  String aging;
  String operatorCode;
  String yardZone;
  String containerRemarks;
  String nominatedBookingNo;
  String nominatedBookingRef;
  String gateInNo;
  String isBlock;
  String blockReason;

  ContainerDetailModel(
      {this.containerNo,
        this.containerStatus,
        this.typeSize,
        this.iSOCode,
        this.containerGrade,
        this.mGW,
        this.manufactureDate,
        this.gateInDate,
        this.aging,
        this.operatorCode,
        this.yardZone,
        this.containerRemarks,
        this.nominatedBookingNo,
        this.nominatedBookingRef,
        this.gateInNo,
        this.isBlock,
        this.blockReason});

  ContainerDetailModel.fromJson(Map<String, dynamic> json) {
    containerNo = json['ContainerNo'];
    containerStatus = json['ContainerStatus'];
    typeSize = json['TypeSize'];
    iSOCode = json['ISOCode'];
    containerGrade = json['ContainerGrade'];
    mGW = json['MGW'];
    manufactureDate = json['ManufactureDate'];
    gateInDate = json['GateInDate'];
    aging = json['Aging'];
    operatorCode = json['OperatorCode'];
    yardZone = json['YardZone'];
    containerRemarks = json['ContainerRemarks'];
    nominatedBookingNo = json['NominatedBookingNo'];
    nominatedBookingRef = json['NominatedBookingRef'];
    gateInNo = json['GateInNo'];
    isBlock = json['IsBlock'];
    blockReason = json['BlockReason'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ContainerNo'] = this.containerNo;
    data['ContainerStatus'] = this.containerStatus;
    data['TypeSize'] = this.typeSize;
    data['ISOCode'] = this.iSOCode;
    data['ContainerGrade'] = this.containerGrade;
    data['MGW'] = this.mGW;
    data['ManufactureDate'] = this.manufactureDate;
    data['GateInDate'] = this.gateInDate;
    data['Aging'] = this.aging;
    data['OperatorCode'] = this.operatorCode;
    data['YardZone'] = this.yardZone;
    data['ContainerRemarks'] = this.containerRemarks;
    data['NominatedBookingNo'] = this.nominatedBookingNo;
    data['NominatedBookingRef'] = this.nominatedBookingRef;
    data['GateInNo'] = this.gateInNo;
    data['IsBlock'] = this.isBlock;
    data['BlockReason'] = this.blockReason;
    return data;
  }
}
