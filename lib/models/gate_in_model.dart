class GateInModel {
  String type;
  String containerNo;
  String containerStatus;
  String typeSize;
  String iSOCode;
  String containerGrade;
  String mGW;
  String tareWeight;
  String operatorCode;
  String haulierCode;
  String primeMoverRegNo;
  String primeMoverId;
  String remarks;
  String gateInDocNo;
  String preGateInNo;
  String isCanEditOperator;
  String isCanEditHaulier;
  String isEmptyReject;
  String isFileAttached;

  GateInModel(
      {this.type,
        this.containerNo,
        this.containerStatus,
        this.typeSize,
        this.iSOCode,
        this.containerGrade,
        this.mGW,
        this.tareWeight,
        this.operatorCode,
        this.haulierCode,
        this.primeMoverRegNo,
        this.primeMoverId,
        this.remarks,
        this.gateInDocNo,
        this.preGateInNo,
        this.isCanEditOperator,
        this.isCanEditHaulier,
        this.isEmptyReject,
        this.isFileAttached});

  GateInModel.fromJson(Map<String, dynamic> json) {
    type = json['Type'];
    containerNo = json['ContainerNo'];
    containerStatus = json['ContainerStatus'];
    typeSize = json['TypeSize'];
    iSOCode = json['ISOCode'];
    containerGrade = json['ContainerGrade'];
    mGW = json['MGW'];
    tareWeight = json['TareWeight'];
    operatorCode = json['OperatorCode'];
    haulierCode = json['HaulierCode'];
    primeMoverRegNo = json['PrimeMoverRegNo'];
    primeMoverId = json['PrimeMoverId'];
    remarks = json['Remarks'];
    gateInDocNo = json['GateInDocNo'];
    preGateInNo = json['PreGateInNo'];
    isCanEditOperator = json['IsCanEditOperator'];
    isCanEditHaulier = json['IsCanEditHaulier'];
    isEmptyReject = json['IsEmptyReject'];
    isFileAttached = json['IsFileAttached'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Type'] = this.type;
    data['ContainerNo'] = this.containerNo;
    data['ContainerStatus'] = this.containerStatus;
    data['TypeSize'] = this.typeSize;
    data['ISOCode'] = this.iSOCode;
    data['ContainerGrade'] = this.containerGrade;
    data['MGW'] = this.mGW;
    data['TareWeight'] = this.tareWeight;
    data['OperatorCode'] = this.operatorCode;
    data['HaulierCode'] = this.haulierCode;
    data['PrimeMoverRegNo'] = this.primeMoverRegNo;
    data['PrimeMoverId'] = this.primeMoverId;
    data['Remarks'] = this.remarks;
    data['GateInDocNo'] = this.gateInDocNo;
    data['PreGateInNo'] = this.preGateInNo;
    data['IsCanEditOperator'] = this.isCanEditOperator;
    data['IsCanEditHaulier'] = this.isCanEditHaulier;
    data['IsEmptyReject'] = this.isEmptyReject;
    data['IsFileAttached'] = this.isFileAttached;
    return data;
  }
}
