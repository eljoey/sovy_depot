class ContainerTruck {
  String containerNo;
  String typeSize;
  String iSOCode;
  String containerGrade;
  String mGW;

  ContainerTruck(
      {this.containerNo,
        this.typeSize,
        this.iSOCode,
        this.containerGrade,
        this.mGW});

  ContainerTruck.fromJson(Map<String, dynamic> json) {
    containerNo = json['ContainerNo'];
    typeSize = json['TypeSize'];
    iSOCode = json['ISOCode'];
    containerGrade = json['ContainerGrade'];
    mGW = json['MGW'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ContainerNo'] = this.containerNo;
    data['TypeSize'] = this.typeSize;
    data['ISOCode'] = this.iSOCode;
    data['ContainerGrade'] = this.containerGrade;
    data['MGW'] = this.mGW;
    return data;
  }
}