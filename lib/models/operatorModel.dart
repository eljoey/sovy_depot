class OperatorCode {
  String operatorCode;
  String haulierCode;
  String primeMoverRegNo;
  String primeMoverId;
  String driverICNo;
  String driverName;
  String typeSize;

  OperatorCode(
      {this.operatorCode,
        this.haulierCode,
        this.primeMoverRegNo,
        this.primeMoverId,
        this.driverICNo,
        this.driverName,
        this.typeSize});

  OperatorCode.fromJson(Map<String, dynamic> json) {
    operatorCode = json['OperatorCode'];
    haulierCode = json['HaulierCode'];
    primeMoverRegNo = json['PrimeMoverRegNo'];
    primeMoverId = json['PrimeMoverId'];
    driverICNo = json['DriverICNo'];
    driverName = json['DriverName'];
    typeSize = json['TypeSize'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['OperatorCode'] = this.operatorCode;
    data['HaulierCode'] = this.haulierCode;
    data['PrimeMoverRegNo'] = this.primeMoverRegNo;
    data['PrimeMoverId'] = this.primeMoverId;
    data['DriverICNo'] = this.driverICNo;
    data['DriverName'] = this.driverName;
    data['TypeSize'] = this.typeSize;
    return data;
  }
}