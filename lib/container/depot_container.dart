import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:barcode_scan2/barcode_scan2.dart';
import 'package:depot/drop/gateInPosition.dart';
import 'package:depot/models/response.dart';
import 'package:depot/styles/appTheme.dart';
import 'package:depot/utils/ApiBaseHelper.dart';
import 'package:depot/utils/AppPrefs.dart';
import 'package:depot/utils/ShareMethod.dart';
import 'package:depot/utils/UrlConfig.dart';
import 'package:depot/utils/force_logout.dart';
import 'package:depot/utils/progress_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_mlkit_text_recognition/google_mlkit_text_recognition.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:depot/models/container.dart';
import 'package:flutter/services.dart';
import 'package:recase/recase.dart';
import 'dart:math' as math;

import '../models/container_detail.dart';

class ContainerDetail extends StatefulWidget {
  final int bit;

  const ContainerDetail({Key key, this.bit}) : super(key: key);

  @override
  _ContainerDetailState createState() => _ContainerDetailState();
}

class _ContainerData {
  String _containerNo = '';
  String _containerStatus = '';
  String _containerRemark = '';
  String _containerReason = '';
  String _containerYard = '';
}

class _ContainerDetailState extends State<ContainerDetail> {
  ApiBaseHelper _httpHelper = ApiBaseHelper();
  final GlobalKey<FormState> _keyContainer = new GlobalKey<FormState>();
  final GlobalKey<FormState> _keyStatus = new GlobalKey<FormState>();
  final GlobalKey<FormState> _keyBlock = new GlobalKey<FormState>();
  final GlobalKey<FormState> _keyYard = new GlobalKey<FormState>();
  ProgressDialog pr;
  _ContainerData _data = new _ContainerData();
  String result, baseUrl, defaultStatus;
  List<ContainerModel> listContainer = [];
  TextEditingController txtSearchController = new TextEditingController();
  FocusNode focusContainer = new FocusNode();
  List<Widget> listISO = List(),
      listCrStatus = List(),
      listYard = List(),
      listOperator = List();
  bool _isCheck, _isBlock = false;
  List crContainerTemp = [];
  List yardZoneTemp = [];
  bool blocked = false;
  ContainerDetailModel containerDetailModel;
  final beforeCapitalLetter = RegExp(r"(?=[A-Z])");

  final dobFormat2 = DateFormat("yyyy-MM-dd");
  final dobFormat = DateFormat("yyyy-MM-dd hh:mm:ss");
  final dobFormatStore = DateFormat("yyyy-MM-ddThh:mm:ss");
  String gateInDoc = "", _checkContainer;
  FocusNode focusNode = new FocusNode();
  final ImagePicker _picker = ImagePicker();

  TextEditingController textController = new TextEditingController();

  ScrollController _scrollController = new ScrollController();

  TextEditingController tecContainerNo = new TextEditingController();
  TextEditingController tecContainerDetail = new TextEditingController();
  TextEditingController tecContainerStatus = new TextEditingController();
  TextEditingController tecRemark = new TextEditingController();
  TextEditingController tecReason = new TextEditingController();
  TextEditingController tecReasonDisplay = new TextEditingController();
  TextEditingController tecYard = new TextEditingController();
  FocusNode focusDetail;

  ScrollController _scrollbarController = ScrollController();

  TextRecognizer textDetector = TextRecognizer();

  _ContainerDetailState();

  Future<Null> _readContainer() async {
    XFile image = await _picker.pickImage(source: ImageSource.camera);

    final inputImage = InputImage.fromFile(File(image.path));

    RecognizedText regText =
        await textDetector.processImage(inputImage);

    print("TEXT " + regText.blocks[0].lines[0].text);

    String text = regText.blocks[0].lines[0].text.replaceAll(" ", "");
    final regex = RegExp(r'^[A-Z]{4}\d{6}$');

    if (regex.hasMatch(text)) {
      tecContainerNo.text = _getCheckDigit(text).toUpperCase();
    } else {
      tecContainerNo.text = text.toUpperCase();
    }
  }

  bool _checkDigit(String containerNo) {
    int value;
    int result = 0;

    if (containerNo.length != 11) {
      Fluttertoast.showToast(
          msg: "Incorrect Container No.",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    } else {
      for (int i = 0; i < 10; i++) {
        if (i < 4) {
          value = (11 * (containerNo[i].codeUnits[0] - 56) ~/ 10) + 1;
          print("value string = " + value.toString());
        } else {
          value = (containerNo[i].codeUnits[0] - 48);
          print("value interger = " + value.toString());
        }

        int newValue = value * math.pow(2, i);

        result += newValue;
        print("resutl = " + result.toString() + " " + i.toString());
      }
    }

    int checkDigit = (result - (result ~/ 11) * 11).toInt();

    if (int.parse(containerNo[10]) != checkDigit) {
      return false;
    } else {
      return true;
    }
  }

  String _getCheckDigit(String containerNo) {
    int value;
    int result = 0;

    for (int i = 0; i < 10; i++) {
      if (i < 4) {
        value = (11 * (containerNo[i].codeUnits[0] - 56) ~/ 10) + 1;
        print("value string = " + value.toString());
      } else {
        value = (containerNo[i].codeUnits[0] - 48);
        print("value interger = " + value.toString());
      }

      int newValue = value * math.pow(2, i);

      result += newValue;
      print("resutl = " + result.toString() + " " + i.toString());
    }

    int checkDigit = (result - (result ~/ 11) * 11).toInt();

    return containerNo + checkDigit.toString();
  }

  @override
  void initState() {
    getData();
    // TODO: implement initState
    super.initState();
  }

  popDialog(String title, List<Widget> list) {
    showCupertinoModalPopup(
      context: context,
      builder: (BuildContext context) => CupertinoActionSheet(
          title: Text(title),
          actions: list,
          cancelButton: CupertinoActionSheetAction(
            child: const Text('Cancel'),
            isDefaultAction: true,
            onPressed: () {
              Navigator.pop(context);
            },
          )),
    );
  }

  // void _settingModalBottomSheet(context, var list) {
  //   showModalBottomSheet(
  //       isScrollControlled: true,
  //       shape: RoundedRectangleBorder(
  //         borderRadius: BorderRadius.vertical(top: Radius.circular(15.0)),
  //       ),
  //       context: context,
  //       builder: (BuildContext bc) {
  //         return DraggableScrollableSheet(
  //             expand: false,
  //             builder:
  //                 (BuildContext context, ScrollController scrollController) {
  //               return Column(
  //                 children: [
  //                   Padding(
  //                     padding: const EdgeInsets.all(4.0),
  //                     child: Text(
  //                       "ISO Code Options:",
  //                       style: TextStyle(color: Colors.black54),
  //                     ),
  //                   ),
  //                   Expanded(
  //                     child: Container(
  //                         width: double.infinity,
  //                         child: Padding(
  //                           padding: const EdgeInsets.only(
  //                               top: 8.0, left: 8, right: 8),
  //                           child: GridView.count(
  //                             childAspectRatio: 2,
  //                             crossAxisCount: 3,
  //                             children: List.generate(list.length, (index) {
  //                               return GestureDetector(
  //                                 onTap: () {
  //                                   Navigator.of(context).pop();
  //                                   setState(() {
  //                                     tecISOCode.text = list[index]['Key'];
  //                                     _data._iSOCode = list[index]['Key'];
  //                                   });
  //                                 },
  //                                 child: Card(
  //                                   child:
  //                                   Center(child: Text(list[index]['Key'])),
  //                                 ),
  //                               );
  //                             }),
  //                           ),
  //                         )),
  //                   ),
  //                 ],
  //               );
  //             });
  //       });
  // }

  getData() async {
    baseUrl = await AppPrefs.getAppServer();
    String lookUp = await AppPrefs.getLookup();
    log("Lookup -- $lookUp");

    crContainerTemp = json.decode(json.decode(lookUp))['CRContainerStatus'];
    yardZoneTemp = json.decode(json.decode(lookUp))['YardZones'];

    for (int i = 0; i < crContainerTemp.length; i++) {
      listCrStatus.add(CupertinoActionSheetAction(
        child: Text(crContainerTemp[i]['Value']),
        onPressed: () {
          setState(() {
            tecContainerStatus.text = crContainerTemp[i]['Value'];
            _data._containerStatus = crContainerTemp[i]['Key'];

//            searchHint = jsonTemp[i]['Value'];
            Navigator.pop(context);
          });
        },
      ));
    }

    for (int i = 0; i < yardZoneTemp.length; i++) {
      listYard.add(CupertinoActionSheetAction(
        child: Text(yardZoneTemp[i]['Value']),
        onPressed: () {
          setState(() {
            tecYard.text = yardZoneTemp[i]['Value'];
            _data._containerYard = yardZoneTemp[i]['Key'];

//            searchHint = jsonTemp[i]['Value'];
            Navigator.pop(context);
          });
        },
      ));
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData media = MediaQuery.of(context);
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);
    // final Size screenSize = media.size;
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(
          color: Colors.white,
        ),
        title: Text(
          "Container Details",
          style: AppTheme.title_white,
        ),
        actions: <Widget>[
          InkWell(
              onTap: () {
                _getContainerDetail();
              },
              child: Icon(
                Icons.refresh,
                color: Colors.white,
                size: 30,
              )),
          SizedBox(
            width: 12,
          ),
          InkWell(
              onTap: () {
                reset();
              },
              child: Icon(
                Icons.restore_page_outlined,
                color: Colors.white,
                size: 30,
              )),
          SizedBox(
            width: 16,
          ),
        ],
      ),
      body: SingleChildScrollView(
          controller: _scrollController,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 10,
                ),
                Focus(
                  onFocusChange: (hasFocus) {
                    if (!hasFocus) {
                      if (_checkContainer != tecContainerNo.text) {
                        _checkContainer = tecContainerNo.text;
                        if (_keyContainer.currentState.validate()) {
                          _keyContainer.currentState.save();
                          if (!_checkDigit(tecContainerNo.text)) {
                            showDialog<void>(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Text('Error'),
                                    content: SingleChildScrollView(
                                      child: ListBody(
                                        children: <Widget>[
                                          Padding(
                                            padding:
                                                const EdgeInsets.only(top: 8.0),
                                            child: new Text(
                                              "Container No. is not comply to ISO6346.!\nDo you wish to continue ?",
                                              style: TextStyle(
                                                fontSize: 20,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    actions: <Widget>[
                                      FlatButton(
                                        child: const Text('Continue'),
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                          _getContainerDetail();
                                        },
                                      ),
                                      FlatButton(
                                        child: const Text('Cancel'),
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                          focusNode.requestFocus();
                                        },
                                      ),
                                    ],
                                  );
                                });
                          } else {
                            _getContainerDetail();
                          }
                        } else {
                          _getContainerDetail();
                        }
                      }
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: Form(
                      key: _keyContainer,
                      child: TextFormField(
                        controller: tecContainerNo,
                        focusNode: focusNode,
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.characters,
                        // Use email input type for emails.
                        // inputFormatters: [
                        //   UpperCaseTextFormatter(),
                        // ],
                        validator: (value) {
                          if (value.length != 11) {
                            return "Container No. must be 11 characters.";
                          }
                          return null;
                        },
                        maxLength: 11,
                        decoration: new InputDecoration(
                            suffixIcon: Container(
                              color: Colors.transparent,
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  _isCheck != null
                                      ? _isCheck
                                          ? Icon(
                                              Icons.check,
                                              color: AppTheme.colorPrimary,
                                            )
                                          : Icon(
                                              Icons.clear,
                                              color: Colors.red,
                                            )
                                      : SizedBox(),
                                  GestureDetector(
                                      onTap: () {
                                        _readContainer();
                                      },
                                      child: Image.asset(
                                        "assets/images/ic_ocr.png",
                                        height: 35,
                                      ))
                                ],
                              ),
                            ),
                            labelText: "Container No* :",
                            labelStyle:
                                TextStyle(fontSize: 18, color: Colors.black38),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: AppTheme.colorPrimary, width: 1.0),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: AppTheme.colorMediumGrey, width: 1.0),
                            ),
                            border: new OutlineInputBorder(),
                            counterText: ""),
                        autofocus: false,
                        onSaved: (value) {
                          this._data._containerNo = value;
                        },
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: Scrollbar(
                    thickness: 5,
                    controller: _scrollbarController,
                    isAlwaysShown: true,
                    child: TextFormField(
                      controller: tecContainerDetail,
                      scrollController: _scrollbarController,
                      keyboardType: TextInputType.text,
                      // Use email input type for emails.
                      textCapitalization: TextCapitalization.characters,
                      // inputFormatters: [
                      //   UpperCaseTextFormatter(),
                      // ],
                      minLines: 4,
                      maxLines: 7,
                      style: tecContainerDetail.text
                              .contains("Container not found")
                          ? TextStyle(color: Colors.red)
                          : null,
                      readOnly: true,
                      focusNode: focusDetail,
                      decoration: new InputDecoration(
                          floatingLabelBehavior: FloatingLabelBehavior.always,
                          labelText: "Container Details :",
                          labelStyle:
                              TextStyle(fontSize: 18, color: Colors.black38),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorPrimary, width: 1.0),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorMediumGrey, width: 1.0),
                          ),
                          disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorMediumGrey, width: 1.0),
                          ),
                          border: new OutlineInputBorder(),
                          errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Theme.of(context).errorColor,
                                width: 1.0),
                          ),
                          errorStyle: TextStyle(
                            color: Theme.of(context)
                                .errorColor, // or any other color
                          ),
                          counterText: ""),
                      autofocus: false,
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Form(
                  key: _keyStatus,
                  child: Column(
                    children: [
                      Padding(
                          padding: const EdgeInsets.only(bottom: 8.0),
                          child: TextFormField(
                            onTap: (){
                              if (!IsBitSet(widget.bit, 20)) {
                                Fluttertoast.showToast(
                                    msg:
                                    "Don't have permission to change Container Status. Please contact your admin");
                              } else {
                                FocusScope.of(context).unfocus();
                                popDialog("Container Status Options", listCrStatus);
                              }
                            },
                            controller: tecContainerStatus,
                            keyboardType: TextInputType.text,
                            textCapitalization: TextCapitalization.characters,
                            // inputFormatters: [
                            //   UpperCaseTextFormatter(),
                            // ],
                            readOnly: true,
                            decoration: new InputDecoration(
                                suffixIcon: Icon(Icons.arrow_drop_down),
                                labelText: "Container Status :",
                                labelStyle:
                                    TextStyle(fontSize: 18, color: Colors.black38),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: AppTheme.colorPrimary, width: 1.0),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: AppTheme.colorMediumGrey, width: 1.0),
                                ),
                                border: new OutlineInputBorder(),
                                disabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: AppTheme.colorMediumGrey, width: 1.0),
                                ),
                                counterText: ""),
                            autofocus: false,
                          ),
                        ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 8.0),
                        child: Focus(
                          onFocusChange: (hasFocus) {
                            if (!IsBitSet(widget.bit, 19)) {
                              if (hasFocus) {
                                Fluttertoast.showToast(
                                    msg:
                                    "Don't have permission to change Container Remark. Please contact your admin");
                                FocusScope.of(context).unfocus();
                              }
                            }
                          },
                          child: TextFormField(
                            controller: tecRemark,
                            keyboardType: TextInputType.multiline,
                            // Use email input type for emails.
                            maxLines: 5,
                            minLines: 1,
                            textInputAction: TextInputAction.newline,
                            decoration: new InputDecoration(
                                labelText: "Container Remark :",
                                labelStyle:
                                TextStyle(fontSize: 18, color: Colors.black38),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: AppTheme.colorPrimary, width: 1.0),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: AppTheme.colorMediumGrey, width: 1.0),
                                ),
                                border: new OutlineInputBorder(),
                                disabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: AppTheme.colorMediumGrey, width: 1.0),
                                ),
                                counterText: ""),
                            autofocus: false,
                            readOnly: (!IsBitSet(widget.bit, 19)),
                            onSaved: (value) {
                              _data._containerRemark = value;
                            },
                          ),
                        ),
                      ),

                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    SizedBox(
                      width: 150,
                      height: 50,
                      child: ElevatedButton(
                        child: Text(
                          "Update",
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: _isCheck == null ||
                                !_isCheck ||
                                (!IsBitSet(widget.bit, 20) &&
                                    !IsBitSet(widget.bit, 19))
                            ? null
                            : () {
                                if (_isCheck != null && _isCheck) {
                                  if (containerDetailModel != null)
                                    _updateContainerStatus();
                                } else {
                                  setState(() {
                                    _isCheck = false;
                                    _scrollController.animateTo(0,
                                        duration: Duration(milliseconds: 300),
                                        curve: Curves.ease);
                                    focusNode.requestFocus();
                                    Fluttertoast.showToast(
                                        msg: "Invalid Container No.",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.CENTER,
                                        timeInSecForIosWeb: 1,
                                        backgroundColor: Colors.red,
                                        textColor: Colors.white,
                                        fontSize: 16.0);
                                  });
                                }
                              },
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    Checkbox(
                        value: blocked,
                        onChanged: (value) {
                          if (!IsBitSet(widget.bit, 17)) {
                            Fluttertoast.showToast(
                                msg:
                                    "Don't have permission to Block/Unblock. Please contact your admin");
                            FocusScope.of(context).unfocus();
                          } else {
                            setState(() {
                              blocked = value;
                              if (blocked == true) {
                                tecReason.text = "";
                              }
                            });
                          }
                        }),
                    _isBlock
                        ? Row(
                            children: [
                              Text("Unblock"),
                              SizedBox(
                                width: 5,
                              ),
                              Text(
                                "(Container blocked!)",
                                style: TextStyle(color: Colors.red),
                              ),
                            ],
                          )
                        : Text("Block")
                  ],
                ),
                if (!blocked)
                  Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: TextFormField(
                      controller: tecReasonDisplay,
                      keyboardType: TextInputType.multiline,
                      textCapitalization: TextCapitalization.characters,
                      // inputFormatters: [
                      //   UpperCaseTextFormatter(),
                      // ],
                      maxLines: 5,
                      minLines: 1,
                      textInputAction: TextInputAction.newline,
                      decoration: new InputDecoration(
                          labelText: blocked ? "Reason(Old) :" : "Reason:",
                          labelStyle:
                              TextStyle(fontSize: 18, color: Colors.black38),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorPrimary, width: 1.0),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorMediumGrey, width: 1.0),
                          ),
                          border: new OutlineInputBorder(),
                          disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorMediumGrey, width: 1.0),
                          ),
                          counterText: ""),
                      autofocus: false,
                      readOnly: true,
                      enabled: false,
                      validator: (value) {
                        if (value.isEmpty) {
                          return "Please provide reason to block/unblock.";
                        }
                        return null;
                      },
                      onSaved: (value) {
                        this._data._containerReason = value;
                      },
                    ),
                  ),
                if (blocked)
                  Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: Form(
                      key: _keyBlock,
                      child: Focus(
                        onFocusChange: (hasFocus) {
                          if (!IsBitSet(widget.bit, 17)) {
                            Fluttertoast.showToast(
                                msg:
                                    "Don't have permission to Block/Unblock. Please contact your admin");
                            FocusScope.of(context).unfocus();
                          }
                        },
                        child: TextFormField(
                          controller: tecReason,
                          keyboardType: TextInputType.multiline,
                          textCapitalization: TextCapitalization.characters,
                          // inputFormatters: [
                          //   UpperCaseTextFormatter(),
                          // ],
                          maxLines: 5,
                          minLines: 1,
                          textInputAction: TextInputAction.newline,
                          decoration: new InputDecoration(
                              labelText: "Reason :",
                              labelStyle: TextStyle(
                                  fontSize: 18, color: Colors.black38),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: AppTheme.colorPrimary, width: 1.0),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: AppTheme.colorMediumGrey,
                                    width: 1.0),
                              ),
                              border: new OutlineInputBorder(),
                              disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: AppTheme.colorMediumGrey,
                                    width: 1.0),
                              ),
                              counterText: ""),
                          autofocus: false,
                          enabled: blocked ? true : false,
                          validator: (value) {
                            if (value.isEmpty) {
                              return "Please provide reason to block/unblock.";
                            }
                            return null;
                          },
                          onSaved: (value) {
                            this._data._containerReason = value;
                          },
                        ),
                      ),
                    ),
                  ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    SizedBox(
                        width: 150,
                        height: 50,
                        child: ElevatedButton(
                          child: Text(
                            _isBlock ? "Unblock" : "Block",
                            style: TextStyle(color: Colors.white),
                          ),
                          onPressed: _isCheck == null ||
                                  !_isCheck ||
                                  !blocked ||
                                  (!IsBitSet(widget.bit, 17))
                              ? null
                              : () {
                                  if (!IsBitSet(widget.bit, 17)) {
                                    Fluttertoast.showToast(
                                        msg:
                                            "Don't have permission to Block/Unblock. Please contact your admin");
                                  } else {
                                    if (_isCheck != null && _isCheck) {
                                      _updateBlockReason();
                                    } else {
                                      setState(() {
                                        _isCheck = false;
                                        _scrollController.animateTo(0,
                                            duration:
                                                Duration(milliseconds: 300),
                                            curve: Curves.ease);
                                        focusNode.requestFocus();
                                        Fluttertoast.showToast(
                                            msg: "Invalid Container No.",
                                            toastLength: Toast.LENGTH_SHORT,
                                            gravity: ToastGravity.CENTER,
                                            timeInSecForIosWeb: 1,
                                            backgroundColor: Colors.red,
                                            textColor: Colors.white,
                                            fontSize: 16.0);
                                      });
                                    }
                                  }
                                },
                        )),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Form(
                  key: _keyYard,
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: TextFormField(
                      controller: tecYard,
                      keyboardType: TextInputType.text,
                      textCapitalization: TextCapitalization.characters,
                      // inputFormatters: [
                      //   UpperCaseTextFormatter(),
                      // ],
                      onTap: () {
                        if (_isCheck != null && _isCheck)
                          popDialog("Yard Zone Options", listYard);
                      },
                      validator: (value) {
                        if (value.length <= 0) {
                          return "Please select Yard Zone";
                        }
                        return null;
                      },
                      decoration: new InputDecoration(
                          suffixIcon: Icon(Icons.arrow_drop_down),
                          labelText: "Yard Zone :",
                          labelStyle:
                              TextStyle(fontSize: 18, color: Colors.black38),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorPrimary, width: 1.0),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorMediumGrey, width: 1.0),
                          ),
                          border: new OutlineInputBorder(),
                          disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorMediumGrey, width: 1.0),
                          ),
                          counterText: ""),
                      readOnly: true,
                      autofocus: false,
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    SizedBox(
                      width: 150,
                      height: 50,
                      child: ElevatedButton(
                        child: Text(
                          "Update",
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: _isCheck == null || !_isCheck
                            ? null
                            : () {
                                if (_isCheck != null && _isCheck) {
                                  // _submitGateIn();
                                  _updateYardZone();
                                } else {
                                  setState(() {
                                    _isCheck = false;
                                    _scrollController.animateTo(0,
                                        duration: Duration(milliseconds: 300),
                                        curve: Curves.ease);
                                    focusNode.requestFocus();
                                    Fluttertoast.showToast(
                                        msg: "Invalid Container No.",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.CENTER,
                                        timeInSecForIosWeb: 1,
                                        backgroundColor: Colors.red,
                                        textColor: Colors.white,
                                        fontSize: 16.0);
                                  });
                                }
                              },
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
      ),
    );
  }

//  startActivityWithResult(
//      String containerNo, ContainerModel containerModel) async {
//    var result = await Navigator.push(
//        context,
//        MaterialPageRoute(
//            builder: (_) => DepotOutCondition(
//                  containerModel: containerModel,
//                )));
//
//    print("here $result");
//    if (result == "success") {
//      _submitGateInSearch();
//    } else {
//      FocusScope.of(context).unfocus();
//    }
//  }

  Future<String> _scanQR() async {
    try {
      var qrResult = await BarcodeScanner.scan();
      print("Scanned");
      result = qrResult.rawContent;
      print(result);

      txtSearchController.text = result;
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.cameraAccessDenied) {
        setState(() {
          result = "Camera permission was denied";
          Fluttertoast.showToast(
              msg: result,
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0);
        });
      } else {
        setState(() {
          result == "Unknown Error $e";
          //Toast.show(result, context, duration: 2);
        });
      }
    } on FormatException {
      setState(() {
        result = "Cancelled";
        Fluttertoast.showToast(
            msg: result,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      });
    } catch (e) {
      setState(() {
        result == "Unknown Error $e";
        //Toast.show(result, context, duration: 2);
      });
    }
  }

  void _getContainerDetail() async {
    // Save our form now.
    FocusScope.of(context).unfocus();
    if (_keyContainer.currentState.validate()) {
      _keyContainer.currentState.save();

      Map dataValue = {"Key": "ContainerNumber", "Value": _data._containerNo};

      List list = [];
      list.add(json.encode(dataValue));

      Map data = {'Key': 'Data', 'Value': "$list"};

      final response = await _httpHelper.post(
          baseUrl, UrlConfig.GET_CONTAINER_DETAILS, json.encode(data));
      Response res = Response.fromJson(response);
      print("PKM" + res.statusCode.toString());
      pr.hide();
      if (res.statusCode == 200) {
        if (res.results.length > 0) {
          print("RESULT -- " + res.results[0].value);
          var tempResult = json.decode(res.results[0].value);
          _isCheck = true;
          if (tempResult['Result'] == null) {
            tecContainerDetail.text = "Container not found!";
            setState(() {
              _isCheck = false;
              containerDetailModel = null;
            });
          } else {
            print(tempResult['Result']);
            List tempList = json.decode(tempResult['Result']);
            Map tempMap = tempList[0];
            containerDetailModel = ContainerDetailModel.fromJson(tempMap);
            String tempString = "";

            for (int i = 2; i < tempMap.keys.toList().length; i++) {
              var recase = ReCase(tempMap.keys.toList()[i]);
              String disKey = recase.titleCase;

              if (tempMap.keys.toList()[i].contains("ISO")) {
                disKey = "ISO Code";
              }

              String disValue = tempMap.values.toList()[i];

              if (i == 2) {
                tempString = disKey + " : " + disValue;
              } else if (tempMap.keys.toList()[i] == "GateInDate") {
                tempString = tempString +
                    "\n" +
                    disKey +
                    " : " +
                    dobFormat.format(DateTime.parse(disValue));
              } else if(
              tempMap.keys.toList()[i] == "ManufactureDate"){
                tempString = tempString +
                    "\n" +
                    disKey +
                    " : " +
                    dobFormat2.format(DateTime.parse(disValue));
              }else if (tempMap.keys.toList()[i] != "IsBlock" &&
                  tempMap.keys.toList()[i] != "BlockReason" &&
                  tempMap.keys.toList()[i] != "ContainerRemarks" &&
                  tempMap.keys.toList()[i] != "ContainerStatus" &&
                  tempMap.keys.toList()[i] != "ContainerNo" &&
                  tempMap.keys.toList()[i] != "YardZone") {
                tempString = tempString + "\n" + disKey + " : " + disValue;
              }
              // switch (i) {
              //   case 2:
              //     tempString = "Type Size : " + tempMap.values.toList()[i];
              //     break;
              //   case 3:
              //     tempString = tempString +
              //         "\n" +
              //         "ISO Code : " +
              //         tempMap.values.toList()[i];
              //     break;
              //   case 4:
              //     tempString = tempString +
              //         "\n" +
              //         "Container Grade : " +
              //         tempMap.values.toList()[i];
              //     break;
              //   case 5:
              //     tempString =
              //         tempString + "\n" + "MGW : " + tempMap.values.toList()[i];
              //     break;
              //   case 6:
              //     tempString = tempString +
              //         "\n" +
              //         "Manufacturer Date : " +
              //         dobFormat
              //             .format(DateTime.parse(tempMap.values.toList()[i]));
              //     break;
              //   case 7:
              //     tempString = tempString +
              //         "\n" +
              //         "Gate In Date : " +
              //         dobFormat
              //             .format(DateTime.parse(tempMap.values.toList()[i]));
              //     break;
              //   case 8:
              //     tempString = tempString +
              //         "\n" +
              //         "Aging : " +
              //         tempMap.values.toList()[i];
              //     break;
              //   case 9:
              //     tempString = tempString +
              //         "\n" +
              //         "Operator Code : " +
              //         tempMap.values.toList()[i];
              //     break;
              //
              //   case 12:
              //     tempString = tempString +
              //         "\n" +
              //         "Gate In Doc No : " +
              //         tempMap.values.toList()[i];
              //     break;
              //   case 13:
              //     tempString = tempString +
              //         "\n" +
              //         "Nominated Booking No : " +
              //         tempMap.values.toList()[i];
              //     break;
              //   case 14:
              //     tempString = tempString +
              //         "\n" +
              //         "Nominated Booking Ref : " +
              //         tempMap.values.toList()[i];
              //     break;
              //   default:
              //     tempString = tempString;
              //     break;
              // }
            }
            tecContainerDetail.text = tempString;

            setState(() {
              tecRemark.text = containerDetailModel.containerRemarks;
              tecYard.text = containerDetailModel.yardZone;
              _data._containerYard = containerDetailModel.yardZone;
              tecReasonDisplay.text = containerDetailModel.blockReason;
              for (int i = 0; i < crContainerTemp.length; i++) {
                if (containerDetailModel.containerStatus ==
                    crContainerTemp[i]["Key"]) {
                  tecContainerStatus.text = crContainerTemp[i]['Value'];
                  _data._containerStatus = crContainerTemp[i]['Key'];
                }
              }

              if (containerDetailModel.isBlock == "0") {
                _isBlock = false;
              } else {
                _isBlock = true;
              }

              blocked = false;
            });
          }
        }
      } else if (res.statusCode == 400) {
        FocusScope.of(context).unfocus();
        showDialog<void>(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text(res.errorDetail),
                content: SingleChildScrollView(
                  child: ListBody(
                    children: <Widget>[
                      Text(
                        res.errorMessage,
                        style: TextStyle(),
                      ),
                    ],
                  ),
                ),
                actions: <Widget>[
                  FlatButton(
                    child: const Text('Ok'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              );
            });
        setState(() {
          _isCheck = false;
          blocked = false;
        });
      } else {
        Fluttertoast.showToast(
            msg: res.errorMessage,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
        setState(() {
          listContainer = [];
          FocusScope.of(context).unfocus();
        });

        if (res.errorMessage.contains("AccessToken"))
          new force_logout(context).doForceLogout();
      }
    } else {
      _isCheck = false;
    }
  }

  void _updateContainerStatus() async {
    // Save our form now.

    FocusScope.of(context).unfocus();
    if (_keyStatus.currentState.validate()) {
      _keyStatus.currentState.save();
      pr.show();

      Map dataValue = {
        "GateInDocNo": containerDetailModel.gateInNo,
        "ContainerNo": containerDetailModel.containerNo,
        "ContainerStatus": _data._containerStatus,
        "ContainerRemarks": _data._containerRemark,
      };

      List list = [];
      list.add(json.encode(dataValue));

      Map data = {'Key': 'Data', 'Value': json.encode(dataValue)};

      final response = await _httpHelper.post(
          baseUrl, UrlConfig.UPDATE_GATEIN_STATUS, json.encode(data));
      Response res = Response.fromJson(response);
      print("PKM" + res.statusCode.toString());
//      pr.hide();
      if (res.statusCode == 200) {
        if (res.results.length > 0) {
          print("RESULT -- " + res.results[0].value);
          var tempResult = json.decode(res.results[0].value);
          _getContainerDetail();
          Fluttertoast.showToast(
              msg: "Success Update Status",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              textColor: Colors.white,
              fontSize: 16.0);
        }
      } else if (res.statusCode == 400) {
        FocusScope.of(context).unfocus();
        showDialog<void>(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text(res.errorDetail),
                content: SingleChildScrollView(
                  child: ListBody(
                    children: <Widget>[
                      Text(
                        res.errorMessage,
                        style: TextStyle(),
                      ),
                    ],
                  ),
                ),
                actions: <Widget>[
                  FlatButton(
                    child: const Text('Ok'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              );
            });
      } else {
        Fluttertoast.showToast(
            msg: res.errorMessage,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
        setState(() {
          FocusScope.of(context).unfocus();
        });

        if (res.errorMessage.contains("AccessToken"))
          new force_logout(context).doForceLogout();
      }
    }
  }

  void _updateBlockReason() async {
    // Save our form now.

    FocusScope.of(context).unfocus();
    if (_keyBlock.currentState.validate()) {
      _keyBlock.currentState.save();

      pr.show();

      Map dataValue = {
        "GateInDocNo": containerDetailModel.gateInNo,
        "ContainerNo": containerDetailModel.containerNo,
        "IsBlock": _isBlock ? 0 : 1,
        "BlockReason": _data._containerReason,
      };

      List list = [];
      list.add(json.encode(dataValue));

      Map data = {'Key': 'Data', 'Value': json.encode(dataValue)};

      final response = await _httpHelper.post(
          baseUrl, UrlConfig.UPDATE_GATEIN_BLOCK, json.encode(data));
      Response res = Response.fromJson(response);
      print("PKM" + res.statusCode.toString());
//      pr.hide();
      if (res.statusCode == 200) {
        if (res.results.length > 0) {
          print("RESULT -- " + res.results[0].value);
          var tempResult = json.decode(res.results[0].value);
          _getContainerDetail();
          Fluttertoast.showToast(
              msg: _isBlock ? "Success Unblocked" : "Success Blocked",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              textColor: Colors.white,
              fontSize: 16.0);
        }
      } else if (res.statusCode == 400) {
        FocusScope.of(context).unfocus();
        showDialog<void>(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text(res.errorDetail),
                content: SingleChildScrollView(
                  child: ListBody(
                    children: <Widget>[
                      Text(
                        res.errorMessage,
                        style: TextStyle(),
                      ),
                    ],
                  ),
                ),
                actions: <Widget>[
                  FlatButton(
                    child: const Text('Ok'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              );
            });
      } else {
        Fluttertoast.showToast(
            msg: res.errorMessage,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
        setState(() {
          FocusScope.of(context).unfocus();
        });

        if (res.errorMessage.contains("AccessToken"))
          new force_logout(context).doForceLogout();
      }
    }
  }

  void _updateYardZone() async {
    // Save our form now.
    FocusScope.of(context).unfocus();
    if (_keyYard.currentState.validate()) {
      _keyYard.currentState.save();
      pr.show();

      Map dataValue = {
        "GateInDocNo": containerDetailModel.gateInNo,
        "ContainerNo": containerDetailModel.containerNo,
        "YardZone": _data._containerYard,
      };

      List list = [];
      list.add(json.encode(dataValue));

      Map data = {'Key': 'Data', 'Value': json.encode(dataValue)};

      final response = await _httpHelper.post(
          baseUrl, UrlConfig.UPDATE_YARD_ZONE, json.encode(data));
      Response res = Response.fromJson(response);
      print("PKM" + res.statusCode.toString());
//      pr.hide();
      if (res.statusCode == 200) {
        if (res.results.length > 0) {
          print("RESULT -- " + res.results[0].value);
          var tempResult = json.decode(res.results[0].value);
          _getContainerDetail();
          Fluttertoast.showToast(
              msg: "Success Update Yard Zone",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              textColor: Colors.white,
              fontSize: 16.0);
        }
      } else if (res.statusCode == 400) {
        FocusScope.of(context).unfocus();
        showDialog<void>(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text(res.errorDetail),
                content: SingleChildScrollView(
                  child: ListBody(
                    children: <Widget>[
                      Text(
                        res.errorMessage,
                        style: TextStyle(),
                      ),
                    ],
                  ),
                ),
                actions: <Widget>[
                  FlatButton(
                    child: const Text('Ok'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              );
            });
      } else {
        Fluttertoast.showToast(
            msg: res.errorMessage,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
        setState(() {
          FocusScope.of(context).unfocus();
        });

        if (res.errorMessage.contains("AccessToken"))
          new force_logout(context).doForceLogout();
      }
    }
  }

  reset() {
    FocusScope.of(context).unfocus();
    _scrollController.animateTo(0,
        duration: Duration(milliseconds: 500), curve: Curves.ease);
    setState(() {
      _data = new _ContainerData();
      containerDetailModel = null;
      _isCheck = null;
      _checkContainer = "";
      tecContainerNo.text = "";
      tecYard.text = "";
      tecReason.text = '';
      tecReasonDisplay.text = '';
      tecRemark.text = "";
      tecContainerDetail.text = "";
      tecContainerStatus.text = "";
      gateInDoc = "";
    });
  }

/*
  void _showModal(context) {
    showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(15.0)),
        ),
        context: context,
        builder: (context) {
          //3
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
                return DraggableScrollableSheet(
                    expand: false,
                    builder:
                        (BuildContext context, ScrollController scrollController) {
                      return Column(children: <Widget>[
                        Padding(
                            padding: EdgeInsets.all(8),
                            child: Row(children: <Widget>[
                              Expanded(
                                  child: TextField(
                                      controller: textController,
                                      decoration: InputDecoration(
                                        contentPadding: EdgeInsets.all(8),
                                        border: new OutlineInputBorder(
                                          borderRadius:
                                          new BorderRadius.circular(15.0),
                                          borderSide: new BorderSide(),
                                        ),
                                        prefixIcon: Icon(Icons.search),
                                      ),
                                      onChanged: (value) {
                                        //4
                                        setState(() {
                                          tempListHaulier = _buildSearchList(value);
                                        });
                                      })),
                            ])),
                        Expanded(
                          child: ListView.separated(
                              controller: scrollController,
                              //5
                              itemCount: (tempListHaulier != null &&
                                  tempListHaulier.length > 0)
                                  ? tempListHaulier.length
                                  : listHaulier.length,
                              separatorBuilder: (context, int) {
                                return Divider();
                              },
                              itemBuilder: (context, index) {
                                return InkWell(
                                    child: (tempListHaulier != null &&
                                        tempListHaulier.length > 0)
                                        ? _showBottomSheetWithSearch(
                                        index, tempListHaulier)
                                        : _showBottomSheetWithSearch(
                                        index, listHaulier),
                                    onTap: () {
                                      if (tempListHaulier.length > 0) {
                                        _data._haulier =
                                        tempListHaulier[index]['Key'];
                                        print(
                                            "id =  ${tempListHaulier[index]['Key']}");
                                      } else {
                                        _data._haulier = listHaulier[index]['Key'];
                                        print("id =  ${listHaulier[index]['Key']}");
                                      }
                                      Navigator.of(context).pop();
                                    });
                              }),
                        )
                      ]);
                    });
              });
        });
  }

  Widget _showBottomSheetWithSearch(int index, List listAM) {
    return Text(listAM[index]["Value"],
        style: TextStyle(color: Colors.black, fontSize: 16),
        textAlign: TextAlign.center);
  }

  List _buildSearchList(String userSearchTerm) {
    List _searchList = [];

    for (int i = 0; i < listHaulier.length; i++) {
      String name = listHaulier[i]["Value"];
      if (name.toLowerCase().contains(userSearchTerm.toLowerCase())) {
        _searchList.add(listHaulier[i]);
      }
    }
    return _searchList;
  }*/
}
