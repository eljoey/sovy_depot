import 'dart:convert';

import 'dart:io';
import 'package:barcode_scan2/barcode_scan2.dart';
import 'package:depot/models/response.dart';
import 'package:flutter/services.dart';
import 'package:depot/utils/AppPrefs.dart';
import 'package:depot/utils/ShareMethod.dart';
import 'package:depot/utils/progress_dialog.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'company_selection.dart';
import 'home_menu.dart';
import 'utils/route_generator.dart';
import 'utils/UrlConfig.dart';
import 'utils/ApiBaseHelper.dart';
import 'package:device_info/device_info.dart';
import 'styles/appTheme.dart';
import 'dart:convert';

void main() {
  WidgetsFlutterBinding.ensureInitialized(); //jeff
  SystemChrome.setPreferredOrientations(
          [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown])
      .then((_) => runApp(
            new MyApp(),
          ));
  // runApp(new MyApp());
//  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
//    //systemNavigationBarColor: Colors.blue, // navigation bar color
//    statusBarColor: Colors.black12, // status bar color
//  ));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Sovy Depot',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: createMaterialColor(Color(0xFF8BC73F)),
        cursorColor: createMaterialColor(Color(0xFF8BC73F)),
      ),
      home: MyHomePage(title: 'Sovy Depot'),
      onGenerateRoute: RouteGenerator.generateRoute,
    );
  }
}

void _portraitModeOnly() {
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _LoginData {
  String username = '';
  String password = '';
  String forgot_mail = '';
}

class _MyHomePageState extends State<MyHomePage> {
  ApiBaseHelper _httpHelper = ApiBaseHelper();
  int _counter = 0;
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final GlobalKey<FormState> _formKey_frgtpass = new GlobalKey<FormState>();
  _LoginData _data = new _LoginData();
  ProgressDialog pr;
  bool isActivated;
  String result;
  String errorMsg = "";
  BuildContext bcontext;
  TextEditingController txtActivate = new TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    this.checkActivated();

    super.initState();
  }

  Future checkActivated() async {
    bool _seen = (await AppPrefs.getIsActivated() ?? false);

    if (!_seen) {
      this._activate_dlg();
    }
  }

  Future checkActivateLogin() async {
    bool _seen = (await AppPrefs.getIsActivated() ?? false);

    if (!_seen) {
      this._activate_dlg();
    }
  }

  String _validatePassword(String value) {
    if (value.length < 6) {
      return 'The Password must be at least 6 characters.';
    }

    return null;
  }

  String _validateUsername(String value) {
    if (value.length < 1) {
      return 'The Login Id can not be empty.';
    }

    return null;
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData media = MediaQuery.of(context);

    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);

    final Size screenSize = media.size;

    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/home_page.png"),
                    fit: BoxFit.cover)),
            padding: new EdgeInsets.all(20.0),
            child: new Form(
              key: this._formKey,
              child: new Column(
                children: <Widget>[
                  Expanded(child: SizedBox()),
                  new Container(
                    child: Column(children: <Widget>[
                      Container(
                          decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.3),
                                spreadRadius: 0.2,
                                blurRadius: 5,
                                offset:
                                    Offset(0, 1), // changes position of shadow
                              ),
                            ],
                            color: Colors.transparent,
                          ),
                          height: 110,
                          child: Image.asset(
                            "assets/images/ic_launcher_temp_roundedCorner.png",
                          )),
                      Padding(
                        padding: const EdgeInsets.only(top: 4.0, bottom: 16),
                        child: Text(
                          "SOVY DEPOT",
                          style: TextStyle(
                              fontSize: 30,
                              color: AppTheme.colorSecondary,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      new Container(
                        child: Card(
                          elevation: 3.0,
                          // this field changes the shadow of the card 1.0 is default
                          child: new TextFormField(
                              keyboardType: TextInputType.text,
                              // inputFormatters: [
                              //   UpperCaseTextFormatter(),
                              // ],
                              textCapitalization: TextCapitalization.characters,
                              decoration: new InputDecoration(
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: AppTheme.colorPrimary,
                                        width: 1.0),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: AppTheme.colorSecondary,
                                        width: 1.0),
                                  ),
                                  labelText: 'Login ID',
                                  border: new OutlineInputBorder(),
                                  prefixIcon: new Icon(Icons.person_outline)),
                              validator: this._validateUsername,
                              onSaved: (String value) {
                                this._data.username = value;
                              }),
                        ),
                      ),
                      new Container(
                        child: Card(
                          elevation: 3.0,
                          // this field changes the shadow of the card 1.0 is default
                          child: new TextFormField(
                            obscureText: true,
                            // Use secure text for passwords.
                            decoration: new InputDecoration(
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: AppTheme.colorPrimary, width: 1.0),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: AppTheme.colorSecondary,
                                      width: 1.0),
                                ),
                                labelText: 'Enter your password',
                                border: new OutlineInputBorder(),
                                prefixIcon: new Icon(Icons.lock_outline)),
                            validator: this._validatePassword,
                            onSaved: (String value) {
                              this._data.password = value;
                            },
                            onFieldSubmitted: (String value) {
                              this._submitEmailLogin();
                            },
                          ),
                        ),
                      ),
                    ]),
                  ),
                  new Container(
                    width: screenSize.width,
                    child: new Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new Container(
                          color: Colors.transparent,
                          height: 50.0,
                          width: 170.0,
                          margin: new EdgeInsets.only(bottom: 10.0, top: 16.0),
                          child: Card(
                            elevation: 5.0,
                            // this field changes the shadow of the card 1.0 is default
                            child: new RaisedButton(
                              padding: const EdgeInsets.all(10.0),
                              child: new Text(
                                'Login',
                                style: new TextStyle(color: Colors.white),
                              ),
                              onPressed: this._submitEmailLogin,
                              color: AppTheme.colorPrimary,
//                              shape: RoundedRectangleBorder(
//                                  borderRadius: new BorderRadius.circular(10.0),
//                                  side: BorderSide(color: Colors.transparent)),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(child: SizedBox()),
                  Expanded(child: SizedBox()),

                  /*new Container(
                          width: screenSize.width,
                          child:new Column(
                            children: <Widget>[
                              new Container(
                                margin: const EdgeInsets.only(left: 10.0,top: 20.0),
                                child:    new Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    new Container(
                                      height:50.0,
                                      width: 210.0,
                                      child: new RaisedButton.icon(
                                        label: new Text(
                                          'Sign-In with Google',
                                          style: new TextStyle(
                                            color: Colors.white,
                                          ),
                                        ),
                                        icon: new Image.asset("assets/google_plus.png",width: 24.0,height: 24.0),
                                        onPressed: () => _googleSignInButton().then(
                                              (FirebaseUser user)=>print(user),
                                        ).catchError((e)=>print(e)),
                                        color: Colors.red,

                                      ),

                                    ),

                                  ],
                                ),
                              ),
                              new Container(
                                margin: const EdgeInsets.only(left: 10.0,top: 20.0),
                                child: new Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    new Container(
                                      height:50.0,
                                      width: 210.0,
                                      child: new RaisedButton.icon(
                                        label: new Text(
                                          'Login with Facebook',
                                          style: new TextStyle(
                                            color: Colors.white,
                                          ),
                                        ),
                                        icon: new Image.asset("assets/facebook.png",width: 24.0,height: 24.0,),
                                        // icon: const Icon(Icons.adjust, size: 28.0,color: Colors.white),

                                        onPressed:this._facebookLogin,
                                        color: Colors.indigo,

                                      ),

                                    ),

                                  ],
                                ),
                              )
                            ],
                          ),
                        )*/
                ],
              ),
            )),
      ),
    );
  }

  void reset() {
    AppPrefs.setIsActivated(false);
  }

  _activate_dlg() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;

    await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) => Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(16),
              ),
              elevation: 0.0,
              backgroundColor: Colors.transparent,
              child: Container(
                height: 300,
                width: double.infinity,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(16)),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: <Widget>[
                      Text(
                        'Alert',
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.w400),
                        textAlign: TextAlign.center,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 8, top: 8.0),
                        child: Text(
                          'Your device is not active, please activate your device with center.',
                          style: TextStyle(fontSize: 14),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Text(
                        'Your DeviceID is : ${androidInfo.androidId}',
                        textAlign: TextAlign.center,
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: <Widget>[
                              new Expanded(
                                  child: new Form(
                                      key: this._formKey_frgtpass,
                                      child: new TextFormField(
                                        controller: txtActivate,
                                        keyboardType: TextInputType.url,
                                        // Use email input type for emails.
                                        decoration: new InputDecoration(
                                            focusedBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: AppTheme.colorPrimary,
                                                  width: 1.0),
                                            ),
                                            enabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color:
                                                      AppTheme.colorSecondary,
                                                  width: 1.0),
                                            ),
                                            suffixIcon: IconButton(
                                              icon:
                                                  Icon(FontAwesomeIcons.qrcode),
                                              onPressed: this._scanQR,
                                            ),
                                            hintText: 'Enter URL',
                                            labelText: 'URL',
                                            border: new OutlineInputBorder()),
                                        validator: this._validateUsername,
                                        onSaved: (value) {
                                          this._data.forgot_mail = value;
                                        },
                                      ))),
                            ],
                          ),
                        ),
                      ),
//                      errorMsg == "" ?SizedBox():Text(errorMsg,style: TextStyle(color: Colors.red),),
                      new Container(
                        color: Colors.transparent,
                        height: 50.0,
                        width: 170.0,
                        margin: new EdgeInsets.only(bottom: 10.0, top: 20.0),
                        child: Card(
                          elevation: 5.0,
                          // this field changes the shadow of the card 1.0 is default
                          child: new RaisedButton(
                            padding: const EdgeInsets.all(12.0),
                            child: new Text(
                              'Activate',
                              style: new TextStyle(color: Colors.white),
                            ),
                            onPressed: () {
                              this._submitForgotPass();
                            },
                            color: AppTheme.colorPrimary,
//                              shape: RoundedRectangleBorder(
//                                  borderRadius: new BorderRadius.circular(10.0),
//                                  side: BorderSide(color: Colors.transparent)),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ));
//    await showDialog<String>(
//        context: context,
//        barrierDismissible: false,
//        builder: (BuildContext context) {
//          return Container(
//            child: Column(children: <Widget>[
//              Text('Forgot Your Password?'),
//              Row(
//                children: <Widget>[
//                  new Expanded(
//                      child: new Form(
//                          key: this._formKey_frgtpass,
//                          child: new TextFormField(
//                            autofocus: true,
//                            keyboardType: TextInputType.emailAddress,
//                            // Use email input type for emails.
//                            decoration: new InputDecoration(
//                                hintText: 'you@example.com',
//                                labelText: 'E-mail Address',
//                                border: new OutlineInputBorder()),
//                            validator: (String value) {
//                              if (!(value.length > 0)) {
//                                return 'E-mail not valid.';
//                              }
//                              return null;
//                            },
//                            onChanged: (value) {
//                              this._data.forgot_mail = value;
//                            },
//                          )))
//                ],
//              ),
//            ],),
//          );
//        });
  }

  Future<String> _scanQR() async {
    try {
      var qrResult = await BarcodeScanner.scan();
      print("Scanned");
      result = qrResult.rawContent;
      print(result);

      txtActivate.text = result;
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.cameraAccessDenied) {
        setState(() {
          result = "Camera permission was denied";
          Fluttertoast.showToast(
              msg: result,
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0
          );
        });
      } else {
        setState(() {
          result == "Unknown Error $e";
          //Toast.show(result, context, duration: 2);
        });
      }
    } on FormatException {
      setState(() {
        result = "Cancelled";
        Fluttertoast.showToast(
            msg: result,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );      });
    } catch (e) {
      setState(() {
        result == "Unknown Error $e";
        //Toast.show(result, context, duration: 2);
      });
    }
  }

  void _submitEmailLogin() async {
    FocusScope.of(context).unfocus();

    bool _seen = (await AppPrefs.getIsActivated() ?? false);

    if (!_seen) {
      this._activate_dlg();
    } else {
      DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      String id = androidInfo.androidId;

      if (this._formKey.currentState.validate()) {
        _formKey.currentState.save(); // Save our form now.
        pr.show();

        print('Printing the login data.');
        print('Username: ${_data.username}');
        print('Password: ${_data.password}');

        String tempUser = base64.encode(utf8.encode(_data.username));
        String tempPw = base64.encode(utf8.encode(_data.password));

        String baseUrl = await AppPrefs.getAppServer();
        String url = "GetAccessTokens/" + id + "/" + tempUser + "/" + tempPw;

        final response = await _httpHelper.get(baseUrl, url);
        pr.hide();
        Response res = Response.fromJson(response);
        if (res.statusCode == 200) {
          List<Results> tempResults;
          tempResults = [];
          List<dynamic> tempList = json.decode(res.results[0].value);

          for (int i = 0; i < tempList.length; i++) {
            Results tempRes = Results.fromJson(tempList[i]);
            tempResults.add(tempRes);
          }

          AppPrefs.setAccessToken(tempResults[0].value);
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (_) => Company_Selection(
                        listAccess: tempResults,
                      )));
        } else {
          Fluttertoast.showToast(
              msg: res.errorMessage,
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0
          );
        }
      }
    }
  }

  void _submitForgotPass() async {
    if (this._formKey_frgtpass.currentState.validate()) {
      pr.show();
      _formKey_frgtpass.currentState.save();
      String id;
      Map data;

      if (Platform.isIOS) {
        DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
        IosDeviceInfo iosInfo = await deviceInfo.iosInfo;

        data = {
          'DeviceId': iosInfo.identifierForVendor,
          'SystemInfo': iosInfo.systemName,
          'DeviceName': iosInfo.name + " " + iosInfo.model
        };
      } else {
        DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
        AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;

        data = {
          'DeviceId': androidInfo.androidId,
          'SystemInfo': androidInfo.id,
          'DeviceName': androidInfo.brand + " " + androidInfo.device
        };
      }

      final response = await _httpHelper.activate(
          _data.forgot_mail, UrlConfig.APP_ACTIVATE_ROUTE, json.encode(data));

      if (pr.isShowing()) {
        pr.hide();
      }
      Response res = Response.fromJson(response);
      print("PKM" + res.statusCode.toString());
      if (res.statusCode == 200) {
        if (res.results[0].value == "true") {
          await AppPrefs.setAppServer(_data.forgot_mail);
          await AppPrefs.setIsActivated(true);
          Navigator.pop(context);
        }
      } else {
//        Navigator.pop(context);
        Fluttertoast.showToast(
            msg: res.errorMessage,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
      }
    }
  }
}
