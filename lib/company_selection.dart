import 'dart:convert';
import 'dart:developer';

import 'package:depot/styles/appTheme.dart';
import 'package:depot/utils/ApiBaseHelper.dart';
import 'package:depot/utils/AppPrefs.dart';
import 'package:depot/utils/UrlConfig.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'home_menu.dart';
import 'models/response.dart';

class Company_Selection extends StatefulWidget {
  final List<Results> listAccess;

  Company_Selection({Key key, @required this.listAccess})
      : super(key: key);

  @override
  _Company_SelectionState createState() => _Company_SelectionState();
}

class _Company_SelectionState extends State<Company_Selection> {
  String baseUrl;
  ApiBaseHelper _httpHelper = ApiBaseHelper();

  getPrefData() async {
    baseUrl = await AppPrefs.getAppServer();
    if(widget.listAccess.length == 1){
      AppPrefs.setAccessToken(widget.listAccess[0].value);
      _submitCondition(widget.listAccess[0].key);
    }
  }

  void _submitCondition(String name) async {
    FocusScope.of(context).unfocus();
//      pr.show();
    Map dataKey = {
      "Key": "Data",
      "Value":"Null"
    };

    List list = [];
    list.add(json.encode(dataKey));

    Map data = {'Key': 'Data',
      'Value': "null"};

    final response =
    await _httpHelper.post(
        baseUrl, UrlConfig.APP_DOWNLOAD_LOOKUP, json.encode(data));
    print("PKM" + response.toString() + baseUrl);

    Response res = Response.fromJson(response);
    print("PKM" + res.statusCode.toString());
    if (res.statusCode == 200) {
      log("LOOKUP" + json.encode(res.results));
      AppPrefs.setLookup(json.encode(res.results[0].value));
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (_) => HomeMenu(
                name: name,
              )));
    } else {
      Fluttertoast.showToast(
          msg: res.errorMessage,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );
    }
  }

  @override
  void initState() {
    // TODO: implement initState

    getPrefData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        new Container(
          height: double.infinity,
          width: double.infinity,
          color: Colors.white,
//          decoration: new BoxDecoration(
//            image: new DecorationImage(
//              image: new AssetImage("assets/images/login_bg.png"),
//              fit: BoxFit.cover,
//            ),
//          ),
        ),
//        new Container(
//          height: double.infinity,
//          width: double.infinity,
//          decoration: new BoxDecoration(color: Colors.black38
//          ),
//        ),
        Scaffold(
            appBar: new AppBar(
              leading: Padding(
                padding: const EdgeInsets.only(top:8.0,left:16,right:0,bottom: 8),
                child: AspectRatio(aspectRatio:1,child: Container(decoration:BoxDecoration(borderRadius: BorderRadius.circular(50),color: Colors.white),child: Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: GestureDetector(onTap:(){
                  },child: Image.asset("assets/images/owl_transporter.png")),
                ))),
              ),
              title: new Text('SOVY Depot'),
              backgroundColor: AppTheme.colorPrimary,
            ),
            backgroundColor: Colors.transparent,
            body: Column(children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 20.0),
              child: Text("Select Company", style: TextStyle(fontSize: 20,fontWeight: FontWeight.w500, color: Colors.black),),
            ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(top:16.0,bottom: 16),
              child: ListView.builder(
                  itemCount: widget.listAccess.length,
                  itemBuilder: (BuildContext context, int index) {
                    return CompanyListView(
                      callback: () {
                        _submitCondition(widget.listAccess[0].key);
                      },
                      post: widget.listAccess[index],
                    );
                  }),
            ),
          ),])
        ),
      ],
    );
  }
}

class CompanyListView extends StatelessWidget {
  final VoidCallback callback;
  final Results post;

  const CompanyListView({Key key, this.post, this.callback})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Padding(
          padding: const EdgeInsets.only(
              left: 24, right: 24, top: 8, bottom: 8),
          child: InkWell(
            splashColor: Colors.transparent,
            onTap: () {
              callback();
            },
            child: Card(
              elevation: 3.0,
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(12.0)),
                child: Container(
                  height: 50,
                  decoration: BoxDecoration(color: Colors.white,borderRadius: BorderRadius.circular(4)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                          child: Row(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(left:15.0, right: 15,top: 2, bottom: 2),
                                child: Icon(FontAwesomeIcons.warehouse,size: 25,),
                              ),
                              Expanded(child: Padding(
                                padding: const EdgeInsets.only(left:5.0, top: 2, bottom: 2,right:5 ),
                                child: Text(post.key, style: AppTheme.body2),
                              )),
                            ],
                          )),
                    ],
                  ),

                )
              ),
            ),
          ),
        )
    );
  }
}
