import 'dart:convert';
import 'dart:developer';
import 'dart:io';

//import 'package:depot/jobTracking/JobTaskListing.dart';
import 'package:barcode_scan2/barcode_scan2.dart';
import 'package:depot/eor/eorDetails.dart';
import 'package:depot/eor/eorListing.dart';
import 'package:depot/models/eorModel.dart';
import 'package:depot/models/response.dart';

//import 'package:depot/models/taskDetailModel.dart';
import 'package:depot/styles/appTheme.dart';
import 'package:depot/utils/ApiBaseHelper.dart';
import 'package:depot/utils/AppPrefs.dart';
import 'package:depot/utils/ShareMethod.dart';
import 'package:depot/utils/UrlConfig.dart';
import 'package:depot/utils/force_logout.dart';
import 'package:depot/utils/progress_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';

import 'dart:math' as math;

class EORList extends StatefulWidget {
  @override
  _EORListState createState() => _EORListState();
}

class _EORListState extends State<EORList> {
  ProgressDialog pr;
  String baseUrl;
  List<Header> listPending, listComplete, listProgress;
  List<TaskList> listPendingTask, listCompleteTask, listProgressTask;
  ApiBaseHelper _httpHelper = ApiBaseHelper();
  List<dynamic> tempComp, tempPend, tempProg;
  bool isLoading = true;
  List<Header> listHead;
  List<TaskList> listTask;
  List<Widget> listWidget;
  bool _isSurveyor = false;
  String result;
  TextEditingController txtSearchController = new TextEditingController();

  // google_mlkit.TextDetector textDetector =
  //     google_mlkit.GoogleMlKit.instance.textDetector();

  @override
  void initState() {
    // TODO: implement initState
//    pr = new ProgressDialog(context,
//        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);

    listPending = List();
    listProgress = List();
    listComplete = List();
    super.initState();

    _submitCondition("");
  }

  Future<String> _scanQR() async {
    try {
      var qrResult = await BarcodeScanner.scan();
      print("Scanned");
      result = qrResult.rawContent;
      print(result);

      txtSearchController.text = result;
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.cameraAccessDenied) {
        setState(() {
          result = "Camera permission was denied";
          Fluttertoast.showToast(
              msg: result,
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0);
        });
      } else {
        setState(() {
          result == "Unknown Error $e";
          //Toast.show(result, context, duration: 2);
        });
      }
    } on FormatException {
      setState(() {
        result = "Cancelled";
        Fluttertoast.showToast(
            msg: result,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      });
    } catch (e) {
      setState(() {
        result == "Unknown Error $e";
        //Toast.show(result, context, duration: 2);
      });
    }
  }

  void _submitCondition(String eorNo) async {
    setState(() {
      isLoading = true;
    });
    baseUrl = await AppPrefs.getAppServer();

    Map dataKey = {
      "Key": "ContainerNumber",
      "Value": eorNo,
    };

    List list = [];
    list.add(json.encode(dataKey));

    Map data = {'Key': 'Data', 'Value': '$list'};

    final response = await _httpHelper.post(
        baseUrl, UrlConfig.EOR_DETAIL, json.encode(data));
    pr.dismiss();
    log(response.toString());
    Response res = Response.fromJson(response);
    print("PKM" + res.statusCode.toString());
//      pr.hide();
    if (res.statusCode == 200) {
      listPending = List();
      listProgress = List();
      listComplete = List();
      listPendingTask = List();
      listProgressTask = List();
      listCompleteTask = List();

      listHead = List();
      listTask = List();

      Map<String, dynamic> jsonResult = json.decode(res.results[0].value);

      String headerValue = jsonResult['Header'];
      if (headerValue != null) {
        List tempHeader = json.decode(headerValue);
        for (int i = 0; i < tempHeader.length; i++) {
          Header header = Header.fromJson(tempHeader[i]);
          listHead.add(header);
        }
      }

      String taskValue = jsonResult['TaskList'];
      if (taskValue != null) {
        print(taskValue);
        List tempTask = json.decode(taskValue
            .replaceAll('\n', '\\n')
            .replaceAll('\r', '\\r')
            .replaceAll('\t', '\\t'));
        for (int i = 0; i < tempTask.length; i++) {
          TaskList taskList = TaskList.fromJson(tempTask[i]);
          listTask.add(taskList);
        }
      }

      for (int i = 0; i < listHead.length; i++) {
        if (listTask[i]
            .status
            .toLowerCase()
            .contains(("Pending start").toLowerCase())) {
          listPending.add(listHead[i]);
          listPendingTask.add(listTask[i]);
        } else if (listTask[i]
            .status
            .toLowerCase()
            .contains(("Pending completion").toLowerCase())) {
          listProgress.add(listHead[i]);
          listProgressTask.add(listTask[i]);
        } else {
          listComplete.add(listHead[i]);
          listCompleteTask.add(listTask[i]);
        }
      }

      setState(() {
        if (listPending.length > 0 && listPending[0].allowToEdit == "0" ||
            listComplete.length > 0 && listComplete[0].allowToEdit == "0" ||
            listProgress.length > 0 && listProgress[0].allowToEdit == "0") {
          _isSurveyor = true;
        } else if (listPending.length > 0 &&
                listPending[0].allowToEdit == "1" ||
            listComplete.length > 0 && listComplete[0].allowToEdit == "1" ||
            listProgress.length > 0 && listProgress[0].allowToEdit == "1") {
          _isSurveyor = false;
        }
        isLoading = false;
      });
    } else {
      if (res.errorMessage.contains("AccessToken"))
        new force_logout(context).doForceLogout();
      Fluttertoast.showToast(
          msg: res.errorMessage,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }

  // Future<Null> _readContainer() async {
  //   File image = await ImagePicker.pickImage(source: ImageSource.camera);
  //
  //   final inputImage = google_mlkit.InputImage.fromFile(image);
  //
  //   google_mlkit.RecognisedText regText =
  //       await textDetector.processImage(inputImage);
  //
  //   print("TEXT " + regText.textBlocks[0].textLines[0].lineText);
  //
  //   String text =
  //       regText.textBlocks[0].textLines[0].lineText.replaceAll(" ", "");
  //   final regex = RegExp(r'^[A-Z]{4}\d{6}$');
  //
  //   if (regex.hasMatch(text)) {
  //     txtSearchController.text = _getCheckDigit(text);
  //   } else {
  //     txtSearchController.text = text;
  //   }
  // }

  String _getCheckDigit(String containerNo) {
    int value;
    int result = 0;

    for (int i = 0; i < 10; i++) {
      if (i < 4) {
        value = (11 * (containerNo[i].codeUnits[0] - 56) ~/ 10) + 1;
        print("value string = " + value.toString());
      } else {
        value = (containerNo[i].codeUnits[0] - 48);
        print("value interger = " + value.toString());
      }

      int newValue = value * math.pow(2, i);

      result += newValue;
      print("resutl = " + result.toString() + " " + i.toString());
    }

    int checkDigit = (result - (result ~/ 11) * 11).toInt();

    return containerNo + checkDigit.toString();
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQuery = MediaQuery.of(context);

    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);

    if (_isSurveyor) {
      return DefaultTabController(
        length: 3,
        // The Builder widget is used to have a different BuildContext to access
        // closest DefaultTabController.
        child: Builder(builder: (BuildContext context) {
          final TabController tabController = DefaultTabController.of(context);
          tabController.addListener(() {
            if (!tabController.indexIsChanging) {
              // Your code goes here.
              // To get index of current tab use tabController.index
            }
          });
          return Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              leading: BackButton(
                color: Colors.white,
              ),
              title: Text(
                "EOR",
                style: AppTheme.title_white,
              ),
              actions: <Widget>[
                Padding(
                    padding: EdgeInsets.only(right: 20.0),
                    child: GestureDetector(
                      onTap: () {
                        txtSearchController.text = "";
                        _submitCondition("");
                      },
                      child: Icon(
                        Icons.refresh,
                        size: 26.0,
                        color: Colors.white,
                      ),
                    )),
              ],
              backgroundColor: AppTheme.colorPrimary,
              bottom: DecoratedTabBar(
                tabBar: TabBar(
                  unselectedLabelColor: Colors.grey,
                  labelColor: Colors.blueGrey,
                  indicatorColor: Colors.blueGrey,
                  tabs: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "Pending Approve",
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 15.0,
                            fontFamily: "Gotik"),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "Pending Start",
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 15.0,
                            fontFamily: "Gotik"),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "Pending Completion",
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 15.0,
                            fontFamily: "Gotik"),
                      ),
                    ),
                  ],
                ),
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color: Colors.grey,
                      width: 2.0,
                    ),
                  ),
                ),
              ),
            ),
            body: Column(
              children: [
                new Form(
                    child: new TextFormField(
                  controller: txtSearchController,
                  keyboardType: TextInputType.text,
                  textInputAction: TextInputAction.search,
                  textCapitalization: TextCapitalization.characters,
                  // Use email input type for emails.
                  // inputFormatters: [
                  //   UpperCaseTextFormatter(),
                  // ],
                  decoration: new InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: AppTheme.colorPrimary, width: 1.0),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: AppTheme.colorMediumGrey, width: 1.0),
                      ),
                      suffixIcon: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          GestureDetector(
                              onTap: () {
                                // _readContainer();
                              },
                              child: Image.asset(
                                "assets/images/ic_ocr.png",
                                height: 35,
                              )),
                          IconButton(
                            icon: Icon(FontAwesomeIcons.search),
                            onPressed: () {
                              this._submitCondition(txtSearchController.text);
                            },
                          ),
                        ],
                      ),
                      hintText: 'Enter Container No.',
                      border: new OutlineInputBorder()),
                  autofocus: false,
                  validator: (String value) {
                    return null;
                  },
                  onFieldSubmitted: (value) {
                    _submitCondition(value);
                  },
                )),
                Expanded(
                  child: TabBarView(
                    children: [
                      isLoading
                          ? Center(
                              child: Container(
                                  height: 50,
                                  width: 50,
                                  child: CircularProgressIndicator()))
                          : EorListing(listComplete, listCompleteTask, 2,
                              this._submitCondition),
                      isLoading
                          ? Center(
                              child: Container(
                                  height: 50,
                                  width: 50,
                                  child: CircularProgressIndicator()))
                          : EorListing(listPending, listPendingTask, 0,
                              this._submitCondition),
                      isLoading
                          ? Center(
                              child: Container(
                                  height: 50,
                                  width: 50,
                                  child: CircularProgressIndicator()))
                          : EorListing(listProgress, listProgressTask, 1,
                              this._submitCondition),
                    ],
                  ),
                ),
              ],
            ),
          );
        }),
      );
    } else {
      return DefaultTabController(
        length: 2,
        // The Builder widget is used to have a different BuildContext to access
        // closest DefaultTabController.
        child: Builder(builder: (BuildContext context) {
          final TabController tabController = DefaultTabController.of(context);
          tabController.addListener(() {
            if (!tabController.indexIsChanging) {
              // Your code goes here.
              // To get index of current tab use tabController.index
            }
          });
          return Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              leading: BackButton(
                color: Colors.white,
              ),
              title: Text(
                "EOR",
                style: AppTheme.title_white,
              ),
              actions: <Widget>[
                Padding(
                    padding: EdgeInsets.only(right: 20.0),
                    child: GestureDetector(
                      onTap: () {
                        txtSearchController.text = "";
                        _submitCondition("");
                      },
                      child: Icon(
                        Icons.refresh,
                        size: 26.0,
                        color: Colors.white,
                      ),
                    )),
              ],
              backgroundColor: AppTheme.colorPrimary,
              bottom: DecoratedTabBar(
                tabBar: TabBar(
                  unselectedLabelColor: Colors.grey,
                  labelColor: Colors.blueGrey,
                  indicatorColor: Colors.blueGrey,
                  tabs: [
//                    Padding(
//                      padding: const EdgeInsets.all(8.0),
//                      child: Text(
//                        "Pending Approve",
//                        style: TextStyle(
//                            fontWeight: FontWeight.w600,
//                            fontSize: 15.0,
//                            fontFamily: "Gotik"),
//                      ),
//                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "Pending Start",
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 15.0,
                            fontFamily: "Gotik"),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "Pending Completion",
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 15.0,
                            fontFamily: "Gotik"),
                      ),
                    ),
                  ],
                ),
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color: Colors.grey,
                      width: 2.0,
                    ),
                  ),
                ),
              ),
            ),
            body: Column(
              children: [
                new Form(
                    child: new TextFormField(
                  controller: txtSearchController,
                  keyboardType: TextInputType.text,
                  textInputAction: TextInputAction.search,
                  // Use email input type for emails.
                      textCapitalization: TextCapitalization.characters,

                  //     inputFormatters: [
                  //   UpperCaseTextFormatter(),
                  // ],
                  decoration: new InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: AppTheme.colorPrimary, width: 1.0),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: AppTheme.colorMediumGrey, width: 1.0),
                      ),
                      suffixIcon: Wrap(
                        children: <Widget>[
                          IconButton(
                            icon: Icon(FontAwesomeIcons.qrcode),
                            onPressed: this._scanQR,
                          ),
                          IconButton(
                            icon: Icon(FontAwesomeIcons.search),
                            onPressed: () {
                              this._submitCondition(txtSearchController.text);
                            },
                          ),
                        ],
                      ),
                      hintText: 'Enter Container No.',
                      border: new OutlineInputBorder()),
                  autofocus: false,
                  validator: (String value) {
                    return null;
                  },
                  onFieldSubmitted: (value) {
                    _submitCondition(value);
                  },
                )),
                Expanded(
                  child: TabBarView(
                    children: [
//                isLoading
//                    ? Center(
//                    child: Container(
//                        height: 50,
//                        width: 50,
//                        child: CircularProgressIndicator()))
//                    : EorListing(
//                    listComplete, listCompleteTask, 2, _submitCondition),
                      isLoading
                          ? Center(
                              child: Container(
                                  height: 50,
                                  width: 50,
                                  child: CircularProgressIndicator()))
                          : EorListing(listPending, listPendingTask, 0,
                              this._submitCondition),
                      isLoading
                          ? Center(
                              child: Container(
                                  height: 50,
                                  width: 50,
                                  child: CircularProgressIndicator()))
                          : EorListing(listProgress, listProgressTask, 1,
                              this._submitCondition),
                    ],
                  ),
                ),
              ],
            ),
          );
        }),
      );
    }
  }

  Widget columnDynamic(var list) {
    listWidget = [];
    final decoded = list as Map;
    for (final name in decoded.keys) {
      String value = decoded[name];
      print("check try");
      if (value.length > 0)
        listWidget.add(
            dynamicWidget(name, value)); // prints entries like "AED,3.672940"
    }
    return Padding(
      padding: const EdgeInsets.only(bottom: 6.0),
      child: Column(
        children: listWidget,
      ),
    );
  }

  Widget dynamicWidget(String title, String subtitle) {
    return Column(
      children: [
        SizedBox(
          height: 6,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(flex: 1, child: Text(title + " :")),
            Expanded(flex: 2, child: Text(subtitle)),
          ],
        ),
      ],
    );
  }
}

class DecoratedTabBar extends StatelessWidget implements PreferredSizeWidget {
  DecoratedTabBar({@required this.tabBar, @required this.decoration});

  final TabBar tabBar;
  final BoxDecoration decoration;

  @override
  Size get preferredSize => tabBar.preferredSize;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned.fill(child: Container(decoration: decoration)),
        tabBar,
      ],
    );
  }
}
