import 'dart:convert';

//import 'package:depot/jobTracking/jobTaskDetail.dart';
//import 'package:depot/jobTracking/job_track_images.dart';
import 'package:depot/eor/eorDetails.dart';
import 'package:depot/eor/eorImage.dart';
import 'package:depot/models/eorModel.dart';
import 'package:depot/models/response.dart';

//import 'package:depot/models/taskDetailModel.dart';
import 'package:depot/styles/appTheme.dart';
import 'package:depot/utils/ApiBaseHelper.dart';
import 'package:depot/utils/AppPrefs.dart';
import 'package:depot/utils/UrlConfig.dart';
import 'package:depot/utils/force_logout.dart';
import 'package:depot/utils/progress_dialog.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';

//import 'package:location/location.dart';

class EorListing extends StatefulWidget {
  List<Header> list;
  List<TaskList> listTask;
  int type;
  Function function;

  @override
  _EorListingState createState() => _EorListingState();

  EorListing(this.list, this.listTask, this.type, this.function);
}

class _SearchData {
  String txtSearch = '';
}

class _EorListingState extends State<EorListing> with TickerProviderStateMixin {
//  List<JobTaskModel> items;
  ProgressDialog pr;
  ApiBaseHelper _httpHelper = ApiBaseHelper();
  final GlobalKey<FormState> _formKey_Remark = new GlobalKey<FormState>();
  _SearchData _data = new _SearchData();
  TextEditingController txtSearchController = new TextEditingController();
  String baseUrl;

//  Location location = new Location();
  bool _serviceEnabled;

//  PermissionStatus _permissionGranted;
//  LocationData _locationData;
  List<Widget> listWidget;

  String lat, long;

  @override
  void initState() {
    // TODO: implement initState
//    _fetchCurrentLocation();

//    items = List();
    super.initState();
  }

  String getDateTime() {
    DateTime dateTime = DateTime.now();

    return DateFormat('yyyy-MM-dd HH:mm').format(dateTime);
  }

  String getDateFormat(String date) {
    DateTime dateTime = DateTime.parse(date);

    return DateFormat('yyyy-MM-dd HH:mm').format(dateTime);
  }

  void _submitStart(Header header) async {
    pr.show();

    baseUrl = await AppPrefs.getAppServer();
//    await _fetchCurrentLocation();
    lat = await AppPrefs.read("userCurrentLatitude");
    long = await AppPrefs.read("userCurrentLongitude");

    Map dataKey = {
      "EORNo": header.eORNo,
      "StartDate": getDateTime(),
    };

    List list = [];
    list.add(json.encode(dataKey));

    Map data = {'Key': 'Data', 'Value': json.encode(dataKey)};

    final response = await _httpHelper.post(
        baseUrl, UrlConfig.UPDATE_EOR_DETAIL, json.encode(data));
    Response res = Response.fromJson(response);
    print("PKM" + res.statusCode.toString());
    pr.hide();
    if (res.statusCode == 200) {
      setState(() {
        Fluttertoast.showToast(
            msg: "EOR ${header.eORNo} started.",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );

        widget.function("");
      });
    } else {
      if(res.errorMessage.contains("AccessToken"))
        new force_logout(context).doForceLogout();
      Fluttertoast.showToast(
          msg: res.errorMessage,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );
      widget.function("");
    }
  }

  void _submitComplete(Header header) async {
    pr.show();

    baseUrl = await AppPrefs.getAppServer();
//    await _fetchCurrentLocation();
    lat = await AppPrefs.read("userCurrentLatitude");
    long = await AppPrefs.read("userCurrentLongitude");

    Map dataKey = {
      "EORNo": header.eORNo,
      "EndDate": getDateTime(),
    };

    List list = [];
    list.add(json.encode(dataKey));

    Map data = {'Key': 'Data', 'Value': json.encode(dataKey)};

    final response = await _httpHelper.post(
        baseUrl, UrlConfig.UPDATE_EOR_DETAIL, json.encode(data));
    Response res = Response.fromJson(response);
    print("PKM" + res.statusCode.toString());
    pr.hide();
    if (res.statusCode == 200) {
      setState(() {
        Fluttertoast.showToast(
            msg: "EOR ${header.eORNo} completed.",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );

        widget.function("");
      });
    } else {
      if(res.errorMessage.contains("AccessToken"))
        new force_logout(context).doForceLogout();
      Fluttertoast.showToast(
          msg: res.errorMessage,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );

      widget.function("");
    }
  }

  navDetails(Header headlist, TaskList tasklist) async {
    var back = await Navigator.of(context).push(PageRouteBuilder(
        pageBuilder: (_, __, ___) => new EorDetail(
              type: widget.type,
              header: headlist,
              taskList: tasklist,
            )));

    if (back != null) {
      widget.function("");
    }
  }

  /*

  _fetchCurrentLocation() async {
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }

    _locationData = await location.getLocation();
    print("STARTING LOCATION SERVICE");
//    var location = Location();
    location.changeSettings(
        accuracy: LocationAccuracy.powerSave,
        interval: 1000,
        distanceFilter: 500);

    AppPrefs.save(
        "userCurrentLatitude", _locationData.latitude.toString());
    AppPrefs.save(
        "userCurrentLongitude", _locationData.longitude.toString());
  }

  void _submitComplete(var jobTaskModel) async {
    pr.show();

    baseUrl = await AppPrefs.getAppServer();

    lat = await AppPrefs.read("userCurrentLatitude");
    long = await AppPrefs.read("userCurrentLongitude");

    Map dataKey = {
      "JobNo": jobTaskModel['JobNo'],
      "TaskName": jobTaskModel['TaskName'],
      "EndDate":getDateTime(),
      "Container": jobTaskModel['Container'],
      "longitude": long,
      "latitude": lat,
    };

    List list = [];
    list.add(json.encode(dataKey));

    Map data = {'Key': 'Data', 'Value': json.encode(dataKey)};

    final response = await _httpHelper.post(
        baseUrl, UrlConfig.APP_UPDATE_TASK_DETAIL, json.encode(data));
    Response res = Response.fromJson(response);
    print("PKM" + res.statusCode.toString());
    pr.hide();
    if (res.statusCode == 200) {
      setState(() {
        Toast.show("Job ${jobTaskModel['JobNo']} completed.", context, duration: 3);
        widget.function();
      });
    } else {
      Toast.show(res.errorMessage, context, duration: 3);
    }
  }

  void _submitRemarks(var jobTaskModel) async {
    if(_formKey_Remark.currentState.validate()) {
      Navigator.of(context).pop();

      pr.show();
      baseUrl = await AppPrefs.getAppServer();

      lat = await AppPrefs.read("userCurrentLatitude");
      long = await AppPrefs.read("userCurrentLongitude");

//    var jsonJob = json.decode(jobTaskModel);

      Map dataKey = {
        "JobNo": jobTaskModel['JobNo'],
        "TaskName": jobTaskModel['TaskName'],
        "Container": jobTaskModel['Container'],
        "Remarks": _data.txtSearch,
      };

      List list = [];
      list.add(json.encode(dataKey));

      Map data = {'Key': 'Data', 'Value': json.encode(dataKey)};

      final response = await _httpHelper.post(
          baseUrl, UrlConfig.APP_UPDATE_TASK_DETAIL, json.encode(data));
      Response res = Response.fromJson(response);
      print("PKM" + res.statusCode.toString());
      pr.hide();
      if (res.statusCode == 200) {
        setState(() {
          Toast.show("Remarks submitted", context, duration: 3);
          widget.function();
        });
      } else {
        Toast.show(res.errorMessage, context, duration: 3);
      }
    }
  }*/

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);

    if (widget.list.length > 0) {
      return Container(
        child: ListView.builder(
            itemCount: widget.list.length,
            padding: const EdgeInsets.symmetric(),
            itemBuilder: (context, position) {
              return GestureDetector(
                onTap: () {
                  _onTapItem(context, widget.list[position]);
                },
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 2.0, right: 2, top: 2, bottom: 2),
                  child: columnDynamic(widget.list[position],
                      widget.listTask[position], widget.type),
                ),
              );
            }),
      );
    } else {
      return emptyList(widget.type);
    }
  }

  Widget columnDynamic(Header headlist, TaskList tasklist, int type) {
    listWidget = [];
    var list = headlist.toJson();
    final decoded = list as Map;
    for (final name in decoded.keys) {
      String value = decoded[name];
      RegExp dateMatch = RegExp(
          r'^([0-9]{4})+\-+([0-9]{2})+\-+([0-9]{2})T+([0-9]{2}):+([0-9]{2}):+([0-9]{2})');
      if (value.length > 0) if (dateMatch.hasMatch(value)) {
        listWidget.add(dynamicWidget(
            name, getDateFormat(value))); // prints entries like "AED,3.672940"

      } else {
        listWidget.add(
            dynamicWidget(name, value)); // prints entries like "AED,3.672940"
      }
    }
    return Padding(
        padding: const EdgeInsets.only(bottom: 0.0),
        child: Container(
            child: Padding(
          padding:
              const EdgeInsets.only(left: 20, right: 20, top: 8, bottom: 8),
          child: InkWell(
            splashColor: Colors.transparent,
            onTap: () {
              navDetails(headlist, tasklist);
            },
            child: headlist.allowToEdit == "1"
                ? Card(
                    elevation: 7,
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(0.0),
                          child: Card(
                            elevation: 0,
                            child: ClipRRect(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(12.0)),
                                child: Container(
                                  height: widget.type > 0 ? 90 : 80,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(4)),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Expanded(
                                          child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Container(
                                                height: 40,
                                                child: Image.asset(
                                                    "assets/images/container.png")),
                                            SizedBox(
                                              width: 12,
                                            ),
                                            Expanded(
                                              child: Column(
                                                children: <Widget>[
                                                  Expanded(
                                                    child: Row(
                                                      children: <Widget>[
                                                        Expanded(
                                                            child: Text(
                                                          headlist.containerNo,
                                                          style: AppTheme.title,
                                                        )),
                                                      ],
                                                    ),
                                                  ),
                                                  Expanded(
                                                    child: Row(
                                                      children: <Widget>[
                                                        Expanded(
                                                            child: Text(
                                                          headlist.iSOCode,
                                                          style: AppTheme
                                                              .titleDesc,
                                                        )),
                                                        Expanded(
                                                            child: Text(
                                                                headlist
                                                                    .operatorCode,
                                                                style: AppTheme
                                                                    .titleDesc,
                                                                textAlign:
                                                                    TextAlign
                                                                        .end)),
                                                      ],
                                                    ),
                                                  ),
                                                  Expanded(
                                                    child: Row(
                                                      children: <Widget>[
                                                        Expanded(
                                                            child: Text(
                                                          headlist.eORNo,
                                                          maxLines: 1,
                                                          style: AppTheme
                                                              .titleDesc,
                                                        )),
                                                      ],
                                                    ),
                                                  ),
                                                  widget.type > 0
                                                      ? widget.type == 1
                                                          ? Expanded(
                                                              child: Row(
                                                                children: <
                                                                    Widget>[
                                                                  Expanded(
                                                                      child:
                                                                          Text(
                                                                    "Start Date : " +
                                                                        getDateFormat(
                                                                            tasklist.startDate),
                                                                    style: AppTheme
                                                                        .titleDesc,
                                                                    maxLines: 1,
                                                                  )),
                                                                ],
                                                              ),
                                                            )
                                                          : Expanded(
                                                              child: Row(
                                                                children: <
                                                                    Widget>[
                                                                  Expanded(
                                                                      child:
                                                                          Text(
                                                                    "End Date : " +
                                                                        getDateFormat(
                                                                            tasklist.endDate),
                                                                    style: AppTheme
                                                                        .titleDesc,
                                                                    maxLines: 1,
                                                                  )),
                                                                ],
                                                              ),
                                                            )
                                                      : SizedBox(),
                                                ],
                                              ),
                                            ),
                                            SizedBox(
                                              width: 30,
                                            ),
//                                    !post.isFileAttached ? Container(height:100,child: Center(child: Container(height:60,child: Image.asset("assets/images/camera.png")))):
                                            Container(
                                                height: 100,
                                                child: Center(
                                                    child: InkWell(
                                                  onTap: () {
                                                    Navigator.push(
                                                        context,
                                                        MaterialPageRoute(
                                                            builder: (_) =>
                                                                EORImage(
                                                                  docNo: headlist
                                                                      .eORNo,
                                                                  containerNo:
                                                                      headlist
                                                                          .containerNo,
                                                                  docType:
                                                                      "EOR",
                                                                )));
                                                  },
                                                  child: Container(
                                                      height: 60,
                                                      child: Image.asset(
                                                          "assets/images/camera.png")),
                                                ))),
                                          ],
                                        ),
                                      )),
                                    ],
                                  ),
                                )),
                          ),
                        ),
                        // widget.type == 2 ? SizedBox() : Divider(),
                        // dynamicBar(widget.type, headlist)
                      ],
                    ),
                  )
                : Card(
                    elevation: 7.0,
                    child: ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(12.0)),
                        child: Container(
                          height: 80,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(4)),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                  child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                        height: 40,
                                        child: Image.asset(
                                            "assets/images/container.png")),
                                    SizedBox(
                                      width: 12,
                                    ),
                                    Expanded(
                                      child: Column(
                                        children: <Widget>[
                                          Expanded(
                                            child: Row(
                                              children: <Widget>[
                                                Expanded(
                                                    child: Text(
                                                  headlist.containerNo,
                                                  style: AppTheme.title,
                                                )),
                                              ],
                                            ),
                                          ),
                                          Expanded(
                                            child: Row(
                                              children: <Widget>[
                                                Expanded(
                                                    child: Text(
                                                  headlist.iSOCode,
                                                  style: AppTheme.titleDesc,
                                                )),
                                                Expanded(
                                                    child: Text(
                                                        headlist.operatorCode,
                                                        style:
                                                            AppTheme.titleDesc,
                                                        textAlign:
                                                            TextAlign.end)),
                                              ],
                                            ),
                                          ),
                                          Expanded(
                                            child: Row(
                                              children: <Widget>[
                                                Expanded(
                                                    child: Text(
                                                  headlist.eORNo,
                                                  style: AppTheme.titleDesc,
                                                  maxLines: 1,
                                                )),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      width: 30,
                                    ),
                                    InkWell(
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (_) => EORImage(
                                                      docNo: headlist.eORNo,
                                                      containerNo:
                                                          headlist.containerNo,
                                                      docType: "EOR",
                                                    )));
                                      },
                                      child: Container(
                                          height: 100,
                                          child: Center(
                                              child: Container(
                                                  height: 60,
                                                  child: Image.asset(
                                                      "assets/images/camera.png")))),
                                    )
//                                    Container(
//                                        height: 100,
//                                        child: Center(
//                                            child: Container(
//                                                height: 60,
//                                                child: Image.asset(
//                                                    "assets/images/camera_taken.png")))),
                                  ],
                                ),
                              )),
                            ],
                          ),
                        )),
                  ),
          ),
        )));
  }

  _onTapItem(BuildContext context, var taskDetailModel) {
    print("Tapped on item");
//    Navigator.push(
//        context,
//        MaterialPageRoute(
//            builder: (_) => JobTaskDetail(
//              taskDetailModel: taskDetailModel,
//            )));
  }

  Widget dynamicBar(int type, Header header) {
    if (type == 0) {
      return Padding(
        padding: const EdgeInsets.only(bottom: 8),
        child: Row(
          children: [
            Expanded(
              child: Container(
                color: Colors.white,
                child: Center(
                  child: GestureDetector(
                    onTap: () {
                      _submitStart(header);
                    },
                    child: Card(
                        elevation: 3,
                        color: AppTheme.colorPrimary,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 8.0, horizontal: 30),
                          child: Text(
                            "Start",
                            style: AppTheme.btnText,
                          ),
                        )),
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    } else if (type == 1) {
      return Padding(
        padding: const EdgeInsets.only(bottom: 8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(
              child: Container(
                color: Colors.white,
                child: Center(
                  child: GestureDetector(
                    onTap: () {
                      _submitComplete(header);
                    },
                    child: Card(
                        elevation: 3,
                        color: AppTheme.colorPrimary,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 8.0, horizontal: 20),
                          child: Text("Complete", style: AppTheme.btnText),
                        )),
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return SizedBox();
    }
  }

  _remarkDlg(var jobTaskModel) async {
    await showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) => Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(16),
              ),
              elevation: 0.0,
              backgroundColor: Colors.transparent,
              child: Container(
                height: 300,
                width: double.infinity,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(16)),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: <Widget>[
                      Text(
                        'Remark',
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w400,
                            fontFamily: AppTheme.fontName),
                        textAlign: TextAlign.center,
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: <Widget>[
                              new Expanded(
                                  child: new Form(
                                      key: this._formKey_Remark,
                                      child: new TextFormField(
                                        controller: txtSearchController,
                                        keyboardType: TextInputType.multiline,
                                        textInputAction:
                                            TextInputAction.newline,
                                        maxLines: 10,
                                        decoration: new InputDecoration(
                                            focusedBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: AppTheme.colorPrimary,
                                                  width: 1.0),
                                            ),
                                            enabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color:
                                                      AppTheme.colorSecondary,
                                                  width: 1.0),
                                            ),
//                                        suffixIcon: IconButton(
//                                          icon:
//                                          Icon(FontAwesomeIcons.qrcode),
//                                          onPressed: this._scanQR,
//                                        ),
                                            hintText: 'Enter Remark',
                                            labelText: 'Remark',
                                            border: new OutlineInputBorder()),
                                        validator: (value) {
                                          if (value.length > 250) {
                                            return "Remark cannot exceed 250characters";
                                          }
                                          return null;
                                        },
                                        onChanged: (value) {
                                          this._data.txtSearch = value;
                                        },
                                      ))),
                            ],
                          ),
                        ),
                      ),
//                      errorMsg == "" ?SizedBox():Text(errorMsg,style: TextStyle(color: Colors.red),),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              color: Colors.transparent,
                              height: 50.0,
                              margin:
                                  new EdgeInsets.only(bottom: 10.0, top: 20.0),
                              child: Card(
                                elevation: 5.0,
                                // this field changes the shadow of the card 1.0 is default
                                child: new RaisedButton(
                                  padding: const EdgeInsets.all(10.0),
                                  child: new Text(
                                    'Submit',
                                    style: AppTheme.btnText,
                                  ),
                                  onPressed: () {
//                                this._updateRemark();
//                                _submitRemarks(jobTaskModel);
                                  },
                                  color: AppTheme.colorPrimary,
//                              shape: RoundedRectangleBorder(
//                                  borderRadius: new BorderRadius.circular(10.0),
//                                  side: BorderSide(color: Colors.transparent)),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Container(
                              color: Colors.transparent,
                              height: 50.0,
                              margin:
                                  new EdgeInsets.only(bottom: 10.0, top: 20.0),
                              child: Card(
                                elevation: 5.0,
                                // this field changes the shadow of the card 1.0 is default
                                child: new RaisedButton(
                                  padding: const EdgeInsets.all(10.0),
                                  child: new Text(
                                    'Cancel',
                                    style: AppTheme.btnText,
                                  ),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                  color: AppTheme.colorPrimary,
//                              shape: RoundedRectangleBorder(
//                                  borderRadius: new BorderRadius.circular(10.0),
//                                  side: BorderSide(color: Colors.transparent)),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ));
  }

  String validateRmk(String value) {
    if (value.length > 250) {
      return "Remark cannot exceed 250 character.";
    }
    return null;
  }

  Widget dynamicWidget(String title, String subtitle) {
    return Column(
      children: [
        SizedBox(
          height: 6,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(flex: 1, child: Text(title + " :")),
            Expanded(flex: 2, child: Text(subtitle)),
          ],
        ),
      ],
    );
  }

  Widget emptyList(int type) {
    return Container(
      width: 500.0,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).padding.top + 100.0)),
            Icon(
              Icons.new_releases,
              color: Colors.red.shade300,
              size: 60,
            ),
            Padding(padding: EdgeInsets.only(bottom: 30.0)),
            type != 0
                ? type == 1
                    ? Text(
                        "NO PENDING COMPLETION EOR AVAILABLE",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: 18.5,
                            color: Colors.black54,
                            fontFamily: AppTheme.fontName),
                      )
                    : Text(
                        "NO PENDING APPROVED EOR",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: 18.5,
                            color: Colors.black54,
                            fontFamily: AppTheme.fontName),
                      )
                : Text(
                    "NO PENDING START EOR AVAILABLE.",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 18.5,
                        color: Colors.black54,
                        fontFamily: AppTheme.fontName),
                  ),
          ],
        ),
      ),
    );
  }
}
