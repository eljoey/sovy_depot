import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:depot/models/container.dart';
import 'package:depot/models/eorModel.dart';
import 'package:depot/models/response.dart';
import 'package:depot/styles/appTheme.dart';
import 'package:depot/utils/ApiBaseHelper.dart';
import 'package:depot/utils/AppPrefs.dart';
import 'package:depot/utils/UrlConfig.dart';
import 'package:depot/utils/file_process.dart';
import 'package:depot/utils/force_logout.dart';
import 'package:depot/utils/progress_dialog.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image/image.dart' as imgg;
import 'package:intl/intl.dart';
import 'package:pdf/pdf.dart';

class EorDetail extends StatefulWidget {
  Header header;
  TaskList taskList;
  int type;

  EorDetail({Key key, this.type = 2,this.header,this.taskList}) : super(key: key);

  @override
  _EorDetailState createState() => _EorDetailState();
}

class _SearchData {
  String txtSearch = '';
}

class _EorDetailState extends State<EorDetail> {
  ApiBaseHelper _httpHelper = ApiBaseHelper();
  final GlobalKey<FormState> _formKey_Remark = new GlobalKey<FormState>();
  ProgressDialog pr;
  _SearchData _data = new _SearchData();
  String result, baseUrl, resultRemark = "";
  TextEditingController txtSearchController = new TextEditingController();
  List<Header> listHead;
  List<TaskList> listTask;
  List<Widget> listWidget;
  bool isLoading = false;

  @override
  void initState() {
    listHead = List();
    listTask = List();
    getData();
    // TODO: implement initState
    super.initState();

//    _submitCondition();
  }

  String getDateTime() {
    DateTime dateTime = DateTime.now();

    return DateFormat('yyyy-MM-dd HH:mm').format(dateTime);
  }

  void _submitStart(Header header) async {
    pr.show();

    baseUrl = await AppPrefs.getAppServer();

    Map dataKey = {
      "EORNo": header.eORNo,
      "StartDate": getDateTime(),
    };

    List list = [];
    list.add(json.encode(dataKey));

    Map data = {'Key': 'Data', 'Value': json.encode(dataKey)};

    final response = await _httpHelper.post(
        baseUrl, UrlConfig.UPDATE_EOR_DETAIL, json.encode(data));
    Response res = Response.fromJson(response);
    print("PKM" + res.statusCode.toString());
    pr.hide();
    if (res.statusCode == 200) {
      setState(() {
        Fluttertoast.showToast(
            msg: "EOR ${header.eORNo} started.",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );

        Navigator.of(context).pop(true);
      });
    } else {
      if(res.errorMessage.contains("AccessToken"))
        new force_logout(context).doForceLogout();

      Fluttertoast.showToast(
          msg: res.errorMessage,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );
    }
  }

  void _submitComplete(Header header) async {
    pr.show();

    baseUrl = await AppPrefs.getAppServer();

    Map dataKey = {
      "EORNo": header.eORNo,
      "EndDate": getDateTime(),
    };

    List list = [];
    list.add(json.encode(dataKey));

    Map data = {'Key': 'Data', 'Value': json.encode(dataKey)};

    final response = await _httpHelper.post(
        baseUrl, UrlConfig.UPDATE_EOR_DETAIL, json.encode(data));
    Response res = Response.fromJson(response);
    print("PKM" + res.statusCode.toString());
    pr.hide();
    if (res.statusCode == 200) {
      setState(() {
        Fluttertoast.showToast(
            msg: "EOR ${header.eORNo} completed.",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
        Navigator.of(context).pop(true);
      });
    } else {
      if(res.errorMessage.contains("AccessToken"))
        new force_logout(context).doForceLogout();
      Fluttertoast.showToast(
          msg: res.errorMessage,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );
    }
  }

  void _getPDF(Header header) async {
    pr.show();

    baseUrl = await AppPrefs.getAppServer();
    Map dataKey;
    List list = [];

    dataKey = {
      "Key": "EORNo",
      "Value": header.eORNo,
    };

    list.add(json.encode(dataKey));


    Map data = {'Key': 'Data', 'Value': "$list"};

    final response = await _httpHelper.post(
        baseUrl, UrlConfig.GET_EOR_REPORT, json.encode(data));
    Response res = Response.fromJson(response);
    pr.hide();
    if (res.statusCode == 200) {
      if(res.results[0].value != null && res.results[0].value != "null" && res.results[0].value.length>0) {
        await FileProcess.downloadFile(res.results[0].value, header.eORNo);
        FileProcess.openFile(header.eORNo, context);
      }else{
        Fluttertoast.showToast(
            msg: "Invalid PDF.",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
      }
    } else {
      if(res.errorMessage.contains("AccessToken"))
        new force_logout(context).doForceLogout();
      Fluttertoast.showToast(
          msg: res.errorMessage,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );
    }
  }


  void _submitCondition() async {
    baseUrl = await AppPrefs.getAppServer();
    isLoading = true;

    Map dataKey = {
      "Key": "ContainerNumber",
      "Value": "",
    };

    List list = [];
    list.add(dataKey);

    Map data = {'Key': 'Data', 'Value': json.encode(list)};

    final response = await _httpHelper.post(
        baseUrl, UrlConfig.EOR_DETAIL, json.encode(data));
    pr.dismiss();
    Response res = Response.fromJson(response);
    print("PKM" + res.statusCode.toString());
//      pr.hide();
    if (res.statusCode == 200) {
      listHead = List();
      listTask = List();

      Map<String, dynamic> jsonResult = json.decode(res.results[0].value);

      String headerValue = jsonResult['Header'];
      if (headerValue != null) {
        List tempHeader = json.decode(headerValue);
        for (int i = 0; i < tempHeader.length; i++) {
          Header header = Header.fromJson(tempHeader[i]);
          listHead.add(header);
        }

        listWidget = [];
        final decoded = tempHeader[0] as Map;
        print(tempHeader[0]);
        for (final name in decoded.keys) {
          String value = decoded[name];
          print("check try");
          if (value.length > 0)
            listWidget.add(dynamicWidget(
                name, value)); // prints entries like "AED,3.672940"
        }
      }

      String taskValue = jsonResult['TaskList'];
      if (taskValue != null) {
        print(taskValue);
        List tempTask = json.decode(taskValue
            .replaceAll('\n', '\\n')
            .replaceAll('\r', '\\r')
            .replaceAll('\t', '\\t'));
        for (int i = 0; i < tempTask.length; i++) {
          TaskList taskList = TaskList.fromJson(tempTask[i]);
          listTask.add(taskList);
        }
      }

//      TaskDetailsModel taskDetailsModel = TaskDetailsModel.fromJson(jsonResult);
//      print(taskDetailsModel.header);
      setState(() {
        isLoading = false;
      });
    } else {
      setState(() {
        isLoading = false;
      });
      if(res.errorMessage.contains("AccessToken"))
        new force_logout(context).doForceLogout();
      Fluttertoast.showToast(
          msg: res.errorMessage,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );
    }
  }

  getData() async {
    baseUrl = await AppPrefs.getAppServer();
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData media = MediaQuery.of(context);
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);
    final Size screenSize = media.size;
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(
          onPressed: () {
            Navigator.of(context).pop(true);
          },
          color: Colors.white,
        ),
        title: Text(
          "EOR Detail",
          style: AppTheme.title_white,
        ),
      ),
      body: Stack(
        children: <Widget>[
          Container(
            child: Column(
              children: <Widget>[
                isLoading
                    ? Padding(
                        padding: const EdgeInsets.only(top: 18.0),
                        child: Center(
                          child: CircularProgressIndicator(),
                        ),
                      )
                    : Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Card(
elevation: 7,
                          child: Padding(
                            padding: const EdgeInsets.only(
                                top: 4.0, left: 10, right: 10, bottom: 10),
                            child: widget.header!= null
                                ? Column(
                                        children: [
                                          columnHeader(widget.header),
                                          columnTask(widget.taskList),
                                          Padding(
                                            padding: const EdgeInsets.symmetric(vertical: 8.0),
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: [
                                                widget.header.allowToEdit == "1"? dynamicBar(widget.type, widget.header):SizedBox(),
                                                Container(
                                                  color: Colors.white,
                                                  child: Center(
                                                    child: GestureDetector(
                                                      onTap: () {
                                                        print("here");
                                                        _getPDF(widget.header);
                                                      },
                                                      child: Card(
                                                          elevation: 3,
                                                          color: AppTheme.colorPrimary,
                                                          child: Container(
                                                            width: 140,
                                                            child: Padding(
                                                              padding: const EdgeInsets.symmetric(
                                                                  vertical: 8.0),
                                                              child: Center(child: Text("Preview", style: AppTheme.btnText)),
                                                            ),
                                                          )),
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ],
                                  )
                                : Container(
                                    child: Center(
                                        child: Padding(
                                      padding: const EdgeInsets.all(15.0),
                                      child: Column(
                                        children: [
                                          Icon(
                                            Icons.warning,
                                            size: 50,
                                            color: Colors.red,
                                          ),
                                          Text("No Data. Please reselect."),
                                        ],
                                      ),
                                    )),
                                  ),
                          ),
                        ),
                    ),
                SizedBox(
                  height: 18,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget dynamicBar(int type,Header header) {
    if (type == 0) {
      return Container(
                color: Colors.white,
                child: Center(
                  child: GestureDetector(
                    onTap: () {
                      _submitStart(header);
                    },
                    child: Card(
                        elevation: 3,
                        color: AppTheme.colorPrimary,
                        child: Container(
                          width: 140,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 8.0),
                            child:Center(
                              child: Text(
                                  "Start",
                                  style: AppTheme.btnText,
                                ),
                            ),
                          ),
                        )),
                  ),
                ),
              );
    } else if (type == 1) {
      return Container(
                color: Colors.white,
                child: Center(
                  child: GestureDetector(
                    onTap: () {
                      _submitComplete(header);
                    },
                    child: Card(
                        elevation: 3,
                        color: AppTheme.colorPrimary,
                        child: Container(
                          width: 140,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 8.0),
                            child: Center(child: Text("Complete", style: AppTheme.btnText)),
                          ),
                        )),
                  ),
                ),
              );

    } else {
      return SizedBox();
    }
  }

  Widget columnDynamic(var list) {
    listWidget = [];
    final decoded = list as Map;
    for (final name in decoded.keys) {
      String value = decoded[name];
      print("check try");
      if (value.length > 0)
        listWidget.add(
            dynamicWidget(name, value)); // prints entries like "AED,3.672940"
    }
    return Padding(
      padding: const EdgeInsets.only(bottom: 6.0),
      child: Column(
        children: listWidget,
      ),
    );
  }

  Widget columnHeader(Header header){
    return Column(
      children: [
        dynamicWidget("EOR No",header.eORNo),
        dynamicWidget("Operator Code",header.operatorCode),
        dynamicWidget("Container No",header.containerNo),
        dynamicWidget("ISO Code",header.iSOCode),
      ],
    );
  }

  Widget columnTask(TaskList taskList){
    return Column(
      children: [
        dynamicWidget("EOR Category",taskList.eORCategory),
        dynamicWidget("Customer Ref",taskList.customerRef),
        dynamicWidget("Customer Name",taskList.customerName),
        dynamicWidget("Type Size",taskList.typeSize),
        taskList.startDate == null || taskList.startDate ==""? SizedBox():dynamicWidget("Start Date",getDateFormat(taskList.startDate)),
        taskList.endDate == null || taskList.endDate ==""? SizedBox():dynamicWidget("Ebd Date",getDateFormat(taskList.endDate)),
        dynamicWidget("Status",taskList.status),
      ],
    );
  }

  Widget dynamicWidget(String title, String subtitle) {
    return Column(
      children: [
        SizedBox(
          height: 6,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical:5.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Expanded(flex: 1, child: Text(title)),
              Text(": "),
              Expanded(flex: 2, child: Text(subtitle)),
            ],
          ),
        ),
        Divider(height: 0.5,)
      ],
    );
  }

  String getDateFormat(String date) {
    DateTime dateTime = DateTime.parse(date);

    return DateFormat('yyyy-MM-dd HH:mm').format(dateTime);
  }
}

class TaskProgress extends StatelessWidget {
  int position;
  TaskList _taskList;
  int length;

  TaskProgress(this.position, this._taskList, this.length);

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Padding(
        padding: const EdgeInsets.only(left: 18.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 12.0),
            ),
            Expanded(
              child: taskProgressing(_taskList),
            )
          ],
        ),
      ),
    ]);
  }

  Widget taskProgressing(TaskList taskList) {
    Color colorText;

    return Padding(
      padding: const EdgeInsets.only(left: 18.0, right: 18, top: 0, bottom: 5),
      child: Container(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 2.0),
                child: Text(
                  taskList.customerName,
                  style: TextStyle(
                      fontFamily: AppTheme.fontName,
                      color: colorText,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 2.0),
                child: taskList.customerRef != null
                    ? taskList.customerRef.length > 0
                        ? Text("(${taskList.customerRef})",
                            style: TextStyle(
                                fontFamily: AppTheme.fontName,
                                color: colorText))
                        : SizedBox()
                    : SizedBox(),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Divider(
                  height: 2,
                  color: Colors.black54,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget bigCircle(String status) {
    if (status == "Completed") {
      return Container(
        height: 12,
        width: 12,
        decoration: BoxDecoration(
            color: AppTheme.colorPrimary,
            borderRadius: BorderRadius.circular(6)),
      );
    } else if (status == "Progress") {
      return Container(
        height: 12,
        width: 12,
        decoration: BoxDecoration(
            color: Colors.lightBlue, borderRadius: BorderRadius.circular(6)),
      );
    } else {
      return Container(
        height: 12,
        width: 12,
        decoration: BoxDecoration(
            color: Colors.grey, borderRadius: BorderRadius.circular(6)),
      );
    }
  }

  String getDateTime(String date) {
    DateTime dateTime = DateTime.parse(date);

    return DateFormat('yyyy-MM-dd HH:mm').format(dateTime);
  }

  Widget smallCircle(String status) {
    if (status == "Completed") {
      return Container(
        height: 8,
        width: 8,
        decoration: BoxDecoration(
            color: AppTheme.colorPrimary,
            borderRadius: BorderRadius.circular(4)),
      );
    } else if (status == "Progress") {
      return Container(
        height: 8,
        width: 8,
        decoration: BoxDecoration(
            color: AppTheme.white,
            borderRadius: BorderRadius.circular(4),
            border: Border.all(color: AppTheme.colorPrimary, width: 1)),
      );
    } else {
      return Container(
        height: 8,
        width: 8,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(4),
            border: Border.all(color: Colors.black45, width: 1)),
      );
    }
  }
}
