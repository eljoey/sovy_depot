import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
  import 'package:flutter_cached_pdfview/flutter_cached_pdfview.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:oktoast/oktoast.dart';

import '../styles/appTheme.dart';

class PDFScreen2 extends StatefulWidget {
  String pathPDF = "";
  String pdfName = "";
  bool isOnline = false;
  bool isHorizontal = false;
  bool doFling = true;

  PDFScreen2(this.pathPDF, this.pdfName,
      {this.isOnline = false, this.isHorizontal = false, this.doFling = true});

  @override
  _PDFScreen2State createState() => _PDFScreen2State();
}

class _PDFScreen2State extends State<PDFScreen2> {
  // _shareLocalFile(String path) async {
  //   String name = path.substring(path.lastIndexOf("/") + 1);
  //   print("share path $path - $name");
  //   final ByteData bytes = await rootBundle.load(path);
  //   await Share.file(name, name, bytes.buffer.asUint8List(), 'application/pdf');
  // }
  //
  // _shareOnlineFile(String path) async {
  //   String name = path.substring(path.lastIndexOf("/") + 1);
  //   print("share path $path - $name");
  //   var request = await HttpClient().getUrl(Uri.parse(path));
  //   var response = await request.close();
  //   Uint8List bytes = await consolidateHttpClientResponseBytes(response);
  //   await Share.file(name, name, bytes, 'application/pdf');
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          leading: BackButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            color: Colors.white,
          ),
          title: Text(
            widget.pdfName,
            style: AppTheme.title_white,
          ),          backgroundColor: AppTheme.colorPrimary,
        ),
        body: OKToast(
            child: Stack(
              children: <Widget>[
                !widget.isOnline
                    ? PDF(
                  fitEachPage: true,
                  enableSwipe: true,
                  swipeHorizontal: widget.isHorizontal,
                  autoSpacing: false,
                  pageFling: widget.doFling,
                  onError: (error) {
                    print(error.toString());
                  },
                  onPageError: (page, error) {
                    //print('$page: ${error.toString()}');
                  },
                  onPageChanged: (int page, int total) {
                    //print('page change: $page/$total');
                  },
                  onRender: (pages) {
                    //print("count $pages");
                    if (pages > 1)
                      showToast("<< swipe-right >>",
                          duration: Duration(seconds: 1),
                          position: ToastPosition.center,
                          backgroundColor: Colors.black.withOpacity(0.8),
                          radius: 8.0,
                          textStyle:
                          TextStyle(fontSize: 18.0, color: Colors.white),
                          textPadding: const EdgeInsets.all(16.0));
                  },
                ).fromAsset(widget.pathPDF)
                    : PDF(
                  fitEachPage: true,
                  enableSwipe: true,
                  swipeHorizontal: widget.isHorizontal,
                  autoSpacing: false,
                  pageFling: widget.doFling,
                  onError: (error) {
                    print(error.toString());
                  },
                  onPageError: (page, error) {
                    //print('$page: ${error.toString()}');
                  },
                  onPageChanged: (int page, int total) {
                    //print('page change: $page/$total');
                  },
                  onRender: (pages) {
                    //print("count $pages");
                    if (pages > 1)
                      Fluttertoast.showToast(
                          msg: "<< swipe-right >>",
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.CENTER,
                          timeInSecForIosWeb: 1,
                          backgroundColor: Colors.black.withOpacity(0.8),
                          textColor: Colors.white,
                          fontSize: 18);
                  },
                ).cachedFromUrl(widget.pathPDF),
              ],
            )));
  }
}