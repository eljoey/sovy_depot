import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:barcode_scan2/barcode_scan2.dart';
import 'package:depot/depotOut/depot_out_condition.dart';
import 'package:depot/depotOut/depot_out_position.dart';
import 'package:depot/eor/eorImage.dart';
import 'package:depot/models/ContainerTruck.dart';
import 'package:depot/models/operatorModel.dart';
import 'package:depot/models/response.dart';
import 'package:depot/styles/appTheme.dart';
import 'package:depot/truckIn/truckImage.dart';
import 'package:depot/utils/ApiBaseHelper.dart';
import 'package:depot/utils/AppPrefs.dart';
import 'package:depot/utils/ShareMethod.dart';
import 'package:depot/utils/UrlConfig.dart';
import 'package:depot/utils/force_logout.dart';
import 'package:depot/utils/progress_dialog.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_mlkit_text_recognition/google_mlkit_text_recognition.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:depot/models/container.dart';
import 'package:flutter/services.dart';

class TruckIn extends StatefulWidget {
  @override
  _TruckInState createState() => _TruckInState();
}

class _TruckInData {
  String _customerRef = '';
  String _containerNo = '';
  String _primeMoverId = '';
  String _primeMoverRegNo = '';
  String _driverIC = '';
  String _driverName = '';
  String _operatorCode = '';
  String _typeSize = '';
  String _haulier = '';
  String _remark = '';
}

class _TruckInState extends State<TruckIn> {
  ApiBaseHelper _httpHelper = ApiBaseHelper();
  final GlobalKey<FormState> _formKey_searchList = new GlobalKey<FormState>();
  ProgressDialog pr;
  _TruckInData _data = new _TruckInData();
  String result, baseUrl, defaultStatus;
  List<ContainerModel> listContainer = [];
  TextEditingController txtSearchController = new TextEditingController();
  FocusNode focusContainer = new FocusNode();
  List listContainerWidget = List();
  List listContainerFiltered = List();
  bool _isCheck, _isSuccess = false, _isReadonly = false;
  List gradeTemp = [], jsonTemp = [], statusTemp = [];
  List<OperatorCode> _listOperator = [], _listTypeSize = [];
  final dobFormat = DateFormat("yyyy-MM-dd");
  final dobFormatStore = DateFormat("yyyy-MM-ddThh:mm:ss");
  String truckInDoc = "";
  ContainerTruck _containerTruck;
  FocusNode focusNode = new FocusNode();
  List listHaulier = [], tempListHaulier = [];
  bool _isRemarkFocused = false;

  ScrollController _scrollController = new ScrollController();

  TextEditingController tecCustomerRef = new TextEditingController();
  TextEditingController tecContainerNo = new TextEditingController();
  TextEditingController tecPrimeMoverId =
      new TextEditingController(); // Return value for success gate in
  TextEditingController tecPrimeMoverRegNo = new TextEditingController();
  TextEditingController tecDriverIC = new TextEditingController();
  TextEditingController tecDriverName = new TextEditingController();

  TextEditingController tecOperator = new TextEditingController();
  TextEditingController tecHaulier = new TextEditingController();
  TextEditingController tecTypeSize = new TextEditingController();
  TextEditingController tecRemark = new TextEditingController();

  TextEditingController tecModal = new TextEditingController();
  TextEditingController tecSearch = new TextEditingController();

  final ImagePicker _picker = ImagePicker();

  @override
  void initState() {
    getData();
    // TODO: implement initState
    super.initState();
  }

  popDialog(String title, List<Widget> list) {
    showCupertinoModalPopup(
      context: context,
      builder: (BuildContext context) => CupertinoActionSheet(
          title: Text(title),
          actions: list,
          cancelButton: CupertinoActionSheetAction(
            child: const Text('Cancel'),
            isDefaultAction: true,
            onPressed: () {
              Navigator.pop(context, 'Cancel');
            },
          )),
    );
  }

  TextRecognizer textDetector = TextRecognizer();

  Future<Null> _read(TextEditingController tec) async {
    XFile image = await _picker.pickImage(source: ImageSource.camera);

    final inputImage = InputImage.fromFile(File(image.path));

    final regText =
        await textDetector.processImage(inputImage);

    print("TEXT " + regText.blocks[0].lines[0].text);

    tec.text = regText.blocks[0].lines[0].text.replaceAll(" ", "").toUpperCase();
  }

  reset() {
    setState(() {
      _data = new _TruckInData();
      listContainerWidget = [];
      _listOperator = [];
      _listTypeSize = [];
      tecContainerNo.text = "";
      tecCustomerRef.text = "";
      tecRemark.text = "";
      tecHaulier.text = "";
      tecOperator.text = "";
      tecTypeSize.text = "";
      tecPrimeMoverId.text = "";
      tecPrimeMoverRegNo.text = "";
      tecDriverIC.text = "";
      tecDriverName.text = "";
      truckInDoc = "";
      _isReadonly = false;
      _isCheck = null;
      _isSuccess = false;
    });
  }

  getData() async {
    baseUrl = await AppPrefs.getAppServer();
    String lookUp = await AppPrefs.getLookup();

    listHaulier = json.decode(json.decode(lookUp))['HaulierCode'];
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData media = MediaQuery.of(context);
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);
    final Size screenSize = media.size;
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(
          color: Colors.white,
        ),
        title: Text(
          "PICK",
          style: AppTheme.title_white,
        ),
        actions: <Widget>[
          InkWell(
              onTap: () {
                reset();
              },
              child: Icon(
                Icons.restore_page_outlined,
                color: Colors.white,
                size: 30,
              )),
          SizedBox(
            width: 16,
          ),
        ],
      ),
      body: new Form(
        key: this._formKey_searchList,
        child: SingleChildScrollView(
          controller: _scrollController,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 10,
                ),
                truckInDoc.length > 0
                    ? Padding(
                        padding: const EdgeInsets.only(bottom: 20.0),
                        child: Row(
                          children: [
                            Container(
                              color: Colors.black26,
                              child: Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Text(
                                  truckInDoc.replaceAll("\"", ""),
                                  maxLines: 1,
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Expanded(
                                child: Row(
                              children: [
                                GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (_) => TruckImage(
                                                    docNo: truckInDoc
                                                        .replaceAll("\"", ""),
                                                    containerNo:
                                                        tecContainerNo.text,
                                                    docType: "TI",
                                                  )));
                                    },
                                    child: Image.asset(
                                      'assets/images/camera.png',
                                      height: 30,
                                      width: 30,
                                    )),
                              ],
                            )),
                          ],
                        ),
                      )
                    : SizedBox(),
                Focus(
                  onFocusChange: (hasFocus) {
                    if (!hasFocus) {
                      if (tecCustomerRef.text != _data._customerRef) {
                        _submitBLCheck();
                        _data._customerRef = tecCustomerRef.text;
                      }
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: TextFormField(
                      controller: tecCustomerRef,
                      keyboardType: TextInputType.text,
                      focusNode: focusNode,
                      textCapitalization: TextCapitalization.characters,
                      // Use email input type for emails.
                      // inputFormatters: [
                      //   UpperCaseTextFormatter(),
                      // ],
                      validator: (value) {
                        return null;
                      },
                      maxLength:50,
                      decoration: new InputDecoration(
                          suffixIcon: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              _isCheck != null
                                  ? _isCheck
                                      ? Icon(
                                          Icons.check,
                                          color: AppTheme.colorPrimary,
                                        )
                                      : Icon(
                                          Icons.clear,
                                          color: Colors.red,
                                        )
                                  : SizedBox(),
                              GestureDetector(
                                  onTap: () {
                                    _scanQR(tecCustomerRef);
                                  },
                                  child: Image.asset(
                                    "assets/images/ic_qr.png",
                                    height: 35,
                                  )),
                              GestureDetector(
                                  onTap: () {
                                    _read(tecCustomerRef);
                                  },
                                  child: Image.asset(
                                    "assets/images/ic_ocr.png",
                                    height: 35,
                                  )),
                            ],
                          ),
                          labelText: "Booking Ref* :",
                          labelStyle:
                              TextStyle(fontSize: 18, color: Colors.black38),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorPrimary, width: 1.0),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorMediumGrey, width: 1.0),
                          ),
                          border: new OutlineInputBorder(),
                          counterText: ""),
                      autofocus: false,
                      onSaved: (value) {
                        this._data._customerRef = value;
                      },
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: TextFormField(
                    controller: tecOperator,
                    keyboardType: TextInputType.text,
                    // Use email input type for emails.
                    textCapitalization: TextCapitalization.characters,
                    //
                    // inputFormatters: [
                    //   UpperCaseTextFormatter(),
                    // ],
                    maxLength: 20,
                    readOnly: true,
                    validator: (value) {
                      if (value.length <= 0) {
                        return "Operator Code cannot be empty.";
                      }
                      return null;
                    },
                    decoration: new InputDecoration(
                        disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: AppTheme.colorMediumGrey, width: 1.0),
                        ),
                        labelText: "Operator Code :",
                        labelStyle:
                            TextStyle(fontSize: 18, color: Colors.black38),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: AppTheme.colorPrimary, width: 1.0),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: AppTheme.colorMediumGrey, width: 1.0),
                        ),
                        border: new OutlineInputBorder(),
                        errorBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Theme.of(context).errorColor,
                              width: 1.0),
                        ),
                        errorStyle: TextStyle(
                          color: Theme.of(context)
                              .errorColor, // or any other color
                        ),
                        counterText: ""),
                    autofocus: false,
                    onSaved: (value) {
                      this._data._operatorCode = value;
                    },
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    _showModal(context, 0, _listOperator);
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: Stack(
                      children: [
                        TextFormField(
                          controller: tecPrimeMoverRegNo,
                          keyboardType: TextInputType.text,
                          // Use email input type for emails.
                          readOnly: true,
                          maxLength: 20,
                          validator: (value) {
                            if (value.length <= 0) {
                              return "Prime mover reg no. cannot be empty.";
                            }
                            return null;
                          },
                          textCapitalization: TextCapitalization.characters,
                          //
                          // inputFormatters: [
                          //   UpperCaseTextFormatter(),
                          // ],
                          decoration: new InputDecoration(
                              labelText: "Prime Mover Reg No :",
                              labelStyle: TextStyle(
                                  fontSize: 18, color: Colors.black38),
                              disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: AppTheme.colorMediumGrey,
                                    width: 1.0),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: AppTheme.colorPrimary, width: 1.0),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: AppTheme.colorMediumGrey,
                                    width: 1.0),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context).errorColor,
                                    width: 1.0),
                              ),
                              errorStyle: TextStyle(
                                color: Theme.of(context)
                                    .errorColor, // or any other color
                              ),
                              border: new OutlineInputBorder(),
                              counterText: ""),
                          enabled: false,
                          onSaved: (value) {
                            this._data._primeMoverRegNo = value;
                          },
                        ),
                        !_isReadonly
                            ? Positioned(
                                right: 12,
                                top: 12,
                                bottom: 12,
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    GestureDetector(
                                        onTap: () {
                                          _read(tecPrimeMoverRegNo);
                                        },
                                        child: Image.asset(
                                          "assets/images/ic_ocr.png",
                                          height: 35,
                                        )),
                                  ],
                                ),
                              )
                            : SizedBox(),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: TextFormField(
                    controller: tecPrimeMoverId,
                    keyboardType: TextInputType.text,
                    // Use email input type for emails.
                    // inputFormatters: [
                    //   UpperCaseTextFormatter(),
                    // ],
                    textCapitalization: TextCapitalization.characters,
                    maxLength: 20,
                    decoration: new InputDecoration(
                        disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: AppTheme.colorMediumGrey, width: 1.0),
                        ),
                        labelText: "Prime Mover Id :",
                        labelStyle:
                            TextStyle(fontSize: 18, color: Colors.black38),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: AppTheme.colorPrimary, width: 1.0),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: AppTheme.colorMediumGrey, width: 1.0),
                        ),
                        border: new OutlineInputBorder(),
                        counterText: ""),
                    autofocus: false,
                    onSaved: (value) {
                      this._data._primeMoverId = value;
                    },
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    _showModalSearch(context);
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: TextFormField(
                      controller: tecHaulier,
                      readOnly: true,
                      enabled: false,
                      keyboardType: TextInputType.text,
                      validator: (value) {
                        if (value.length <= 0) {
                          return "Haulier Code cannot be empty.";
                        }
                        return null;
                      },
                      textCapitalization: TextCapitalization.characters,

                      // Use email input type for emails.
                      // inputFormatters: [
                      //   UpperCaseTextFormatter(),
                      // ],
                      maxLength: 20,
                      decoration: new InputDecoration(
                          disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorMediumGrey, width: 1.0),
                          ),
                          labelText: "Haulier Code :",
                          labelStyle:
                              TextStyle(fontSize: 18, color: Colors.black38),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorPrimary, width: 1.0),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorMediumGrey, width: 1.0),
                          ),
                          errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Theme.of(context).errorColor,
                                width: 1.0),
                          ),
                          errorStyle: TextStyle(
                            color: Theme.of(context)
                                .errorColor, // or any other color
                          ),
                          border: new OutlineInputBorder(),
                          counterText: ""),
                      autofocus: false,
                      onSaved: (value) {
                        this._data._haulier = value;
                      },
                    ),
                  ),
                ),
                Focus(
                  onFocusChange: (focus){
                    if(!focus){
                      if(_data._driverIC != tecDriverIC.text) {
                        _submitDriverSearch();
                        _data._driverIC = tecDriverIC.text;
                      }
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: TextFormField(
                      controller: tecDriverIC,
                      keyboardType: TextInputType.number,
                      // Use email input type for emails.
                      // inputFormatters: [
                      //   UpperCaseTextFormatter(),
                      // ],
                      textCapitalization: TextCapitalization.characters,
                      maxLength: 12,
                      decoration: new InputDecoration(
                          labelText: "Driver IC :",
                          labelStyle:
                              TextStyle(fontSize: 18, color: Colors.black38),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorPrimary, width: 1.0),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorMediumGrey, width: 1.0),
                          ),
                          border: new OutlineInputBorder(),
                          counterText: ""),
                      autofocus: false,
                      onSaved: (value) {
                        this._data._driverIC = value;
                      },
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: TextFormField(
                    controller: tecDriverName,
                    maxLength: 100,
                    keyboardType: TextInputType.text,
                    // Use email input type for emails.
                    // inputFormatters: [UpperCaseTextFormatter()],
                    textCapitalization: TextCapitalization.characters,
                    decoration: new InputDecoration(
                        labelText: "Driver Name :",
                        labelStyle:
                            TextStyle(fontSize: 18, color: Colors.black38),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: AppTheme.colorPrimary, width: 1.0),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: AppTheme.colorMediumGrey, width: 1.0),
                        ),
                        border: new OutlineInputBorder(),
                        counterText: ""),
                    autofocus: false,
                    onSaved: (value) {
                      this._data._driverName = value;
                    },
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    if (!_isReadonly) _showModal(context, 1, _listTypeSize);
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: TextFormField(
                      controller: tecTypeSize,
                      enabled: false,
                      readOnly: true,
                      keyboardType: TextInputType.text,
                      validator: (value) {
                        if (value.length <= 0) {
                          return "Type Size cannot be empty.";
                        }
                        return null;
                      },
                      // Use email input type for emails.
                      // inputFormatters: [
                      //   UpperCaseTextFormatter(),
                      // ],
                      textCapitalization: TextCapitalization.characters,
                      maxLength: 20,
                      decoration: new InputDecoration(
                          disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorMediumGrey, width: 1.0),
                          ),
                          labelText: "Type Size :",
                          labelStyle:
                              TextStyle(fontSize: 18, color: Colors.black38),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorPrimary, width: 1.0),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorMediumGrey, width: 1.0),
                          ),
                          border: new OutlineInputBorder(),
                          errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Theme.of(context).errorColor,
                                width: 1.0),
                          ),
                          errorStyle: TextStyle(
                            color: Theme.of(context)
                                .errorColor, // or any other color
                          ),
                          counterText: ""),
                      autofocus: false,
                      onSaved: (value) {
                        this._data._typeSize = value;
                      },
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: Focus(
                    onFocusChange: (hasFocus){
                      if(hasFocus){
                        setState(() {
                          _isRemarkFocused = true;
                        });
                      }else{
                        setState(() {
                          _isRemarkFocused = false;
                        });
                      }
                    },
                    child: TextFormField(
                      controller: tecRemark,
                      keyboardType: TextInputType.multiline,
                      // Use email input type for emails.
                      // inputFormatters: [
                      //   UpperCaseTextFormatter(),
                      // ],
                      textCapitalization: TextCapitalization.characters,

                      maxLines: 5,
                      minLines: 1,
                      // textInputAction: TextInputAction.newline,
                      decoration: new InputDecoration(
                          labelText: "Remark :",
                          labelStyle:
                          TextStyle(fontSize: 18, color: Colors.black38),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorPrimary, width: 1.0),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.colorMediumGrey, width: 1.0),
                          ),
                          border: new OutlineInputBorder(),
                          counterText: ""),
                      autofocus: false,
                      onSaved: (value) {
                        this._data._remark = value;
                      },
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    if (_isReadonly) {
                      listContainerFiltered = [];
                      for (int i = 0; i < listContainerWidget.length; i++) {
                        if (listContainerWidget[i]["TypeSize"] ==
                            tecTypeSize.text) {
                          listContainerFiltered.add(listContainerWidget[i]);
                        }
                      }
                      print("here");
                      _showModal2(context, listContainerFiltered);
                    } else {
                      print("here2");

                      _showModal2(context, listContainerWidget);
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: Stack(
                      children: [
                        TextFormField(
                          enabled: false,
                          readOnly: true,
                          controller: tecContainerNo,
                          keyboardType: TextInputType.text,
                          // Use email input type for emails.
                          maxLength: 11,
                          // inputFormatters: [
                          //   UpperCaseTextFormatter(),
                          // ],
                          textCapitalization: TextCapitalization.characters,

                          decoration: new InputDecoration(
                              labelText: "Container No :",
                              labelStyle: TextStyle(
                                  fontSize: 18, color: Colors.black38),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: AppTheme.colorPrimary, width: 1.0),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: AppTheme.colorMediumGrey,
                                    width: 1.0),
                              ),
                              border: new OutlineInputBorder(),
                              disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: AppTheme.colorMediumGrey,
                                    width: 1.0),
                              ),
                              counterText: ""),

                          autofocus: false,
                          onSaved: (value) {
                            this._data._containerNo = value;
                          },
                        ),
                        !_isReadonly
                            ? Positioned(
                                right: 12,
                                top: 12,
                                bottom: 12,
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    GestureDetector(
                                        onTap: () {
                                          _read(tecContainerNo);
                                        },
                                        child: Image.asset(
                                          "assets/images/ic_ocr.png",
                                          height: 35,
                                        )),
                                  ],
                                ),
                              )
                            : SizedBox(),
                      ],
                    ),
                  ),
                ),
                !_isSuccess
                    ? Padding(
                        padding: const EdgeInsets.only(top: 16.0, bottom: 16),
                        child: SizedBox(
                          width: 150,
                          height: 50,
                          child: ElevatedButton(
                            child: Text(
                              "Submit",
                              style: TextStyle(color: Colors.white),
                            ),
                            onPressed: () {
                              if (_isCheck != null && _isCheck) {
                                _submitTruckIn();
                              } else {
                                setState(() {
                                  _isCheck = false;
                                  _scrollController.animateTo(0,
                                      duration: Duration(milliseconds: 300),
                                      curve: Curves.ease);
                                  focusNode.requestFocus();
                                  Fluttertoast.showToast(
                                      msg: "Invalid Booking Ref.",
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.CENTER,
                                      timeInSecForIosWeb: 1,
                                      backgroundColor: Colors.red,
                                      textColor: Colors.white,
                                      fontSize: 16.0
                                  );
                                });
                              }
                            },
                          ),
                        ),
                      )
                    : Padding(
                        padding: const EdgeInsets.only(top: 16.0, bottom: 16),
                        child: SizedBox(
                          width: 150,
                          height: 50,
                          child: ElevatedButton(
                            child: Text(
                              "Submit",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                      ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _showModal(BuildContext context, int type, List<OperatorCode> list) {
    FocusScope.of(context).unfocus();

    showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(15.0)),
        ),
        context: context,
        builder: (context) {
          //3
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
            return DraggableScrollableSheet(
                expand: false,
                builder:
                    (BuildContext context, ScrollController scrollController) {
                  return Column(children: <Widget>[
                    Padding(
                        padding: EdgeInsets.all(8),
                        child: Row(children: <Widget>[
                          Expanded(
                              child: TextFormField(
                                  initialValue:
                                      type == 0 ? tecPrimeMoverRegNo.text : "",
                                  enabled: type == 0 ? true : false,
                                  autofocus: list.length > 0 ? false : true,
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.all(8),
                                    border: new OutlineInputBorder(
                                      borderRadius:
                                          new BorderRadius.circular(15.0),
                                      borderSide: new BorderSide(),
                                    ),
                                  ),
                                  // inputFormatters: [UpperCaseTextFormatter()],
                                  textCapitalization: TextCapitalization.characters,
                                  onFieldSubmitted: (value) {
                                    //4
                                    setState(() {
                                      tecPrimeMoverRegNo.text = value;
                                      _submitPrimeSearch();
                                      Navigator.of(context).pop();
                                    });
                                  })),
                        ])),
                    Expanded(
                      child: ListView.separated(
                          controller: scrollController,
                          //5
                          itemCount: list.length,
                          separatorBuilder: (context, int) {
                            return Divider();
                          },
                          itemBuilder: (context, index) {
                            return InkWell(
                                child: type == 0
                                    ? _showBottomSheet(index, list, type)
                                    : _showBottomSheet(index, list, type),
                                onTap: () {
                                  if (type == 1) {
                                    tecContainerNo.text = "";
                                    tecOperator.text = list[index].operatorCode;
                                    tecTypeSize.text = list[index].typeSize;
                                  } else {
                                    tecPrimeMoverRegNo.text =
                                        list[index].primeMoverRegNo;
                                    tecPrimeMoverId.text =
                                        list[index].primeMoverId;
                                    tecDriverIC.text = list[index].driverICNo;
                                    tecDriverName.text = list[index].driverName;
                                    tecHaulier.text = list[index].haulierCode;
                                    tecOperator.text = list[index].operatorCode;
                                    tecTypeSize.text = list[index].typeSize;
                                  }
                                  Navigator.of(context).pop();
                                });
                          }),
                    )
                  ]);
                });
          });
        });
  }

  Widget _showBottomSheet(int index, List<OperatorCode> listAM, int type) {
    if (type == 0) {
      return Text(listAM[index].primeMoverRegNo,
          style: TextStyle(color: Colors.black, fontSize: 16),
          textAlign: TextAlign.center);
    } else {
      print("here");
      return Text(listAM[index].operatorCode + " - " + listAM[index].typeSize,
          style: TextStyle(color: Colors.black, fontSize: 16),
          textAlign: TextAlign.center);
    }
  }

  void _showModal2(BuildContext context, List list) {
    FocusScope.of(context).unfocus();
    showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(15.0)),
        ),
        context: context,
        builder: (context) {
          //3
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
            return DraggableScrollableSheet(
                expand: false,
                builder:
                    (BuildContext context, ScrollController scrollController) {
                  return Column(children: <Widget>[
                    Padding(
                        padding: EdgeInsets.all(8),
                        child: Row(children: <Widget>[
                          Expanded(
                              child: TextFormField(
                                  initialValue: tecContainerNo.text,
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.all(8),
                                    border: new OutlineInputBorder(
                                      borderRadius:
                                          new BorderRadius.circular(15.0),
                                      borderSide: new BorderSide(),
                                    ),
                                  ),
                                  readOnly: true,
                                  autofocus: list.length > 0 ? false : true,
                                  // inputFormatters: [UpperCaseTextFormatter()],
                                  textCapitalization: TextCapitalization.characters,
                                  onFieldSubmitted: (value) {
                                    //4
                                    setState(() {
                                      tecContainerNo.text = value;
                                      Navigator.of(context).pop();
                                    });
                                  })),
                        ])),
                    Expanded(
                      child: ListView.separated(
                          controller: scrollController,
                          //5
                          itemCount: list.length,
                          separatorBuilder: (context, int) {
                            return Divider();
                          },
                          itemBuilder: (context, index) {
                            return InkWell(
                                child: _showBottomSheetWithSearch2(index, list),
                                onTap: () {
                                  tecContainerNo.text =
                                      list[index]["ContainerNo"];
                                  tecTypeSize.text = list[index]["TypeSize"];
                                  Navigator.of(context).pop();
                                });
                          }),
                    )
                  ]);
                });
          });
        });
  }

  Widget _showBottomSheetWithSearch2(int index, List listAM) {
    return Text(
        listAM[index]["ContainerNo"] + " - " + listAM[index]["TypeSize"],
        style: TextStyle(color: Colors.black, fontSize: 16),
        textAlign: TextAlign.center);
  }

  void _showModalSearch(context) {
    showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(15.0)),
        ),
        context: context,
        builder: (context) {
          //3
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
            return DraggableScrollableSheet(
                expand: false,
                builder:
                    (BuildContext context, ScrollController scrollController) {
                  return Column(children: <Widget>[
                    Padding(
                        padding: EdgeInsets.all(8),
                        child: Row(children: <Widget>[
                          Expanded(
                              child: TextField(
                                  controller: tecSearch,
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.all(8),
                                    border: new OutlineInputBorder(
                                      borderRadius:
                                          new BorderRadius.circular(15.0),
                                      borderSide: new BorderSide(),
                                    ),
                                    prefixIcon: Icon(Icons.search),
                                  ),
                                  onChanged: (value) {
                                    //4
                                    setState(() {
                                      tempListHaulier = _buildSearchList(value);
                                    });
                                  })),
                        ])),
                    Expanded(
                      child: ListView.separated(
                          controller: scrollController,
                          //5
                          itemCount: (tempListHaulier != null &&
                                  tempListHaulier.length > 0)
                              ? tempListHaulier.length
                              : listHaulier.length,
                          separatorBuilder: (context, int) {
                            return Divider();
                          },
                          itemBuilder: (context, index) {
                            return InkWell(
                                child: (tempListHaulier != null &&
                                        tempListHaulier.length > 0)
                                    ? _showBottomSheetWithSearch(
                                        index, tempListHaulier)
                                    : _showBottomSheetWithSearch(
                                        index, listHaulier),
                                onTap: () {
                                  if (tempListHaulier.length > 0) {
                                    tecHaulier.text =
                                        tempListHaulier[index]['Key'];
                                    _data._haulier =
                                        tempListHaulier[index]['Key'];
                                    print(
                                        "id =  ${tempListHaulier[index]['Key']}");
                                  } else {
                                    tecHaulier.text = listHaulier[index]['Key'];
                                    _data._haulier = listHaulier[index]['Key'];
                                    print("id =  ${listHaulier[index]['Key']}");
                                  }
                                  Navigator.of(context).pop();
                                });
                          }),
                    )
                  ]);
                });
          });
        });
  }

  Widget _showBottomSheetWithSearch(int index, List listAM) {
    return Text(listAM[index]["Value"],
        style: TextStyle(color: Colors.black, fontSize: 16),
        textAlign: TextAlign.center);
  }

  List _buildSearchList(String userSearchTerm) {
    List _searchList = [];

    for (int i = 0; i < listHaulier.length; i++) {
      String name = listHaulier[i]["Value"];
      if (name.toLowerCase().contains(userSearchTerm.toLowerCase())) {
        _searchList.add(listHaulier[i]);
      }
    }
    return _searchList;
  }

  Future<String> _scanQR(TextEditingController tec) async {
    try {
      var qrResult = await BarcodeScanner.scan();
      result = qrResult.rawContent;
      tec.text = result.toUpperCase();
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.cameraAccessDenied) {
        setState(() {
          result = "Camera permission was denied";
          Fluttertoast.showToast(
              msg: result,
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0
          );        });
      } else {
        setState(() {
          result == "Unknown Error $e";
          //Toast.show(result, context, duration: 2);
        });
      }
    } on FormatException {
      setState(() {
        result = "Cancelled";
        Fluttertoast.showToast(
            msg: result,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );      });
    } catch (e) {
      setState(() {
        result == "Unknown Error $e";
        //Toast.show(result, context, duration: 2);
      });
    }
  }

  void _submitBLCheck() async {
    // Save our form now.
    FocusScope.of(context).unfocus();
    Map dataValue = {"Key": "BookingRef", "Value": tecCustomerRef.text};

    List list = [];
    list.add(json.encode(dataValue));

    Map data = {'Key': 'Data', 'Value': "$list"};

    final response = await _httpHelper.post(
        baseUrl, UrlConfig.TRUCK_IN_BL_CHECK, json.encode(data));
    Response res = Response.fromJson(response);
    print("PKM" + res.statusCode.toString());
//      pr.hide();
    if (res.statusCode == 200) {
      if (res.results.length > 0) {
        print("RESULT -- " + res.results[0].value);
        var tempResult = json.decode(res.results[0].value);

        if (tempResult == null || tempResult == "") {
          _submitTruckInSearch();
        } else {
          setState(() {
            _isCheck = false;
          });
          showDialog<void>(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('BlackListed'),
                content: SingleChildScrollView(
                  child: ListBody(
                    children: <Widget>[
                      const Text(
                        'tempResult',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                    ],
                  ),
                ),
                actions: <Widget>[
                  FlatButton(
                    child: const Text('Ok'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              );
            },
          );
        }
      }
    } else if (res.statusCode == 400) {
      setState(() {
        _isCheck = false;
        _data = new _TruckInData();
        tecContainerNo.text = "";
        tecPrimeMoverId.text = "";
        tecPrimeMoverRegNo.text = "";
        tecDriverIC.text = "";
        tecRemark.text = "";
        tecDriverName.text = "";
        tecHaulier.text = "";
        tecOperator.text = "";
        tecTypeSize.text = "";
        truckInDoc = "";
        _isSuccess = false;
      });
      showDialog<void>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(res.errorDetail),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Text(
                    res.errorMessage,
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: const Text('Ok'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    } else {
      Fluttertoast.showToast(
          msg: res.errorMessage,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );
      setState(() {
        _isCheck = false;
        _data = new _TruckInData();
        tecContainerNo.text = "";
        tecPrimeMoverId.text = "";
        tecPrimeMoverRegNo.text = "";
        tecDriverIC.text = "";
        tecRemark.text = "";
        tecDriverName.text = "";
        tecHaulier.text = "";
        tecOperator.text = "";
        tecTypeSize.text = "";
        truckInDoc = "";
        _isSuccess = false;
        FocusScope.of(context).unfocus();
      });

      if (res.errorMessage.contains("AccessToken"))
        new force_logout(context).doForceLogout();
    }
  }

  void _submitPrimeSearch() async {
    // Save our form now.
    Map dataValue = {
      "Key": "PrimeMoverRegNo",
      "Value": tecPrimeMoverRegNo.text
    };

    List list = [];
    list.add(json.encode(dataValue));

    Map data = {'Key': 'Data', 'Value': "$list"};

    final response = await _httpHelper.post(
        baseUrl, UrlConfig.GET_PRIME_HAULIER, json.encode(data));
    Response res = Response.fromJson(response);
    print("PKM" + res.statusCode.toString());
//      pr.hide();
    if (res.statusCode == 200) {
      Map<String, dynamic> tempResult = json.decode(res.results[0].value);
      if (tempResult['Result'] != null && tempResult['Result'] != "null") {
        var listResult = json.decode(tempResult['Result']);
        tecPrimeMoverId.text = listResult[0]['PrimeMoverId'];
        tecHaulier.text = listResult[0]['HaulierCode'];
      } else {
        tecPrimeMoverId.text = "";
        tecHaulier.text = "";
      }
    } else {
      Fluttertoast.showToast(
          msg: res.errorMessage,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );
      setState(() {
        FocusScope.of(context).unfocus();
      });

      if (res.errorMessage.contains("AccessToken"))
        new force_logout(context).doForceLogout();
    }
  }

  void _submitDriverSearch() async {
    // Save our form now.
    FocusScope.of(context).unfocus();
    if(tecDriverIC.text.length < 12){
      Fluttertoast.showToast(
          msg: "Invalid Driver IC number",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );
    }else {
      Map dataValue = {
        "Key": "DriverIC",
        "Value": tecDriverIC.text
      };

      List list = [];
      list.add(json.encode(dataValue));

      Map data = {'Key': 'Data', 'Value': "$list"};

      final response = await _httpHelper.post(
          baseUrl, UrlConfig.GET_DRIVER_NAME, json.encode(data));
      Response res = Response.fromJson(response);
      print("PKM" + res.statusCode.toString());
//      pr.hide();
      if (res.statusCode == 200) {
        Map<String, dynamic> tempResult = json.decode(res.results[0].value);
        if (tempResult['Result'] != null && tempResult['Result'] != "null") {
          var listResult = json.decode(tempResult['Result']);
          tecDriverName.text = listResult[0]['DriverName'];
        } else {
          tecDriverName.text = "";
        }
      } else {
        Fluttertoast.showToast(
            msg: res.errorMessage,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );

        setState(() {
          FocusScope.of(context).unfocus();
        });

        if (res.errorMessage.contains("AccessToken"))
          new force_logout(context).doForceLogout();
      }
    }
  }

  void _submitTruckInSearch() async {
    // Save our form now.
    FocusScope.of(context).unfocus();
    Map dataValue = {"Key": "BookingRef", "Value": tecCustomerRef.text};

    List list = [];
    list.add(json.encode(dataValue));

    Map data = {'Key': 'Data', 'Value': "$list"};

    final response = await _httpHelper.post(
        baseUrl, UrlConfig.TRUCK_IN_CHECK, json.encode(data));
    Response res = Response.fromJson(response);
    print("PKM" + res.statusCode.toString());
//      pr.hide();
    if (res.statusCode == 200) {
      if (res.results.length > 0) {
        print("RESULT -- " + res.results[0].value);
        setState(() {
          _isCheck = true;
        });
        Map<String, dynamic> tempResult = json.decode(res.results[0].value);

        List tempList = json.decode(tempResult['OperatorCode'].replaceAll("\n","\\n"));
        _listOperator = [];
        if (tempList.length > 0) {
          for (int i = 0; i < tempList.length; i++) {
            OperatorCode operatorCode = OperatorCode.fromJson(tempList[i]);
            if (operatorCode.primeMoverRegNo.length > 0) {
              _listOperator.add(operatorCode);
            }

            if (operatorCode.typeSize.length > 0) {
              _listTypeSize.add(operatorCode);
            }
          }
        }

        tecOperator.text = tempList[0]['OperatorCode'];
        tecPrimeMoverRegNo.text = tempList[0]['PrimeMoverRegNo'];
        tecPrimeMoverId.text = tempList[0]['PrimeMoverId'];
        tecTypeSize.text = tempList[0]['TypeSize'];
        tecHaulier.text = tempList[0]['HaulierCode'];
        tecDriverName.text = tempList[0]['DriverName'];
        tecDriverIC.text = tempList[0]['DriverICNo'];
        tecRemark.text = tempList[0]['Remarks'];

        if (tempList[0]['PrimeMoverRegNo'].length > 0) {
          _isReadonly = true;
        } else {
          _isReadonly = false;
        }

        if (tempResult['ContainerList'] != null) {
          List tempListContainer = json.decode(tempResult['ContainerList']);
          listContainerWidget.clear();
          if (tempListContainer.length > 0) {
            for (int i = 0; i < tempListContainer.length; i++) {
              listContainerWidget.add(tempListContainer[i]);
            }
          }
        } else {
          tecContainerNo.text = "";

          truckInDoc = "";
          _isSuccess = false;
          FocusScope.of(context).unfocus();
        }
      } else {
        if (res.errorMessage.contains("AccessToken"))
          new force_logout(context).doForceLogout();

        Fluttertoast.showToast(
            msg: res.errorMessage,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
        setState(() {
          listContainer = [];
          FocusScope.of(context).unfocus();
        });
      }
    }
  }

  void _submitTruckIn() async {
    if (_formKey_searchList.currentState.validate()) {
      // Save our form now.
      _formKey_searchList.currentState.save();

      Map dataValue = {
        "OperatorCode": _data._operatorCode,
        "HaulierCode": _data._haulier,
        "TypeSize": _data._typeSize,
        "CustomerRef": _data._customerRef,
        "ContainerNo": _data._containerNo,
        "PrimeMoverId": _data._primeMoverId,
        "PrimeMoverRegNo": _data._primeMoverRegNo,
        "DriverIc": _data._driverIC,
        "DriverName": _data._driverName,
        "Remarks": _data._remark
      };

      List list = [];
      list.add(json.encode(dataValue));

      Map data = {'Key': 'Data', 'Value': "${json.encode(dataValue)}"};

      final response = await _httpHelper.post(
          baseUrl, UrlConfig.TRUCK_IN, json.encode(data));
      Response res = Response.fromJson(response);
      print("PKM" + res.statusCode.toString());
//      pr.hide();
      if (res.statusCode == 200) {
        FocusScope.of(context).unfocus();
        if (res.results.length > 0) {
          print("RESULT -- " + res.results[0].value);
          if (res.results[0].value != null) {
            setState(() {
              truckInDoc = res.results[0].value;
              _scrollController.animateTo(0,
                  duration: Duration(milliseconds: 1000), curve: Curves.ease);
              _isSuccess = true;
            });
            showDialog<void>(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text('Success'),
                  content: SingleChildScrollView(
                    child: ListBody(
                      children: <Widget>[
                        const Text(
                          'Truck In Doc No. :',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: new Text(
                            res.results[0].value
                                .toString()
                                .replaceAll('\"', ""),
                            style: TextStyle(
                              fontSize: 20,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  actions: <Widget>[
                    FlatButton(
                      child: const Text('Ok'),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                );
              },
            );
          } else {
            Fluttertoast.showToast(
                msg: res.errorMessage,
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.CENTER,
                timeInSecForIosWeb: 1,
                backgroundColor: Colors.red,
                textColor: Colors.white,
                fontSize: 16.0
            );
            Fluttertoast.showToast(
                msg: "No result.",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.CENTER,
                timeInSecForIosWeb: 1,
                backgroundColor: Colors.red,
                textColor: Colors.white,
                fontSize: 16.0
            );

          }
        }
      } else {
        if (res.errorMessage.contains("AccessToken"))
          new force_logout(context).doForceLogout();

        Fluttertoast.showToast(
            msg: res.errorMessage,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
        setState(() {
          FocusScope.of(context).unfocus();
        });
      }
    }
  }

  Widget txtField(
      String lbl,
      TextInputType type,
      int charCount,
      List<TextInputFormatter> inputFormat,
      List<FormFieldValidator> inputValidator,
      TextEditingController textEditingController) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: TextFormField(
        controller: textEditingController,
        keyboardType: type,
        // Use email input type for emails.
        inputFormatters: inputFormat,
        maxLength: 10,
        decoration: new InputDecoration(
            labelText: lbl,
            labelStyle: TextStyle(fontSize: 18),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: AppTheme.colorPrimary, width: 1.0),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide:
                  BorderSide(color: AppTheme.colorMediumGrey, width: 1.0),
            ),
            border: new OutlineInputBorder(),
            counterText: ""),
        autofocus: false,
      ),
    );
  }
}

class ContainerListView extends StatelessWidget {
  final VoidCallback callback;
  final ContainerModel post;

  const ContainerListView({Key key, this.post, this.callback})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Padding(
      padding: const EdgeInsets.only(left: 24, right: 24, top: 8, bottom: 8),
      child: InkWell(
        splashColor: Colors.transparent,
        onTap: () {
          callback();
        },
        child: Card(
          elevation: 3.0,
          child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(12.0)),
              child: Container(
                height: 100,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(4)),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                        child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                              height: 40,
                              child:
                                  Image.asset("assets/images/container.png")),
                          SizedBox(
                            width: 12,
                          ),
                          Expanded(
                            child: Column(
                              children: <Widget>[
                                Expanded(
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                          child: Text(
                                        post.containerNo,
                                        style: AppTheme.title,
                                      )),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                          child: Text(
                                        post.typeSize,
                                        style: AppTheme.titleDesc,
                                      )),
                                      Expanded(
                                          child: Text(post.operatorCode,
                                              style: AppTheme.titleDesc,
                                              textAlign: TextAlign.end)),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                          child: Text(
                                        DateFormat("dd-MM-yyyy").format(
                                            DateTime.parse(post.pickDropTime)),
                                        style: AppTheme.titleDesc,
                                        maxLines: 1,
                                      )),
                                      Text(
                                        DateFormat("HH : mm").format(
                                            DateTime.parse(post.pickDropTime)),
                                        style: AppTheme.titleDesc,
                                        textAlign: TextAlign.end,
                                        maxLines: 1,
                                      ),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                          child: Text(
                                        post.primeMoverId,
                                        style: AppTheme.titleDesc,
                                        maxLines: 1,
                                      )),
                                      Text(
                                        post.primeMoverRegNo,
                                        style: AppTheme.titleDesc,
                                        textAlign: TextAlign.end,
                                        maxLines: 1,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 30,
                          ),
                          !post.isFileAttached
                              ? Container(
                                  height: 100,
                                  child: Center(
                                      child: Container(
                                          height: 60,
                                          child: Image.asset(
                                              "assets/images/camera.png"))))
                              : Container(
                                  height: 100,
                                  child: Center(
                                      child: Container(
                                          height: 60,
                                          child: Image.asset(
                                              "assets/images/camera_taken.png")))),
                        ],
                      ),
                    )),
                  ],
                ),
              )),
        ),
      ),
    ));
  }

  timeFormater(String lala) {
    String dateFormate =
        DateFormat("dd-MM-yyyy").format(DateTime.parse(post.pickDropTime));
  }
}
