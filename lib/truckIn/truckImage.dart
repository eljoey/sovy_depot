import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:depot/models/response.dart';
import 'package:depot/styles/appTheme.dart';
import 'package:depot/utils/ApiBaseHelper.dart';
import 'package:depot/utils/AppPrefs.dart';
import 'package:depot/utils/UrlConfig.dart';
import 'package:depot/utils/force_logout.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'dart:ui';
import 'dart:io' as Io;
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:path_provider/path_provider.dart';

class TruckImage extends StatefulWidget {
  String docNo;
  String docType;
  String containerNo;

  TruckImage({Key key, this.docNo,this.containerNo,this.docType})
      : super(key: key);

  @override
  _TruckImageState createState() => _TruckImageState();
}

class _TruckImageState extends State<TruckImage> {
  List<Asset> images = List<Asset>();
  List<ByteData> byts = List<ByteData>();
  List<ByteData> byts2 = List<ByteData>();
  List<ByteData> tempCamera = List<ByteData>();
  final GlobalKey<FormState> _formKey_photoList = new GlobalKey<FormState>();
  TextEditingController remark = new TextEditingController();
  String _error = 'No Error Dectected';
  String prefSaved = "";
  String toSaved = "";
  ApiBaseHelper _httpHelper = ApiBaseHelper();
  String baseUrl;
  final ImagePicker _picker = ImagePicker();

  @override
  void initState() {
    getPref();
    super.initState();
  }

  getPref() async{
    baseUrl = await AppPrefs.getAppServer();
  }

  void _uploadImage(List<String> listString) async {
    Map dataKey;
    List list = [];

    for(int i = 0; i<listString.length;i++) {
      dataKey = {
        "DocType": widget.docType,
        "FileType": "jpg",
        "FileContent": listString[i],
        "DocumentNo": widget.docNo,
        "ContainerNo": widget.containerNo,
      };
      list.add(json.encode(dataKey));
    }


    Map data = {'Key': 'Data', 'Value': "$list"};

    final response = await _httpHelper.post(
        baseUrl, UrlConfig.APP_UPLOAD_IMAGES, json.encode(data));
    Response res = Response.fromJson(response);
    print("PKM" + res.statusCode.toString());
    if (res.statusCode == 200) {
      if (res.results.length > 0) {
        Fluttertoast.showToast(
            msg: "Success upload image to ${widget.docNo}.",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
        Navigator.pop(context, "success");
      }
    } else {
      if(res.errorMessage.length>0) {
        if(res.errorMessage.contains("AccessToken"))
          new force_logout(context).doForceLogout();

        Fluttertoast.showToast(
            msg: res.errorMessage,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
      }else{
        if(response['message'].contains("AccessToken"))
          new force_logout(context).doForceLogout();

        Fluttertoast.showToast(
            msg: response['message'],
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
      }
    }
  }


  showAlertDialog(BuildContext context) {
    Widget optionCamera = SimpleDialogOption(
      child: const Padding(
          padding: EdgeInsets.all(2.0),
          child: Text(
            'Take Picture',
            style: AppTheme.title,
          )),
      onPressed: () {
        print('camera');
        Navigator.of(context).pop();
        getImageFile(ImageSource.camera);
      },
    );
    Widget optionPick = SimpleDialogOption(
      child: const Padding(
          padding: EdgeInsets.all(2.0),
          child: Text(
            'Choose from Gallery',
            style: AppTheme.title,
          )),
      onPressed: () {
        print('gallery');
        //getImage();
       getImageFile(ImageSource.gallery);
        Navigator.of(context).pop();
        loadAssets();
      },
    );
    Widget optionCancel = SimpleDialogOption(
      child: const Padding(
          padding: EdgeInsets.all(2.0),
          child: Text(
            'Cancel',
            style: AppTheme.title,
          )),
      onPressed: () {
        print('cancel');
        Navigator.of(context).pop();
      },
    );

    SimpleDialog dialog = SimpleDialog(
      //title: const Text('Choose'),
      children: <Widget>[
        optionCamera,
        Divider(),
        optionPick,
        Divider(),
        optionCancel,
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return dialog;
      },
    );
  }

  getImageFile(ImageSource source) async {
    //Clicking or Picking from Gallery

    XFile imageX = await _picker.pickImage(source: source);
    File image = File(imageX.path);

    //Cropping the image
//    File croppedFile = await ImageCropper.cropImage(
//      sourcePath: image.path,
//      maxWidth: 512,
//      maxHeight: 512,
//    );

    //Compress the image
    //print("cropped ${croppedFile.path}");
    String compress_path = "${image.parent.path}/compressed_${new DateTime.now().millisecondsSinceEpoch}.jpg";
    //print("compress $compress_path");
    var result = await FlutterImageCompress.compressAndGetFile(
      image.path,
      compress_path,
      minHeight: 720,
      minWidth: 720,
      quality: 90,
    );

    setState(() {
      var imageTaken= result.readAsBytesSync();
      tempCamera.add(imageTaken.buffer.asByteData());
      //print(userData._userTmpImage.lengthSync());
      byts2.add(imageTaken.buffer.asByteData());
    });
  }

  Widget buildGridView() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(4.0),
          child: Text("Add Image", style: AppTheme.title),
        ),

        Expanded(
          child: GridView.count(
            crossAxisCount: 3,
            children: List.generate(byts2.length + 2, (index) {
              if (index == byts2.length) {
                return Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: GestureDetector(
                      onTap: () {
                        getImageFile(ImageSource.camera);
                      },
                      child: Container(
                        height: 300,
                        width: 300,
                        decoration: BoxDecoration(
                            border: Border.all(color: AppTheme.colorLightGrey)),
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Center(
                            child: Icon(Icons.camera_alt,size: 60,color: AppTheme.colorMediumGrey,),
                          ),
                        ),
                      )),
                );
              } else if(index == byts2.length +1 ){
                return Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: GestureDetector(
                      onTap: () {
                        loadAssets();
                      },
                      child: Container(
                        height: 300,
                        width: 300,
                        decoration: BoxDecoration(
                            border: Border.all(color: AppTheme.colorLightGrey)),

                        child: Center(
                          child: Icon(Icons.folder,size: 60,color: AppTheme.colorMediumGrey),
                        ),
                      )),
                );
              } else{
                ByteData bts2 = byts2[index];
                return Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Stack(
                    children: <Widget>[
                      Container(
                          decoration: BoxDecoration(
                              border: Border.all(color: AppTheme.colorMediumGrey),color: AppTheme.colorLightGrey),
                          width: double.infinity,
                          height: double.infinity,
                          child: Image.memory(bts2.buffer.asUint8List(),
                              fit: BoxFit.cover
                          )),
//                      Positioned(
//                        right: 2,
//                        top: 2,
//                        child: GestureDetector(
//                            onTap: () {
//                              deleteSaved(index);
//                            },
//                            child: Text((base64Encode(bts2.buffer.asUint8List())).length.toString())),
//                      ),
                    ],

                  ),
                );
              }
            }),
          ),
        ),
      ],
    );
  }

  Future<void> loadAssets() async {
    List<Asset> resultList = List<Asset>();
    String error = 'No Error Dectected';
    List<ByteData> tempByte = List<ByteData>();

    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 300,
        enableCamera: true,
        selectedAssets: images,
        cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: MaterialOptions(
          actionBarColor: "#8BC73F",
          actionBarTitle: "SOVY DEPOT App",
          allViewTitle: "All Photos",
          useDetailsView: false,
          selectCircleStrokeColor: "#ffffff",
        ),
      );
    } on Exception catch (e) {
      error = e.toString();
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    tempByte = new List<ByteData>();
    for (int i = 0; i < resultList.length; i++) {
      ByteData byss = await resultList[i].getByteData(quality:100 );
      tempByte.add(byss);
    }

    setState(() {
      images = resultList;
      byts2 = tempByte;
      byts2.addAll(tempCamera);
//      saveImage(tempByte.toString());
      _error = error;
    });
  }

  saveImage(List<ByteData> tempList) async{
    List<String> tempPhotoString = new List();

    for (int i = 0; i < tempList.length; i++) {
      var buffer = tempList[i].buffer;
      List<int> list = buffer.asUint8List(
          tempList[i].offsetInBytes, tempList[i].lengthInBytes);

      var result = await FlutterImageCompress.compressWithList(
        list,
        minHeight: 720,
        minWidth: 720,
        quality: 70,
        rotate: 0,
      );
      print(list.length);
      print(result.length);
//      _createFileFromString(Uint8List.fromList(result));
      String tempTest = base64Encode(result);

//      if (decodedImage.width <= decodedImage.height) {
//          img.Image image = img.Image.fromBytes(360,
//              (decodedImage.width ~/ (decodedImage.height / 360)),
//              list);
//          img.Image imageResized = img.copyResize(image,width: 360);
//          List<int> imageBytes = imageResized.getBytes();
//          tempTest = base64Encode(imageBytes);
//      }else{
//          img.Image image = img.Image.fromBytes(
//              (decodedImage.width ~/ (decodedImage.height / 360)), 360,
//              list);
//          List<int> imageBytes = image.getBytes();
//          tempTest = base64Encode(imageBytes);
//      }


      tempPhotoString.add(tempTest);
    }

//    for (int i = 0; i < tempList2.length; i++) {
//      var buffer2 = tempList2[i].buffer;
//      var list2 = buffer2.asUint8List(
//          tempList2[i].offsetInBytes, tempList2[i].lengthInBytes);
//      print(list2);
//      String tempTest2 = base64Encode(list2);
//      print(tempTest2.length.toString());
//
//      if (newString2.length == 0) {
//        newString2 = tempTest2;
//      } else {
//        newString2 = newString2 + "," + tempTest2;
//      }
//    }

    _uploadImage(tempPhotoString);

  }

  @override
  Widget build(BuildContext context) {
//    SimplePermissions.requestPermission(Permission.WriteExternalStorage);
    return WillPopScope(
      onWillPop: (byts.length> 0 || byts2.length > 0) ? _willPopCallback:_Pop,
      child: Scaffold(
        appBar: new AppBar(
          backgroundColor: AppTheme.colorPrimary,
          title: Text(
            widget.docNo,
            style: AppTheme.title_white,
          ),
          leading: BackButton(
            color: Colors.white,
          ),
        ),
        body: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
              child: Container(
                color: AppTheme.colorSecondary,
                height: 2,
                width: MediaQuery.of(context).size.width,
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(left: 8.0, right: 8),
                child: buildGridView(),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
              child: Container(
                color: AppTheme.colorSecondary,
                height: 2,
                width: MediaQuery.of(context).size.width,
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Container(
                  color: Colors.transparent,
                  width: 170.0,
                  margin: new EdgeInsets.only(bottom: 10.0, top: 10.0),
                  child: new RaisedButton(
                      padding: const EdgeInsets.all(12.0),
                      child: new Text(
                        'Upload',
                        style: AppTheme.btnText,
                      ),
                      onPressed: () {
                        if(byts2.length <=0){
                          Fluttertoast.showToast(
                              msg: "Please upload attachment",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 1,
                              backgroundColor: Colors.red,
                              textColor: Colors.white,
                              fontSize: 16.0
                          );

                        }else {
                          saveImage(byts2);
                        }
                      },
                      color: AppTheme.colorPrimary,
//                              shape: RoundedRectangleBorder(
//                                  borderRadius: new BorderRadius.circular(10.0),
//                                  side: BorderSide(color: Colors.transparent)),
                    ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Future<bool> _willPopCallback() async {
    return (await showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Are you sure?',style: AppTheme.titleDlg),
        content: new Text('Do you want to leave without save?',style: AppTheme.titleDescDlg),
        actions: <Widget>[

          new FlatButton(
            onPressed: () => Navigator.of(context).pop(true),
            child: new Text('Discard',style: AppTheme.titleBtnDlgRed,),
          ),
        ],
      ),
    )) ?? false;
  }

  Future<bool> _Pop() async {
    return true;
  }

}
