import 'package:flutter/material.dart';

import 'appTheme.dart';

class DetailsDesc extends StatelessWidget {
  final String titleTxt;
  final String subTxt;

  const DetailsDesc({
    Key key,
    this.titleTxt: "",
    this.subTxt: "",
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.blueGrey.withOpacity(0.4),
      child: Padding(
        padding: const EdgeInsets.only(left: 24, right: 24, top: 6, bottom: 6),
        child: Row(
          children: <Widget>[
            SizedBox(
              width: 140,
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Text(
                      titleTxt != null ? titleTxt : "-",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontFamily: AppTheme.fontName,
                        fontWeight: FontWeight.w500,
                        fontSize: 16,
                        letterSpacing: 0.5,
                        color: AppTheme.darkerText,
                      ),
                    ),
                  ),
                  Text(
                    ": ",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontFamily: AppTheme.fontName,
                      fontWeight: FontWeight.w500,
                      fontSize: 16,
                      letterSpacing: 0.5,
                      color: AppTheme.darkerText,
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Text(
                subTxt == null || subTxt.length<=0 ? "-" :subTxt,
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontFamily: AppTheme.fontName,
                  fontWeight: FontWeight.w500,
                  fontSize: 16,
                  letterSpacing: 0.5,
                  color: AppTheme.darkerText,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class DetailsDesc2 extends StatelessWidget {
  final String titleTxt;
  final String subTxt;

  const DetailsDesc2({
    Key key,
    this.titleTxt: "",
    this.subTxt: "",
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.blueGrey.withOpacity(0.7),
      child: Padding(
        padding: const EdgeInsets.only(left: 24, right: 24, top: 6, bottom: 6),
        child: Row(
          children: <Widget>[
            SizedBox(
              width: 140,
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Text(
                      titleTxt ,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontFamily: AppTheme.fontName,
                        fontWeight: FontWeight.w500,
                        fontSize: 16,
                        letterSpacing: 0.5,
                        color: AppTheme.darkerText,
                      ),
                    ),
                  ),
                  Text(
                    ": ",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontFamily: AppTheme.fontName,
                      fontWeight: FontWeight.w500,
                      fontSize: 16,
                      letterSpacing: 0.5,
                      color: AppTheme.darkerText,
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Text(
                subTxt == null || subTxt.length<=0 ? "-" :subTxt,
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontFamily: AppTheme.fontName,
                  fontWeight: FontWeight.w500,
                  fontSize: 16,
                  letterSpacing: 0.5,
                  color: AppTheme.darkerText,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class DetailsDescBox extends StatelessWidget {
  final String titleTxt;
  final String subTxt;

  const DetailsDescBox({
    Key key,
    this.titleTxt: "",
    this.subTxt: "",
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(left: 12, right: 12,top: 2,bottom: 2),
        child: Row(
          children: <Widget>[
            SizedBox(
              width: 120,
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Text(
                      titleTxt ,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontFamily: AppTheme.fontName,
                        fontWeight: FontWeight.normal,
                        fontSize: 13,
                        letterSpacing: 0.5,
                        color: AppTheme.darkerText,
                      ),
                    ),
                  ),
                  Text(
                    ": ",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontFamily: AppTheme.fontName,
                      fontWeight: FontWeight.normal,
                      fontSize: 13,
                      letterSpacing: 0.5,
                      color: AppTheme.darkerText,
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Text(
                subTxt == null || subTxt.length<=0 ? "-" :subTxt,
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontFamily: AppTheme.fontName,
                  fontWeight: FontWeight.normal,
                  fontSize: 13,
                  letterSpacing: 0.5,
                  color: AppTheme.darkerText,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class DetailsDescAbout extends StatelessWidget {
  final String titleTxt;
  final String subTxt;

  const DetailsDescAbout({
    Key key,
    this.titleTxt: "",
    this.subTxt: "",
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(left: 12, right: 12,top: 10,bottom:10),
        child: Row(
          children: <Widget>[
            SizedBox(
              width: 120,
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Text(
                      titleTxt ,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontFamily: AppTheme.fontName,
                        fontWeight: FontWeight.normal,
                        fontSize: 15,
                        letterSpacing: 0.5,
                        color: AppTheme.darkerText,
                      ),
                    ),
                  ),
                  Text(
                    ": ",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontFamily: AppTheme.fontName,
                      fontWeight: FontWeight.normal,
                      fontSize: 15,
                      letterSpacing: 0.5,
                      color: AppTheme.darkerText,
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Text(
                subTxt == null || subTxt.length<=0 ? "-" :subTxt,
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontFamily: AppTheme.fontName,
                  fontWeight: FontWeight.normal,
                  fontSize: 13,
                  letterSpacing: 0.5,
                  color: AppTheme.darkerText,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}