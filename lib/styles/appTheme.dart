import 'package:flutter/material.dart';

class AppTheme {

  AppTheme._();
  static const Color colorPrimary = Color(0xFF8BC73F);
  static const Color colorSecondary = Color(0xFF65B500);
  static const Color colorLightGrey = Color(0xFFC1C1C1);
  static const Color colorMediumGrey = Color(0xFF7C7C7C);

  static const Color nearlyWhite = Color(0xFFFAFAFA);
  static const Color white = Color(0xFFFFFFFF);
  static const Color background = Color(0xFFF2F3F8);
  static const Color nearlyDarkBlue = Color(0xFF2633C5);

  static const Color nearlyBlue = Color(0xFF00B6F0);
  static const Color nearlyBlack = Color(0xFF213333);
  static const Color grey = Color(0xFF3A5160);
  static const Color dark_grey = Color(0xFF313A44);
  static const Color facebook_bg = Color(0xFF4267B2);

  static const Color darkText = Color(0xFF253840);
  static const Color darkerText = Color(0xFF17262A);
  static const Color lightText = Color(0xFF4A6572);
  static const Color deactivatedText = Color(0xFF767676);
  static const Color dismissibleBackground = Color(0xFF364A54);
  static const Color spacer = Color(0xFFF2F2F2);
  static const Color selectionsLogoColor = Color(0xFFb4bd34);
  static const Color selectionsCardColor = Color(0xFFFFFF88);
  static const Color statusBarColor = Colors.black12;

  //static const String fontName = 'SspuFont'; //change to SspuFont to check if font set
  static const String fontName = 'Futura';
  //static const String fontName = 'WorkSans';

  static const TextTheme textTheme = TextTheme(
    /*display1: display1,
    headline: headline,
    title: title,
    subtitle: subtitle,
    body2: body2,
    body1: body1,
    caption: caption,*/
  );

  static const TextStyle display1 = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 36,
    letterSpacing: 0.4,
    height: 0.9,
    color: darkerText,
  );

  static const TextStyle headline = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 24,
    letterSpacing: 0.27,
    color: darkerText,
  );

  static const TextStyle headline1 = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 22,
    letterSpacing: 0.27,
    color: darkerText,
  );

  static const TextStyle title = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 16,
    letterSpacing: 0.18,
    color: darkerText,
  );

  static const TextStyle titleDlg = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 17,
    letterSpacing: 0.18,
    color: darkerText,
  );

  static const TextStyle titleDescDlg = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 15,
    letterSpacing: 0.1,
    color: darkText,
  );

  static const TextStyle titleBtnDlgRed = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 14,
    letterSpacing: 0.1,
    color: Colors.red,
  );

  static const TextStyle titleBtnDlgBlack = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 14,
    letterSpacing: 0.1,
    color: Colors.black,
  );

  static const TextStyle titleDesc = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 12,
    letterSpacing: 0.1,
    color: colorMediumGrey,
  );

  static const TextStyle titleDescCondition = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 16,
    letterSpacing: 0.1,
    color: darkerText,
  );


  static const TextStyle titleOutlet = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 14,
    letterSpacing: 0.1,
    color: darkerText,
  );

  static const TextStyle titlePosition = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 14,
    letterSpacing: 0.1,
    color: colorMediumGrey,
  );

  static const TextStyle titleCondition = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 20,
    letterSpacing: 0.1,
    color: darkerText,
  );

  static const TextStyle title_white = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 16,
    letterSpacing: 0.1,
    color: white,
  );

  static const TextStyle subtitle = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 14,
    letterSpacing: -0.04,
    color: darkText,
  );

  static const TextStyle subtitle_white = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 14,
    letterSpacing: -0.04,
    color: white,
  );

  static const TextStyle body2 = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 13,
    letterSpacing: 0.3,
    color: darkText,
  );

  static const TextStyle bodyPrimary = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.w300,
    fontSize: 12,
    letterSpacing: 0.2,
    color: darkText,
  );

  static const TextStyle body3Negative = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 11,
    letterSpacing: 0.2,
    color: Colors.red,
  );

  static const TextStyle btnText = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.w500,
    fontSize: 15,
    letterSpacing: 0.2,
    color: Colors.white,
  );

  static const TextStyle body1 = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 14,
    letterSpacing: -0.05,
    color: darkText,
  );

  static const TextStyle caption = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 12,
    letterSpacing: 0.2,
    color: lightText, // was lightText
  );
}