import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'dart:typed_data';

import 'package:depot/utils/base64util.dart';
import 'package:flutter/material.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:string_unescape/string_unescape.dart';

import '../eor/pdf_screen2.dart';

class FileProcess {
  static bool isFolderCreated = false;
  static Directory directory;

  static checkDocumentFolder() async {
    try {
      if (!isFolderCreated) {
        directory = await getApplicationDocumentsDirectory();
        await directory.exists().then((value) {
          if (value) directory.create();
          isFolderCreated = true;
        });
      }
    } catch (e) {
      print(e.toString());
    }
  }
  //
  // static Future<File> downloadFile(String base64String, String eorno) async {
  //   var bytes = await consolidateHttpClientResponseBytes(response);
  //   String dir = (await getApplicationDocumentsDirectory()).path;
  //   File file = new File('$dir/$filename');
  //   await file.writeAsBytes(bytes);
  //   return file.path;
  // }

  static Future<String> downloadFile(String base64String, String eorno) async {
    Uint8List bytes = base64.decode(base64String.replaceAll("\"", ""));
    String dir = (await getApplicationDocumentsDirectory()).path;
    File file = File(
        "$dir/" + eorno + ".pdf");
    await file.writeAsBytes(bytes);
    return file.path;
  }

  static void openFile(String fileName, BuildContext context) async {
    await checkDocumentFolder();
    String dir = directory.path + "/${fileName}.pdf";

    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => PDFScreen2(
                  dir,
                  fileName + ".pdf",
                  isOnline: false,
                  isHorizontal: true,
                )));
    // OpenFile.open(dir);
  }
}
