
import 'package:depot/home_menu.dart';
import 'package:depot/main.dart';
import 'package:flutter/material.dart';
import 'AppPrefs.dart';

BuildContext _context;

class force_logout {
  force_logout(BuildContext context){
    _context = context;
  }

  void doForceLogout(){
    AppPrefs.setAccessToken("");

    Navigator.pushAndRemoveUntil(_context,
        MaterialPageRoute(builder: (BuildContext context) => MyHomePage()),(Route<dynamic> route) => false);

    // Navigator.pushReplacement(_context,
        // MaterialPageRoute(builder: (BuildContext context) => MyHomePage()));

    showDialog(
      context: _context,
      builder: (BuildContext context){
        return AlertDialog(
          title: Text("Login Error"),
          content: Text("Duplicate device logged detected. Please login again to continue."),
          actions:[
            FlatButton(
              child: Text("Close"),
              onPressed: (){
                Navigator.pop(context);
              },
            )
          ],
        );
      },
      barrierDismissible: false,
    );
  }
}