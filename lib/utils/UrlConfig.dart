import 'dart:io';

class UrlConfig {
  //remember to change and double check here when change environment
  //start
  static const String APP_SERVER = "https://selectionsgroceries.my/";
  static const String APP_ENVIRONMENT = "UAT"; //either UAT or PROD
  static const String DEFAULT_MERCHANT_ID = '1';
  static const String APP_MEMBER_GET_MEMBER_SLUG = "refer-a-friend-to-join-and-earn-100-points";
  //ended
  //No environment settings or configs below this line, only put between start & ended above.

  static const String APP_API = APP_SERVER + "api/";
  //these are routes
  static const String APP_ACTIVATE_ROUTE = "ActivateDevice";
  static const String APP_CONTAINER_LIST = "APIContainerList";
  static const String APP_UPLOAD_IMAGES = "APIAddAttachment";
  static const String APP_CONDITION_CHECK = "APIContAcceptOrReject";
  static const String GATE_IN_BL_CHECK = "APIBlackListContainerCheck";
  static const String GATE_IN_CHECK = "GetGateInSearchResult";
  static const String GATE_IN = "CreateGateInDetails";
  static const String TRUCK_IN_BL_CHECK = "APIGateOutBookingCheck";
  static const String TRUCK_IN_CHECK = "GetBookingItemDetails";
  static const String TRUCK_IN = "CreateTruckInDetails";
  static const String APP_DOWNLOAD_LOOKUP = "DownloadLookupTables";
  static const String EOR_DETAIL = "GetEORItemDetails";
  static const String UPDATE_EOR_DETAIL = "UpdateEORDetails";
  static const String GET_PRIME_HAULIER = "GetHaulierCodePMId";
  static const String GET_DRIVER_NAME = "GetPMDriverName";
  static const String CREATE_GATE_OUT_DETAILS = "CreateGateOutDetails";
  static const String GET_ACCESS_RIGHTS = "GetAccessRights";
  static const String GET_EOR_ITEMS = "GetEORItemString";
  static const String GET_EOR_REPORT = "GetEORItemReport";
  static const String GET_CONTAINER_DETAILS = "GetContainerDetails";
  static const String UPDATE_GATEIN_STATUS = "UpdateGateInStatus";
  static const String UPDATE_GATEIN_BLOCK = "UpdateGateInBlockStatus";
  static const String UPDATE_YARD_ZONE = "UpdateGateInYard";



  //these are not route but URL
  static const String APP_TERMS_URL = APP_SERVER + "terms?no_header=1";
  static const String APP_PRIVACY_URL = APP_SERVER + "privacy?no_header=1";
  static const String APP_INVITE_FRIEND_REFERRAL_URL = APP_SERVER + "referral";
  static const String APP_MEMBER_GET_MEMBER_URL = APP_SERVER + 'p/' + APP_MEMBER_GET_MEMBER_SLUG +'?no_header=1';



}