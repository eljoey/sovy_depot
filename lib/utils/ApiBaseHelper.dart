import 'dart:async';

import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'dart:convert';
import 'dart:io';
import 'UrlConfig.dart';
import 'AppPrefs.dart';

class ApiBaseHelper {
  Future<dynamic> get(String baseUrl, String route) async {
    var responseJson;
    print("req : "+ baseUrl + route);
    try {
      final response = await http.get(Uri.parse(baseUrl + route), headers: {
        "Content-Type": 'application/json; charset=UTF-8',
      }).timeout(Duration(seconds: 30));
      responseJson = _returnResponse(response);
    } on TimeoutException catch (e) {
      responseJson = json.decode('{"ErrorDetail":"","StatusCode":999, "ErrorMessage":"' +
          'Server Timeout. Please try again.' +
          '"}');
    } on SocketException catch (e) {
      responseJson = json.decode('{"ErrorDetail":"","StatusCode":999, "ErrorMessage":"' +
          'Internet connection error. .' + e.toString() +
          '"}');
    } catch (e) {
      responseJson =
          json.decode('{"ErrorDetail":"","StatusCode":999, "ErrorMessage":"' + "Server Error" + '"}');
    }
    return responseJson;
  }

  Future<dynamic> post(String baseUrl,String route, String data) async {
    var responseJson;
    print("req : " + route + " : " + data);
    List list = [];
    list.add(json.decode(data));

    Map data2 = {"AccessToken": await AppPrefs.getAccessToken(),
    "Params": list};

    try {
      final response = await http.post(Uri.parse(baseUrl + route),
          headers: <String,String>{
            "Content-Type": 'application/json; charset=UTF-8',
          },
          body: json.encode(data2)).timeout(const Duration(seconds: 60));
      responseJson = _returnResponse(response);

    } on TimeoutException catch (e) {
      responseJson = json.decode('{"ErrorDetail":"","StatusCode":999, "ErrorMessage":"' +
          'Server Timeout. Please try again.' +
          '"}');
    } on SocketException catch (e) {
      responseJson = json.decode('{"ErrorDetail":"","StatusCode":999, "ErrorMessage":"' +
          'Internet connection error. .' + e.toString() +
          '"}');
    } catch (e) {
      print(e.toString());

      responseJson =
          json.decode('{"ErrorDetail":"","StatusCode":999, "ErrorMessage":"' + "Server Error" + '"}');
    }
    return responseJson;
  }

  Future<dynamic> activate(String baseURL, String route, String data) async {
    var responseJson;
    print("req : "+ baseURL + route + " : " + data);
    try {
      final response = await http.post(Uri.parse(baseURL + route),
          headers: <String,String>{
            "Content-Type": 'application/json; charset=UTF-8',
          },
          body: data).timeout(const Duration(seconds: 10));
      responseJson = _returnResponse(response);
    } on TimeoutException catch (e) {
      responseJson = json.decode('{"ErrorDetail":"","StatusCode":999, "ErrorMessage":"' +
          'Server Timeout. Please try again.' +
          '"}');
    } on SocketException catch (e) {
      print("req : trycheck");

      responseJson = json.decode('{"ErrorDetail":"","StatusCode":999, "ErrorMessage":"' +
          'Internet connection error. Please check the network icon (or wireless connection settings) to see if you have Internet access and retry later.' +
          '"}');
    } catch (e) {
      print("req : trycheckhere");

      responseJson =
          json.decode('{"ErrorDetail":"","StatusCode":999, "ErrorMessage":"' + e.toString() + '"}');
    }
    return responseJson;
  }

//  Future<dynamic> put(String route, String data) async {
//    var responseJson;
//    print("req : " + route + " : " + data);
//    try {
//      final response = await http.put(_baseUrl + route,
//          headers: {
//            HttpHeaders.authorizationHeader: await AppPrefs.getUserToken(),
//            HttpHeaders.contentTypeHeader: 'application/json',
//            HttpHeaders.userAgentHeader: await AppPrefs.getPackageUserAgent(),
//            'platform': Platform.operatingSystem
//            /*'platform': (Platform.isAndroid ? TargetPlatform.android.toString() :
//                    (Platform.isIOS ? TargetPlatform.iOS.toString() : ""))*/
//          },
//          body: data);
//      responseJson = _returnResponse(response);
//    } catch (e) {
//      responseJson =
//          json.decode('{"code":"999", "message":"' + e.toString() + '"}');
//    }
//    return responseJson;
//  }

  dynamic _returnResponse(http.Response response) {
    /*switch (response.statusCode) {
      case 200:
        var responseJson = json.decode(response.body.toString());
        print(responseJson);
        return responseJson;
      case 400:
        throw BadRequestException(response.body.toString());
      case 401:
      case 403:
        //throw UnauthorisedException(response.body.toString());
        var responseJson = json.decode(response.body.toString());
        print(responseJson);
        return responseJson;
      case 500:
      default:
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
    }*/
    try {
      var responseJson = json.decode(response.body.toString());
      print(responseJson);
      return responseJson;
    } on Exception catch (e) {
//      return json.decode('{"code":"999", "message":"' + e.toString() + '"}');
      //throw FetchDataException(
      //    'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
      print("APiBaseHelper JSON decode exception: " +
          response.body.toString() +
          "ERROR: " +
          e.toString());
      Map data = {
        'code': '${response.statusCode}',
        'message': '${response.body}'
      };
      return jsonEncode(data);
    }
  }
  
}

