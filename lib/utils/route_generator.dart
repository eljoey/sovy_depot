import 'package:depot/aboutScreen.dart';
import 'package:depot/company_selection.dart';
import 'package:depot/container/depot_container.dart';
import 'package:depot/drop/gateIn.dart';
import 'package:depot/depotOut/depot_out_listing.dart';
import 'package:depot/eor/eorList.dart';
import 'package:depot/home_menu.dart';
import 'package:depot/truckIn/truckIn.dart';
import 'package:flutter/material.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    // Getting arguments passed in while calling Navigator.pushNamed

    switch (settings.name) {
      case '/home':
        return MaterialPageRoute(builder: (_) => HomeMenu());

      case '/depot_out_listing':
        return MaterialPageRoute(builder: (_) => DepotOutListing());

      case '/gateIn':
        return MaterialPageRoute(builder: (_) => GateIn());

      case '/truckIn':
        return MaterialPageRoute(builder: (_) => TruckIn());

      case '/eor':
        return MaterialPageRoute(builder: (_) => EORList());

      case '/about':
        return MaterialPageRoute(builder: (_) => AboutScreen());

      case '/container':
        final args = settings.arguments as ContainerDetail;
        return MaterialPageRoute(builder: (_) => ContainerDetail(bit:args.bit,));

      default:
        // If there is no such named route in the switch statement, e.g. /third
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Error'),
        ),
        body: Center(
          child: Text('ERROR'),
        ),
      );
    });
  }
}
