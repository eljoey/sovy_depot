import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class AppPrefs {

  static const String PREF_APP_SERVER = "app_server";
  static const String PREF_IS_USER_FIRST_INSTALL = "is_user_first_install";
  static const String PREF_IS_PHONE_ACTIVATED = "is_phone_activated";
  static const String PREF_ACCESS_TOKEN = "access_token";
  static const String PREF_MERCHANT_ID = "merchant_id";

  static const String PREF_POSITION_FRONT = "pos_front";
  static const String PREF_POSITION_DOOR = "pos_door";
  static const String PREF_POSITION_LEFT = "pos_left";
  static const String PREF_POSITION_RIGHT = "pos_right";
  static const String PREF_POSITION_TOP = "pos_top";
  static const String PREF_POSITION_BOTTOM = "pos_bottom";
  static const String PREF_POSITION_INSIDE = "pos_inside";
  static const String PREF_POSITION_SEAL = "pos_seal";

  static const String PREF_REMARK_FRONT = "rem_front";
  static const String PREF_REMARK_DOOR = "rem_door";
  static const String PREF_REMARK_LEFT = "rem_left";
  static const String PREF_REMARK_RIGHT = "rem_right";
  static const String PREF_REMARK_TOP = "rem_top";
  static const String PREF_REMARK_BOTTOM = "rem_bottom";
  static const String PREF_REMARK_INSIDE = "rem_inside";
  static const String PREF_REMARK_SEAL = "rem_seal";
  static const String PREF_SAVED_SEAL = "saved_seal";

  static const String PREF_USER_ID = "user_id";
  static const String PREF_MERCHANT_ALL = "merchant_all";
  static const String PREF_USER_TOKEN = "user_token";
  static const String PREF_LOOKUP_TABLES = "lookup_table";
  static const String PREF_PROVIDER_IMG = "provider_img";
  static const String PREF_PROVIDER_ID = "provider_id";
  static const String PREF_MERCHANT_ABOUT = "merchant_pages";
  static const String PREF_MERCHANT_LINK = "merchant_links";
  static const String PREF_ENQUIRY_CATEGORY = "enquries_category";
  static const String PREF_USER_PROFILE = "user_profile";
  static const String PACKAGE_USER_AGENT = "package_user_agent";
  static const String PREF_USER_BALANCES = "user_balances";
  static const String PREF_PUSH_TOKEN = "push_token";
  static const String PREF_SECRET_KEY = "secret_key";
  static const String PREF_REFERRAL_CODE = "referral_code";

  static const String PREF_PROVIDER_DETAIL = "provider_detail";

  static Future<bool> setAppServer(String merchantId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PREF_APP_SERVER, merchantId);
  }

  static Future<String> getAppServer() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PREF_APP_SERVER) ?? "app_server";
  }

  static Future<bool> setIsActivated(bool userLogged) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool(PREF_IS_PHONE_ACTIVATED, userLogged);
  }

  static Future<bool> getIsActivated() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(PREF_IS_PHONE_ACTIVATED) ?? false;
  }

  static Future<bool> setAccessToken(String merchantId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PREF_ACCESS_TOKEN, merchantId);
  }

  static Future<String> getAccessToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PREF_ACCESS_TOKEN) ?? "access";
  }

  static Future<bool> setIsUserFirstInstall(bool userInstall) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool(PREF_IS_USER_FIRST_INSTALL, userInstall);
  }

  static Future<String> getRemarkFront() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PREF_REMARK_FRONT) ?? "";
  }

  static Future<bool> setRemarkFront(String asset) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PREF_REMARK_FRONT, asset);
  }

  static Future<String> getRemarkDoor() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PREF_REMARK_DOOR) ?? "";
  }

  static Future<bool> setRemarkDoor(String asset) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PREF_REMARK_DOOR, asset);
  }

  static Future<String> getRemarkLeft() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PREF_REMARK_LEFT) ?? "";
  }

  static Future<bool> setRemarkLeft(String asset) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PREF_REMARK_LEFT, asset);
  }

  static Future<String> getRemarkRight() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PREF_REMARK_RIGHT) ?? "";
  }

  static Future<bool> setRemarkRight(String asset) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PREF_REMARK_RIGHT, asset);
  }

  static Future<String> getRemarkTop() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PREF_REMARK_TOP) ?? "";
  }

  static Future<bool> setRemarkTop(String asset) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PREF_REMARK_TOP, asset);
  }

  static Future<String> getRemarkBottom() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PREF_REMARK_BOTTOM) ?? "";
  }

  static Future<bool> setRemarkBottom(String asset) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PREF_REMARK_BOTTOM, asset);
  }

  static Future<String> getRemarkInside() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PREF_REMARK_INSIDE) ?? "";
  }

  static Future<bool> setRemarkInside(String asset) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PREF_REMARK_INSIDE, asset);
  }

  static Future<String> getRemarkSeal() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PREF_REMARK_SEAL) ?? "";
  }

  static Future<bool> setRemarkSeal(String asset) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PREF_REMARK_SEAL, asset);
  }

  static Future<String> getSavedSeal() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PREF_SAVED_SEAL) ?? "";
  }

  static Future<bool> setSavedSeal(String asset) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PREF_SAVED_SEAL, asset);
  }

  static Future<String> getPosFront() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PREF_POSITION_FRONT) ?? "";
  }

  static Future<bool> setPosFront(String asset) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PREF_POSITION_FRONT, asset);
  }

  static Future<String> getPosDoor() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PREF_POSITION_DOOR) ?? "";
  }

  static Future<bool> setPosDoor(String asset) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PREF_POSITION_DOOR, asset);
  }

  static Future<String> getPosLeft() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PREF_POSITION_LEFT) ?? "";
  }

  static Future<bool> setPosLeft(String asset) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PREF_POSITION_LEFT, asset);
  }

  static Future<String> getPosRight() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PREF_POSITION_RIGHT) ?? "";
  }

  static Future<bool> setPosRight(String asset) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PREF_POSITION_RIGHT, asset);
  }

  static Future<String> getPosTop() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PREF_POSITION_TOP) ?? "";
  }

  static Future<bool> setPosTop(String asset) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PREF_POSITION_TOP, asset);
  }

  static Future<String> getPosBottom() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PREF_POSITION_BOTTOM) ?? "";
  }

  static Future<bool> setPosBottom(String asset) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PREF_POSITION_BOTTOM, asset);
  }

  static Future<String> getPosInside() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PREF_POSITION_INSIDE) ?? "";
  }

  static Future<bool> setPosInside(String asset) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PREF_POSITION_INSIDE, asset);
  }

  static Future<String> getPosSeal() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PREF_POSITION_SEAL) ?? "";
  }

  static Future<bool> setPosSeal(String asset) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PREF_POSITION_SEAL, asset);
  }

  static Future<bool> isUserFirstInstall() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(PREF_IS_USER_FIRST_INSTALL) ?? false;
  }

  static Future<bool> setMerchantId(String merchantId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PREF_MERCHANT_ID, merchantId);
  }

  static Future<String> getMerchantId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PREF_MERCHANT_ID) ?? "1";
  }

  static Future<bool> setUserId(String merchantId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PREF_MERCHANT_ID, merchantId);
  }

  static Future<String> getUserId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PREF_MERCHANT_ID) ?? "1";
  }

  static Future<bool> setUserToken(String userToken) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PREF_USER_TOKEN, (userToken == '' ? '' : "Bearer " + userToken));
  }

  static Future<String> getUserToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PREF_USER_TOKEN) ?? "";
  }

  static Future<bool> setPushToken(String pushToken) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PREF_PUSH_TOKEN, pushToken);
  }

  static Future<String> getPushToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PREF_PUSH_TOKEN) ?? "";
  }

  static Future<bool> setLookup(String tables) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PREF_LOOKUP_TABLES, tables);
  }

  static Future<String> getLookup() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PREF_LOOKUP_TABLES) ?? "";
  }

  static Future<bool> setProviderDetail(String providerDetail) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PREF_PROVIDER_DETAIL, providerDetail);
  }

  static Future<String> getProviderDetail() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PREF_PROVIDER_DETAIL) ?? "";
  }

  static Future<bool> setProviderImg(String providerImg) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PREF_PROVIDER_IMG, providerImg);
  }

  static Future<String> getProviderImg() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PREF_PROVIDER_IMG) ?? "";
  }

  static Future<bool> setProviderId(String providerId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PREF_PROVIDER_ID, providerId);
  }

  static Future<String> getProviderId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PREF_PROVIDER_ID) ?? "";
  }

  static Future<bool> setPages(String pages) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PREF_MERCHANT_ABOUT, pages);
  }

  static Future<String> getPages() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PREF_MERCHANT_ABOUT) ?? "";
  }

  static Future<bool> setMerchant(String merchant) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PREF_MERCHANT_ALL, merchant);
  }

  static Future<String> getMerchant() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PREF_MERCHANT_ALL) ?? "";
  }

  static Future<bool> setEnquiry(String enquiry) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PREF_ENQUIRY_CATEGORY, enquiry);
  }

  static Future<String> getEnquiry() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PREF_ENQUIRY_CATEGORY) ?? "";
  }

  static Future<bool> setLinks(String links) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PREF_MERCHANT_LINK, links);
  }

  static Future<String> getLinks() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PREF_MERCHANT_LINK) ?? "";
  }

  static Future<bool> setUserProfile(String profile) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PREF_USER_PROFILE, profile);
  }

  static Future<String> getUserProfile() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PREF_USER_PROFILE) ?? "";
  }

  static Future<bool> setUserBalances(String balances) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PREF_USER_BALANCES, balances);
  }

  static Future<String> getUserBalances() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PREF_USER_BALANCES) ?? "";
  }

  static Future<bool> setHashKey(String haskey) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PREF_SECRET_KEY, haskey);
  }

  static Future<String> getHashKey() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PREF_SECRET_KEY) ?? "";
  }

  static Future<bool> setReferralCode(String refcode) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PREF_REFERRAL_CODE, refcode);
  }

  static Future<String> getReferralCode() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PREF_REFERRAL_CODE) ?? "";
  }

  static Future<bool> setPackageUserAgent(String packageUserAgent) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(PACKAGE_USER_AGENT, packageUserAgent);
  }

  static Future<String> getPackageUserAgent() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PACKAGE_USER_AGENT) ?? "";
  }

  static read(String key) async {
    final prefs = await SharedPreferences.getInstance();
    String temp = prefs.getString(key);
    if (temp != null) {
      return json.decode(temp);
    } else {
      return null;
    }

  }

  static save(String key, value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(key, json.encode(value));
  }

  static remove(String key) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(key);
  }
}